
import chunk from 'lodash';
import {RecordStruct} from '../constants/RecordStruct';
import update from 'immutability-helper';


 const userAddInit =       {
       }


export default function userAdd(state = userAddInit, action ){


	switch(action.type){
	    case "ADD_USER_CANCEL":
     	    var st =  chunk.merge({},state, {addUser: false, validate: false})
	    st =  chunk.omit(st,'newUser');
	    return st;
	    case "CLEAR_ALL_STATE" :
            return {};

	    case "ADD_USER_SAVE":
     	    st =  chunk.merge({},state, {addUser: false})
	    return chunk.omit(st,'newUser');
	    case "ADD_USER_CHANGE":
     	    return chunk.merge({},state, {newUser:action.value, validate:false})
	    case "ADD_USER":
     	    return chunk.merge({},state, {roles:action.roles}, {newUser:{email:"",name:"", role:""},addUser: true})
	    case "EDIT_USER" :
     	    return chunk.merge({},state, {newUser:action.user,addUser: true})
	    case "ADD_USER_VALIDATE":
	    return chunk.merge({},state,{validate:true});
	    case "USER_SAVE_REQUESTED":
	    return chunk.merge({},state,{userSaveRequested:action.value});
	    case "USER_SAVE_SUCCESSFULL":
     	    var st =  chunk.merge({},state, {addUser: false})
	    st =  chunk.omit(st,'newUser');
	    return st;
	    default: 
		return state;

	}


}
