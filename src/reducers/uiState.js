
import update from 'immutability-helper';
import chunk from 'lodash';

const uiInit =  { 
        filterValue:1,
        isEditing: false,
       }


export default function uiState(state = uiInit, action) {

	switch(action.type) {
	  case "EDITING":
		return {isEditing:action.value}
	  case "CLEAR_ALL_STATE" :
		return {};
	  default:
		return state;

	}



} 
