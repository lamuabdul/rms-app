
import chunk from 'lodash';
import {RecordStruct} from '../constants/RecordStruct';
import update from 'immutability-helper';


 const adminInit =       {
		roles:[],
		roleMap:{},
		users:[],
		userMap:{},
       }


export default function adminView(state = adminInit, action ){


	switch(action.type){
	    case "NEW_USER_FETCHED":
	    if(typeof state.userMap[action.value.email] !== 'undefined') {
		var idx = state.userMap[action.value.email];	
	        return update(state, {users:{[idx]:{$set: action.value}}});
	    } else { 
		var idx = state.users.length ;
		var st = chunk.merge({}, state, {userMap:{[action.value.email]:idx}});
	    	return update(st, {users:{$push:[action.value]} });
	    }
	    case "CLEAR_ALL_STATE" :
              return adminInit

	    case "NEW_ROLE_FETCHED":
	    if(typeof state.roleMap[action.value.id] !== 'undefined') {
		var idx = state.roleMap[action.value.id];	
	        return update(state, {roles:{[idx]:{$set: action.value}}});
	    } else { 
		var idx = state.roles.length ;
		var st = chunk.merge({}, state, {roleMap:{[action.value.id]:idx}});
	    	return update(st, {roles:{$push:[action.value]} });
	    }
	    default: 
		return state;

	}


}
