
import chunk from 'lodash';
import {RecordStruct} from '../constants/RecordStruct';
import update from 'immutability-helper';


 const roleAddInit =       {
	   perms:[],
	   removePerms:[],
       }


export default function roleAdd(state = roleAddInit, action ){


	switch(action.type){
	    case "PERMS_CHANGED":
	    var id = action.fieldId;
	    var st = state;
	    if(action.checked === true ){
	      st = update (state,{editPermVals : {$push: [action.fieldId]}})
	    } else {
	   	var permVals = state.editPermVals.filter((fld) => (fld !== action.fieldId))
	   	var removePerms = state.editPermVals.filter((fld) => (fld === action.fieldId))
		st= update(state, {editPermVals:{$set:permVals}});
		return chunk.merge(st, {removePerms:removePerms});
	    }
	    return chunk.merge({},state, st)
	    case "CLEAR_ALL_STATE" :
            return roleAddInit;

	    case "ADD_OBJECT_PERMS":
	    return chunk.merge({},state,{objectPerms:true, editPermVals:state.perms})
	    case "ADD_STATUS_PERMS":
	    return chunk.merge({},state,{statusPerms:true, editPermVals:state.perms})
	    case "OBJECT_PERMS_CANCEL":
	    var st = update(state, {editPermVals: {$set:[]}})
	    st = update(st, {removePerms:{$set:[]}});
	    return chunk.merge({},st,{objectPerms:false, fieldView:false, fieldEdit:false, statusPerms: false})
	    case "OBJECT_PERMS_DONE":
	    var perms = chunk.union(state.perms,state.editPermVals) 
	    perms = chunk.without(perms,...state.removePerms)
	    var st = chunk.merge({},state,{perms:[]});
	    st = update(st,{perms:{$set:perms}})
	     st = chunk.merge({},st, {objectPerms:false, fieldView:false, fieldEdit:false, statusPerms: false})
	     return update(st, {editPermVals: {$set:[]}})
	    case "ADD_FIELD_VIEW_PERMS":
	    return chunk.merge({},state,{fieldView:true, editPermVals:state.perms})
	    case "ADD_FIELD_EDIT_PERMS":
	    return chunk.merge({},state,{fieldEdit:true, editPermVals:state.perms})
	    case "ADD_ROLE_CANCEL":
     	    var st =  chunk.merge({},state, {addRole: false})
	    st =  chunk.omit(st,'newRole');
	    st =  chunk.omit(st,'perms');
	    return st;
	    case "ADD_ROLE_SAVE":
     	    st =  chunk.merge({},state, {addRole: false})
	    return chunk.omit(st,'newRole');
	    case "ADD_ROLE_CHANGE":
     	    return chunk.merge({},state, {newRole:action.record, validate:false})
	    case "ADD_USER":
     	    return chunk.merge({},state, {newUser:{id:"",name:""},addUser: true})
	    case "ADD_ROLE" :
     	    return chunk.merge({},state, {newRole:{id:"",name:""},addRole: true, perms:[]})
	    case "EDIT_ROLE" :
     	    return chunk.merge({},state, {newRole:action.role,addRole: true, perms:action.role.perms})
	    case "ADD_ROLE_VALIDATE":
	    return chunk.merge({},state,{validate:true});
	    case "ROLE_SAVE_REQUESTED":
	    return chunk.merge({},state,{roleSaveRequested:action.value});
	    case "ROLE_SAVE_SUCCESSFULL":
     	    var st =  chunk.merge({},state, {addRole: false})
	    st =  chunk.omit(st,'newRole');
	    st =  chunk.omit(st,'perms');
	    return st;
	    default: 
		return state;

	}


}
