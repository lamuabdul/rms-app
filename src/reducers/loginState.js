import chunk from 'lodash';
import auth from '../auth/auth';

const initial=  {      
                     isLoggedIn: auth.isLoggedIn(),
                     isLoginRequested: false,
                     users:[], 
                     username: "",
                     password: ""
                }


export default function loginState( state = initial, action) {
  switch(action.type) {
    case "LOGIN_SUCCESS":
      return ({
        isLoggedIn: true,
        isLoginRequested: false
      } )
    case "LOGIN_FAILURE":
      return ({
        isLoggedIn: false,
        isLoginRequested: false
      })
    case "LOGIN_REQUESTED":
      return ({
        isLoginRequested: action.sending
      })
    case "CLEAR_ALL_STATE" :
      return initial;

    case "CHANGE_FORM" :
     return  action.newState
    case "ROLES_FETCHED":
        return {...state, roles:action.roles}
    case "USERS_FETCHED":
        return {...state, users:action.users}
    case "SET_USER_DETAILS":
       if(action.ok) {
          return {...state, user:action.response}
       }
    case "SET_AUTH":
      if(action.newState === true) {
        return chunk.merge({}, state, {isLoggedIn : action.newState, token:action.token})
      } else {
        return chunk.merge({}, {isLoggedIn : action.newState, token:null})
      }
    default :
 	return state;

  }

}
