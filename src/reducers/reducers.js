
import auth from '../auth/auth';
import { SET_AUTH, CHANGE_FORM,  SET_ERROR_MESSAGE } from '../constants/AppConstants';
import {RecordStruct} from '../constants/RecordStruct';
import {PHOTOBASEURL} from '../constants/config';
import chunk from 'lodash';
import update from 'immutability-helper';

const assign = Object.assign || require('object.assign');


const initialState = {
	errorMessage:"",
       refugeeList: [],
       mainView: {
	sideNavOpen:false,
	filterValue:1,
	displayFields:[
	]
       },
       editView :{
	expandedCard:0,
	editRecord:{
	 detailedInfo:{},
	 addresses:[],
	 dependents:[],
	editAddress:{},
	editDependent:{},
        },
        isExtendedDetails:false,
	isAddressDetails:false,
	isDependentDetails:false,

       },
       uiState: {
        isEditing: false,
       }
	
};

export function getInitialState() {

	var df = [];

        RecordStruct.basicInfo.fields.map((field) =>  {
		df.push(field.id);
	})
	
	return chunk.merge(initialState, {mainView:{displayFields:df}});
}

export default function loginReducer( state = getInitialState(), action) {
  switch(action.type) {
    /*case "LOGIN_SUCCESS":
      return chunk.merge({}, state, {loginState: {
        isLoggedIn: true,
        isLoginRequested: false
      } })
    case "LOGIN_FAILURE":
      return chunk.merge({}, state, {loginState:{
        isLoggedIn: false,
        isLoginRequested: false
      }})
    case "LOGIN_REQUESTED":
      return chunk.merge({}, state, {loginState:{
        isLoginRequested: true
      }}) 
    case CHANGE_FORM :
      return chunk.merge({}, state, {loginState: action.newState} )
	
    case "REFUGEE_LIST_UPDATE":
	if(action.ok){
	 var map = {};
	 action.response.forEach((elem, index, arr) => {
		map[elem.key] = index;
	  })
	 return({...state ,refugeeList: action.response,refugeeMap:map})
	}
	else 
	 return state;
  
    case "SET_USER_DETAILS":
	if(action.ok) {
	  return {...state, loginState:{...state.loginState, user:action.response}}
	} 
  	
    case "FILTER_REFUGEE_LIST" :
      return chunk.merge({}, state, {uiState: {filterValue: action.value}} )
    case "FILTER_SEARCH_NAME":
      return chunk.merge({}, state, {uiState: {searchName: action.value}} )
    case "FILTER_SEARCH_COUNTRY":
      return chunk.merge({}, state, {uiState: {searchCountry: action.value}} )
 
    case SET_ERROR_MESSAGE:
      return chunk.merge({},state, {errorMessage:action.message})
    case SET_AUTH:
      if(action.newState === true) {
      	return chunk.merge({}, state, {loginState: {isLoggedIn : action.newState, token:action.token}})
      } else {
      	return chunk.merge({}, state, {loginState: {isLoggedIn : action.newState, token:null}})
      }
*/
    case "FORWARD_TO":
     return chunk.merge({}, state, {forwardTo:action}) 
  //  case "SIDENAV_OPEN":
  //   return chunk.merge({},state, {mainView:{sideNavOpen: action.open}})
   /* case "ADD_NEW_APPLICANT":
     return chunk.merge({}, state, {uiState: { isEditing:true}})
    case "ADD_EXTENDED_DETAILS":
     return chunk.merge ({},state, {uiState: {isExtendedDetails:true}})
    case "EXTENDED_DONE":
     return chunk.merge({},state, {uiState: {isExtendedDetails:false}})
    case "ADDRESS_DETAILS":
     return chunk.merge({},state,{uiState:{isAddressDetails:true}})
    case "ADDRESS_DETAILS_CANCEL":
     return chunk.merge({},state,{uiState:{isAddressDetails:false}})
    case "DEPENDENT_DETAILS":
     return chunk.merge({},state,{uiState:{isDependentDetails:true}})
    case "DEPENDENT_DETAILS_DONE":
      var st = {};
      if( state.uiState.dependentIndex >= 0  ){
	var ind = state.uiState.dependentIndex ;
	var edep = state.uiState.editRecord.editDependent;
	st = update(state,{uiState:{editRecord:{dependents: {[ind]:{$set: edep}} }} });
	st = chunk.merge({},st, 
		{uiState:{isDependentDetails:false, dependentIndex:null }});
        st.uiState.editRecord.editDependent = {};
      } else {
        st =  chunk.merge({},state,{uiState:{isDependentDetails:false}},{uiState:{editRecord:{dependents: [...state.uiState.editRecord.dependents,state.uiState.editRecord.editDependent]}}} )
        st.uiState.editRecord.editDependent = {};
      }
      return st;
    case "DEPENDENT_DETAILS_CANCEL":
	return chunk.merge({},state,{uiState:{isDependentDetails:false}})
    case "ADDRESS_DETAILS_DONE":
	var st1={};
      if( state.uiState.addressIndex >= 0  ){
        var ind = state.uiState.addressIndex ;
        var edep = state.uiState.editRecord.editAddress;
        st1 = update(state,{uiState:{editRecord:{addresses: {[ind]:{$set: edep}} }} });
        st1 = chunk.merge({},st1, 
                {uiState:{isAddressDetails:false, addressIndex:null }});
        st1.uiState.editRecord.editAddress = {};
	return st1;
      } else {
      	st1 =  chunk.merge({},state,{uiState:{isAddressDetails:false}},{uiState:{editRecord:{addresses: [...state.uiState.editRecord.addresses,state.uiState.editRecord.editAddress]}}} )
      	return chunk.omit(st1,'editAddress');
	}
    case "EDIT_CHANGE":
     var newRec = {};
     if(action.field == null){
	newRec = action.newState;
     }else {
	newRec[action.field] = action.newState;
     } 
     return chunk.merge({},state,{uiState: {editRecord:newRec }},{uiState:{editRecord:{validate:false}}},{uiState:{editTouched:true}})
    case "CARD_EXPANDED":
	var index = 0;
	if(action.expanded === true){
	   index = action.cardIdx;
	} else {
	   index = (action.cardIdx + 1)%4;
	}
	return chunk.merge({},state, {uiState:{expandedCard:index}});
    case "VALIDATE":
	return chunk.merge({},state,{uiState:{editRecord:{validate:true}}})
   */ //case "SAVE_RECORD":
//	return({...state ,refugeeList: [...state.refugeeList,state.uiState.editRecord]})
    case "SAVE_REQUESTED":
	return chunk.merge({}, state, {uiState: {showBusy:true}});
    case "SAVE_SUCCESSFULL":
	action.savedRecord.photoUrl = PHOTOBASEURL;
	if(typeof state.refugeeMap[action.savedRecord.key] !== 'undefined'  && state.refugeeMap[action.savedRecord.key] !== null){
		var idn = state.refugeeMap[action.savedRecord.key] ;
     		var sst2 = update(state,{refugeeList:{[idn]: {$set: action.savedRecord }}});
     		sst2 = update(sst2,{uiState:{editRecord: {$set: initialState.uiState.editRecord}}});
	        return chunk.merge({},sst2, {errorMessage:""}, {uiState:{editRecord: initialState.uiState.editRecord, isEditing:false,expandedCard:0, editTouched:false,showBusy:false}});
	} else {
	state.refugeeMap[action.savedRecord.key] = state.refugeeList.length;
	var sts = update(state, {uiState:{editRecord:initialState.uiState.editRecord}});
	sts = update(sts,{uiState:{status:{$set:{}}}});
	var ss = chunk.merge({},sts, {errorMessage:""}, {uiState:{isEditing:false,expandedCard:0, editTouched:false,showBusy:false}});
        return({...ss, refugeeList: [...ss.refugeeList, action.savedRecord]});
	}
    case "SAVE_FAILED":
	return chunk.merge({},state, {errorMessage:action.err}, {uiState:{showBusy:false}}) ; 
    case "LOADING_ALL_RECORDS":
	return chunk.merge({}, state, {loadingAllRecords: true});
    case "LOADING_COMPLETED":
	return chunk.merge({}, state, {loadingAllRecords: false});
    case "PHOTO_SELECTED":
	return chunk.merge({}, state, {uiState: {editRecord:{photoUrl:action.url}}});
    case "REFRESH_REFUGEE_LIST_REQUEST":
	return chunk.merge({},state,  {uiState:{refreshRequest:action.request}}) ; 
    case "REFRESH_REFUGEE_LIST_CONFIRM":
	return chunk.merge({},state,  {uiState:{refreshRequest:false, refreshRequestConfirm:true}}) ; 
    case "STATUS_CHANGE":
	return chunk.merge({}, state, {uiState:{statusChangeRequest:action.status , status:{}}});
    case "CONFIRM_STATUS_NON_NESTED":
	var stat = {};
	stat[action.value] = {};
	return chunk.merge({}, state, {uiState:{editRecord:{status:stat} , statusChangeRequest:"", status:{}}})
    case "CONFIRM_STATUS_CHANGE":
 	var status = state.uiState.status;	
	var statusKey = state.uiState.statusChangeRequest;
	var stat = {};
	stat[statusKey] = status;
	return chunk.merge({}, state, {uiState:{editRecord:{status:stat} , statusChangeRequest:"", status:{}}})
    case "CANCEL_STATUS_CHANGE":
	return chunk.merge({}, state, {uiState:{statusChangeRequest:"" , status:{}}});
    case "STATUS_EDIT":
     var newRec = {};
     newRec = action.newState;
     return chunk.merge({},state,{uiState: {status:newRec }})
    //case "VIEW_RECORD":
    // var st2 = update(state,{uiState:{editRecord:{$set: state.refugeeList[state.refugeeMap[action.value]]}}});
    // return chunk.merge({},st2,{uiState: {isEditing:false}});
   // case "EDIT_RECORD":
    // return chunk.merge({}, state, {uiState: { isEditing:true}}, {uiState:{editRecord:{editDependent:{}, editAddress:{}}}})
   // case "EDIT_DEPENDENT":
    // var dep = state.uiState.editRecord.dependents[action.value];
    // return chunk.merge({},state, {uiState: { editRecord:{editDependent: dep} , dependentIndex: action.value,isDependentDetails: true} });
   // case "EDIT_ADDRESS":
   //  var dep = state.uiState.editRecord.addresses[action.value];
   //  return chunk.merge({},state, {uiState: { editRecord:{editAddress: dep} , addressIndex: action.value,isAddressDetails: true} });
   // case "CLEAR_EDIT_DATA":
   //  var clr=  update(state, {uiState: {editRecord: {$set:initialState.uiState.editRecord}}});
//	clr = update(clr,{uiState:{status:{$set:{}}}});
//	return chunk.merge(clr, {uiState:{isEditing:false, editTouched: false, showBusy:false, expandedCard:0}});
    default:
      return state    
  };
}

function removeByKey (myObj, deleteKey) {
  return Object.keys(myObj)
    .filter(key => key !== deleteKey)
    .reduce((result, current) => {
      result[current] = myObj[current];
      return result;
  }, {});
}

  


