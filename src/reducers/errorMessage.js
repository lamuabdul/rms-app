
import update from 'immutability-helper';
import chunk from 'lodash';



export default function errorMessage(state = "", action) {

	switch(action.type) {
	  case "SET_ERROR_MESSAGE":
		return action.msg;
	  default:
		return state;
	}



} 
