
import chunk from 'lodash';
import update from 'immutability-helper';
import {RecordStruct} from '../constants/RecordStruct';


 const mainInit =       {
        sideNavOpen:false,
        filterValue:1,
        displayFields:[
        ]
       }

function initialSt() {
        
        var df = [];
        
        RecordStruct.basicInfo.fields.map((field) =>  {
                df.push(field.id);
        })
        
        return chunk.merge(mainInit, {displayFields:df});
}


export default function mainView(state = initialSt(), action ){


	switch(action.type){
	    case "SIDENAV_OPEN":
     	    return chunk.merge({},state, {sideNavOpen: action.open})
	    case "OPEN_MV_POP":
     	    return chunk.merge({},state, {key: action.key, ctxMenuOpen: true, ctxAnchor:action.target})
	    case "CLOSE_MV_POP":
     	    return chunk.merge({},state, {ctxMenuOpen: false, key:null})
	    case "CLEAR_CTX_MENU":
	    return chunk.merge({}, state, {ctxMenuOpen: false, key:null});
	    case "FILTER_REFUGEE_LIST" :
      		return chunk.merge({}, state, {filterValue: action.value} )
	    case "SET_DISPLAY_FIELDS":
		return update(state,{ displayFields:{$set: action.fields}});
	    case "SET_VIEW_FIELDS":
		return update(state,{ viewFields:{$set: action.fields}});
	    case "SET_EDIT_FIELDS":
		return update(state,{ editFields:{$set: action.fields}});
	    case "SET_USER_PERMS":
		return update(state,{permMap: {$set: action.permMap}});
    	    case "FILTER_SEARCH_NAME":
      		return chunk.merge({}, state, {searchName: action.value} )
    	    case "FILTER_SEARCH_COUNTRY":
      		return chunk.merge({}, state, {searchCountry: action.value} )
    	    case "FILTER_SEARCH_ID":
      		return chunk.merge({}, state, {searchId: action.value} )
    case "LOADING_ALL_RECORDS":
        return chunk.merge({}, state, {loadingAllRecords: true});
    case "LOADING_COMPLETED":
        return chunk.merge({}, state, {loadingAllRecords: false});
    case "REFRESH_REFUGEE_LIST_REQUEST":
        return chunk.merge({},state,  {refreshRequest:action.request}) ;
    case "REFRESH_REFUGEE_LIST_CONFIRM":
        return chunk.merge({},state,  {refreshRequest:false, refreshRequestConfirm:true}) ;
           case "CLEAR_ALL_STATE" :
                return initialSt();


	    default: 
		return state;

	}


}
