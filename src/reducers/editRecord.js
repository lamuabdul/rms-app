
import update from 'immutability-helper';
import chunk from 'lodash';

const editInit =       { 
        isEditing: false,
        showBusy: false,
        expandedCard:0,
        editRecord:{
         detailedInfo:{},
         socialStatus:{},
         addresses:[],
         events:[],
         dependents:[],
        editAddress:{},
        editEvent:{},
        editDependent:{},
        },
        isExtendedDetails:false,
        isAddressDetails:false,
        isDependentDetails:false,
       
       }


export default function editRecord(state = editInit, action) {

	switch(action.type) {


    case "SHOW_BUSY":
	return chunk.merge({}, state, {showBusy: action.value});

    case "EDIT_CHANGE":
     var newRec = {};
     if(action.field == null){
        newRec = action.newState;
     }else {
        newRec[action.field] = action.newState;
     }
     return chunk.merge({},state, {editRecord:newRec },{editRecord:{validate:false}},{editTouched:true})
     case "OPEN_EV_POP":
        return chunk.merge({},state, {key: action.key, ctxMenuOpen: true, ctxAnchor:action.target})
     case "CLOSE_EV_POP":
        return chunk.merge({},state, {ctxMenuOpen: false, key:null})
     case "CLEAR_EV_MENU":
        return chunk.merge({}, state, {ctxMenuOpen: false, key:null});
    case "PRIMARY_EDIT":
     var newRec = {};
     if(action.field == null){
        newRec = action.newState;
     }else {
        newRec[action.field] = action.newState;
     }
     return chunk.merge({},state, {primaryEdit:newRec },{primaryEdit:{validate:false}})
	
    case "VALIDATE":
        return chunk.merge({},state,{editRecord:{validate:true}})
    case "PHOTO_SELECTED":
        return chunk.merge({}, state, {editRecord:{photoUrl:action.url}});
    case "ADD_EXTENDED_DETAILS":
     return chunk.merge ({},state, {isExtendedDetails:true})
    case "EXTENDED_DONE":
     return chunk.merge({},state, {isExtendedDetails:false})
    case "ADDRESS_DETAILS":
     return chunk.merge({},state,{isAddressDetails:true},{editRecord:{editDependent:{}, editAddress:{}}})
    case "ADDRESS_DETAILS_CANCEL":
     var st1 =  chunk.merge({},state,{isAddressDetails:false})
        return chunk.omit(st1,'editRecord.editAddress');
    case "EVENT_CANCEL":
      var st =  chunk.merge({},state,{isEventDetails:false, editTouched:false})
        return chunk.omit(st,'editRecord.editEvent');
    case "EVENT_PANEL":
     return chunk.merge({},state,{isEventDetails:true}, {editRecord:{editEvent:{}}})
    case "EVENT_DETAILS":
     return chunk.merge({},state,{isEventDetails:true}, {editRecord:{editEvent:{}}})
    case "DEPENDENT_DETAILS":
     return chunk.merge({},state,{isDependentDetails:true})
    case "EVENT_DONE" :
	if (state.editRecord.events === null || typeof state.editRecord.events === 'undefined' ) {
        var st =  chunk.merge({},state,{isEventDetails:false, editTouched:true},{editRecord:{events: [state.editRecord.editEvent]}} )
        st.editRecord.editEvent = {};
	return st;

	} else {
        var st =  chunk.merge({},state,{isEventDetails:false, editTouched:true},{editRecord:{events: [...state.editRecord.events,state.editRecord.editEvent]}} )
        st.editRecord.editEvent = {};
	return st;
	}

    case "DEPENDENT_DETAILS_DONE":
      var st = {};
      if( state.dependentIndex !== null && state.dependentIndex >= 0  ){
        var ind = state.dependentIndex ;
        var edep = state.editRecord.editDependent;
        st = update(state,{editRecord:{dependents: {[ind]:{$set: edep}} }} );
        st = chunk.merge({},st,
                {isDependentDetails:false, dependentIndex:null });
        st.editRecord.editDependent = {};
      } else {
        st =  chunk.merge({},state,{isDependentDetails:false},{editRecord:{dependents: [...state.editRecord.dependents,state.editRecord.editDependent]}} )
        st.editRecord.editDependent = {};
      }
      return st;
    case "DEPENDENT_DETAILS_CANCEL":
        var st =  chunk.merge({},state,{isDependentDetails:false})
        return chunk.omit(st,'editRecord.editDependent');
    case "ADDRESS_DETAILS_DONE":
        var st1={};
      if( state.addressIndex !== null &&  state.addressIndex >= 0  ){
        var ind = state.addressIndex ;
        var edep = state.editRecord.editAddress;
        st1 = update(state,{editRecord:{addresses: {[ind]:{$set: edep}} }} );
        st1 = chunk.merge({},st1,
                {isAddressDetails:false, addressIndex:null });
        st1.editRecord.editAddress = {};
        return st1;
      } else {
        st1 =  chunk.merge({},state,{isAddressDetails:false},{editRecord:{addresses: [...state.editRecord.addresses,state.editRecord.editAddress]}} )
        return chunk.omit(st1,'editRecord.editAddress');
        }
    case "CONFIRM_EDIT_REQUEST":
	var st = chunk.merge({}, state, {primaryEdit :{ cr:state.editCR }},{editRecord:{validate:false}})
	return chunk.omit(st,'editRequestOpen','editCR');

    case "CANCEL_EDIT_REQUEST":
	var st = chunk.omit(state,'editRequestOpen','editCR',{editRecord:{validate:false}});
	return st;
    case "EDIT_REQUEST_CHANGE":
   	var newRec = {};
	return chunk.merge({}, state, {editCR:action.newState},{editRecord:{validate:false}});
    case "EDIT_CHANGE":
     var newRec = {};
     if(action.field == null){
        newRec = action.newState;
     }else {
        newRec[action.field] = action.newState;
     }
     return chunk.merge({},state, {editRecord:newRec },{editRecord:{validate:false}},{editTouched:true})
    case "PHOTO_SELECTED":
        return chunk.merge({}, state,  {editRecord:{photoUrl:action.url}});

    case "STATUS_CHANGE":
        return chunk.merge({}, state, {statusChangeRequest:action.status , status:{}});
    case "CONFIRM_STATUS_NON_NESTED":
        var stat = {};
        stat[action.value] = {statusdate: new Date()};
        return chunk.merge({}, state, {editRecord:{statusMap:stat, status: action.value} , statusChangeRequest:"", status:{}})
    case "CONFIRM_SAVE_STATUS_CHANGE":
        var status = state.status;
        var statusKey = state.statusChangeRequest;
        var stat = {};
        stat[statusKey] = status;
        return chunk.merge({}, state, {editRecord:{statusMap:stat,statusDate:new Date(), status:statusKey} , statusChangeRequest:"", status:{}})
    case "CONFIRM_STATUS_CHANGE":
        var status = state.status;
        var statusKey = state.statusChangeRequest;
        var stat = {};
        stat[statusKey] = status;
        return chunk.merge({}, state, {editRecord:{statusMap:stat, status:statusKey} , statusChangeRequest:"", status:{}})
    case "CANCEL_CTX_SOCIAL_STATUS_CHANGE":
        var r=  chunk.merge({}, state, {editRecord:{socialStatus:{}}});
	r =  update(r, {editRecord:{$set:editInit.editRecord}});
	return update(r, {editRecord:{socialStatus:{$set:{}}}});
    case "CANCEL_CTX_STATUS_CHANGE":
        var r=  chunk.merge({}, state, {statusChangeRequest:"" , status:{}});
	r =  update(r, {editRecord:{$set:editInit.editRecord}});
	return update(r, {status:{$set:{}}});
    case "CANCEL_STATUS_CHANGE":
	return chunk.merge({}, state, {statusChangeRequest:"" , status:{}});
    case "SOCIAL_STATUS_EDIT":
     var newRec = {};
     newRec = action.newState;
     return chunk.merge({},state,{editRecord:{socialStatus:newRec} })
    case "STATUS_EDIT":
     var newRec = {};
     newRec = action.newState;
     return chunk.merge({},state,{status:newRec })
    case "EDIT_REQUEST_OPEN":
     return chunk.merge({},state,{editRequestOpen:action.field,editCR:{}}, {editCR:state.primaryEdit.cr})   
 
    case "VIEW_PRIMARY":
      var st =  update(state, {primaryEdit:{$set:action.value}} )
      return chunk.merge({}, st, {primaryFields: action.fields});
    case "CLEAR_PRIMARY":
	var st = update(state, {primaryEdit:{$set:{}}});
	st = update(st, {primaryFields:{$set:[]}});
	return update(st, {editRecord:{$set:editInit.editRecord}});
    case "VIEW_RECORD":
     var st2 = update(state,{editRecord:{$set: action.value}});
     return st2;
     // TODO return chunk.merge({},st2,{uiState: {isEditing:false}});
    case "EDIT_RECORD":
     return chunk.merge({}, state, {editRecord:{editDependent:{}, editAddress:{}}})
     // TODOreturn chunk.merge({}, state, {uiState: { isEditing:true}}, {uiState:{editRecord:{editDependent:{}, editAddress:{}}}})
    case "EDIT_DEPENDENT":
     var dep = state.editRecord.dependents[action.value];
     return chunk.merge({},state, { editRecord:{editDependent: dep} , dependentIndex: action.value,isDependentDetails: true} );
    case "EDIT_ADDRESS":
     var dep = state.editRecord.addresses[action.value];
     return chunk.merge({},state,  { editRecord:{editAddress: dep} , addressIndex: action.value,isAddressDetails: true} );
    case "CLEAR_EDIT_DATA":
     var clr=  update(state, {editRecord: {$set:editInit.editRecord}});
        clr = update(clr,{status:{$set:{}}});
        return chunk.merge(clr, { editTouched: false, showBusy:false, expandedCard:0});
        // TODO return chunk.merge(clr, {uiState:{isEditing:false, editTouched: false, showBusy:false, expandedCard:0}});

     case "CLEAR_ALL_STATE" :
      return editInit;


    case "CARD_EXPANDED":
        var index = 0;
        if(action.expanded === true){
           index = action.cardIdx;
        } else {
           index = (action.cardIdx + 1)%4;
        }
        return chunk.merge({},state, {expandedCard:index});

	  default:
		return state;

	}



} 
