import {PHOTOBASEURL} from '../constants/config';
import update from 'immutability-helper';
import chunk from 'lodash';


 const refugeeInit =       {
	 refugeeList: []
       }



export default function refugee(state = refugeeInit, action ){


	switch(action.type){
	    case "REFUGEE_LIST_UPDATE":
        	if(action.ok){
         	var map = {};
         	action.response.forEach((elem, index, arr) => {
                	map[elem.key] = index;
          	})
         	return({...state ,refugeeList: action.response,refugeeMap:map})
        	}
        	else
         		return state;
	case "CLEAR_ALL_STATE" :
                return refugeeInit;

    case "SAVE_SUCCESSFULL":
        action.savedRecord.photoUrl = PHOTOBASEURL;
        if(typeof state.refugeeMap[action.savedRecord.key] !== 'undefined'  && state.refugeeMap[action.savedRecord.key] !== null){
                var idn = state.refugeeMap[action.savedRecord.key] ;
                var sst2 = update(state,{refugeeList:{[idn]: {$set: action.savedRecord }}});
		// reset editRecord
                //sst2 = update(sst2,{uiState:{editRecord: {$set: initialState.uiState.editRecord}}});
		// reset error, editrecord, isEditing, expandedCard , editTouched , showBusy
                //return chunk.merge({},sst2, {errorMessage:""}, {uiState:{editRecord: initialState.uiState.editRecord, isEditing:false,expandedCard:0, editTouched:false,showBusy:false}});
		return sst2;
        } else {
	//  console.log("Severe: No key found for successfully saved record");
	 // return state;
	// New Record Created  - add to Refugee Map
        state.refugeeMap[action.savedRecord.key] = state.refugeeList.length;
        //var sts = update(state, {editRecord:initialState.uiState.editRecord}});
        //sts = update(sts,{uiState:{status:{$set:{}}}});
        //var ss = chunk.merge({},sts, {errorMessage:""}, {uiState:{isEditing:false,expandedCard:0, editTouched:false,showBusy:false}});
        return({...state, refugeeList: [...state.refugeeList, action.savedRecord]});
	
        }

	    default: 
		return state;

	}


}
