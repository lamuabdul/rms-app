
import fetch from 'isomorphic-fetch'
import bcrypt from 'bcryptjs';
import {fblogin, fblogout} from './fbauth'; 
import { firebaseAuth } from '../constants/config';

var auth = {
  /**
   * Logs a user in
   * @param  {string}   username The username of the user
   * @param  {string}   password The password of the user
   * @param  {Function} callback Called after a user was logged in on the remote server
   */
  login(username, password, callback) {
    // If there is a token in the localStorage, the user already is
    // authenticated
    if (this.isLoggedIn()) {
      callback(true);
      return;
    } else {
	fblogin(username,password).then((a) => { console.log("A" + a);callback(true) } ).catch((error) => {
			callback(false,"Invalid username/password");
			return;
                  })
      return;

	}

      /*  let params = {username: username , password : password}
        let fetchData = {
         method: 'POST', 
         body: JSON.stringify( params ),
         headers: {
                'Content-Type':'application/json'
         }
        }
        
        fetch(`/login`,fetchData). then (function (response) {
                if(response.ok ){
                        localStorage.token = response.token;
                        callback(true)
                } else {
                        callback(false,response.statusText)
                }
        });
*/
              
    //request.post('/login', { username, password }, (response) => {
      // If the user was authenticated successfully, save a random token to the
      // localStorage
      //if (response.authenticated) {
       // localStorage.token = response.token;
       // callback(true);
      //} else {
        // If there was a problem authenticating the user, show an error on the
        // form
       // callback(false, response.error);
     // }
    //});
  },


  /**
   * Logs the current user out
   */
  logout(callback) {
    localStorage.clear();
    fblogout().then(() => {
    callback(true);
     }, (error) => {console.log(error);callback(false)});
  },
  /**
   * Checks if anybody is logged in
   * @return {boolean} True if there is a logged in user, false if there isn't
   */
  isLoggedIn() {
	var user = firebaseAuth().currentUser;

	if (user) {
		return true;
	} else {
		return false;
	}
    //return !!localStorage.token;
    //return true;
  },
  onChange() {}
}

module.exports = auth;

