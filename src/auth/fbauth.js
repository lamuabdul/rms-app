import { firebaseAuth } from '../constants/config'


export function fblogout () {
  return firebaseAuth().signOut()
}

export function fblogin (email, pw) {
  return firebaseAuth().signInWithEmailAndPassword(email, pw)
}

export function resetPassword (email) {
  return firebaseAuth().sendPasswordResetEmail(email)
}


