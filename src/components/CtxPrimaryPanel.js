import React,{Component} from 'react';
import EditAddress from './EditAddress';
import {connect} from 'react-redux';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import {RecordStruct} from '../constants/RecordStruct';
import FormField from './FormField';
import PIFieldUp from './PIFieldUp';
import  {Card, CardHeader, CardText} from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import {createRecord,goHome,primaryEdit,clearPrimary,createChangeRequest,showValidations} from '../actions/AppActions'
import chunk from 'lodash';




const STYLE = { 
  borderRadius: '0px 0px 2px',
  padding: '14px 24px 24px'
}
const STYLE_BOTTOM_LEFT_GRID = {
  borderRight: '1px solid #aaa',
  borderBottom: '1px solid #aaa',
  backgroundColor: '#f7f7f7'
}
const STYLE_TOP_LEFT_GRID = {
  borderBottom: '1px solid #aaa',
  borderRight: '1px solid #aaa',
  fontWeight: 'bold'
}
const STYLE_TOP_RIGHT_GRID = {
  fontWeight: 'bold'
}

const TABLE_STYLE= {
        backgroundColor: 'rgb(255, 255, 255)',
    width: '100%',
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    tableLayout: 'auto',
    borderLeft:'1px solid #aaa',
    fontFamily: 'Roboto, sans-serif'

}

const styles = {
 overlay1: {
 position: 'fixed',
 left: 0,
 right: 0,
 top: 0,
 bottom: 0,
 backgroundColor: 'white',
 filter:'alpha(opacity=80)',
 opacity:.80,
 zIndex: 10000000,
},

}


class CtxPrimaryPanel extends Component{


   constructor(props){
	super(props);
	this.handleSave = this.handleSave.bind(this);
	this.handleCancel = this.handleCancel.bind(this);
	this.valid = true;
        this.hasEditPerm = this.hasEditPerm.bind(this);
        this.hasOnlyViewPerm = this.hasOnlyViewPerm.bind(this);
        this.hasObjectPerm = this.hasObjectPerm.bind(this);

   } 


  handleSave() {
	     if(this.valid == false) {
		this.props.dispatch(showValidations())
		return;
	     }
	     var recordId = this.props.editRecord.key;
	     var fields = this.props.primaryFields.map((fld) => (fld.id));
	     fields.push("cr");
	     fields.push("key");
	     var rec = chunk.pick(this.props.editView.primaryEdit, fields);
	     rec = chunk.merge({},this.props.editView.editRecord,rec);
	     this.props.dispatch(clearPrimary());
             this.props.dispatch(createChangeRequest(rec, recordId));
  }

  handleCancel() {
	this.props.dispatch(clearPrimary());
	this.props.dispatch(goHome());
  }


        hasEditPerm(perm) {
                return this.props.permMap && ( this.props.permMap.has("EDIT_"+perm) || this.props.permMap.has("EDIT_ALL"));
        }

        hasOnlyViewPerm(perm) {
                return this.props.permMap && !this.props.permMap.has("EDIT_"+perm) && !this.props.permMap.has("EDIT_ALL") && ( this.props.permMap.has("VIEW_"+perm) || this.props.permMap.has("VIEW_ALL"));
        }

        hasObjectPerm(perm) {
                return this.props.permMap && this.props.permMap.has(perm);
        }



   render() {

	var noneStyle = this.props.editView.showBusy !== true ? {display:'none'}: {};
	var children = [];
		this.valid = true;
		this.validate = this.props.editView.editRecord.validate;
		var all = this.props.permMap.has("EDIT_ALL") ;
		var viewAll =  this.props.permMap.has("VIEW_ALL");
	        this.props.primaryFields.filter((fld) => ( (all || viewAll || this.props.permMap.has("EDIT_"+fld.id) || this.props.permMap.has("VIEW_"+fld.id)) ||  (fld.pri === true && (this.props.permMap.has("VIEW_ALL") || this.props.permMap.has("VIEW_"+fld.id) ) ))).map( (field) => {
			var disabled = !all && !this.props.permMap.has("EDIT_"+field.id) ;
			
			if (field.pi === true ){
                        children.push(<PIFieldUp key={field.id} editView={this.props.editView} editChange= {primaryEdit} validate={this.validate} field={field} dispatch={this.props.dispatch} record={this.props.editRecord}/>)
			} else {
                        children.push(<FormField key={field.id} editChange= {primaryEdit} disabled={disabled} validate={this.validate} field={field} dispatch={this.props.dispatch} record={this.props.editRecord}/>)
			}
                        if(field.required === true && ((typeof this.props.editRecord[field.id] === 'undefined') ||  !this.props.editRecord[field.id])) {
                                 this.valid = false;
                        }
			
                } ) ;
		



	return (	
	<div>
                <div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" >
                    <RefreshIndicator
                        size={40}
                        left={50}
                        top={50}
                        status={this.props.editView.showBusy === true ? "loading":"hide"}
                        style={styles.refresh}
                />

                </div>

      <Toolbar style={{backgroundColor:'transparent'}}>
        <ToolbarGroup firstChild={true}>
        </ToolbarGroup>
        <ToolbarGroup>
          <ToolbarSeparator />
	  <RaisedButton label="Save"  onTouchTap={this.handleSave} primary={true} />
	  <RaisedButton label="Cancel"  onTouchTap={this.handleCancel} primary={false} />
        </ToolbarGroup>
      </Toolbar>

  <Card initiallyExpanded={true}
        expanded={true} >
    <CardHeader
      title={RecordStruct.basicInfo.title}
      actAsExpander={false}
    />
    <CardText expandable={false}>
        <Divider/>
        <div style={{display:'inline'}}>
        {children}
        </div>
    </CardText>
  </Card>




	</div>
	)
   }

}

function select(state) {
  return {
    editRecord: state.editView.primaryEdit,
    permMap: state.mainView.permMap,
    editFields: state.mainView.editFields,
    viewFields: state.mainView.viewFields,
    editView: state.editView,
    primaryFields: state.editView.primaryFields,
  }
}

CtxPrimaryPanel.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  editView: React.PropTypes.object.isRequired,
  recordId: React.PropTypes.string,
  detailed: React.PropTypes.boolean,
}

export default connect(select)(CtxPrimaryPanel);

