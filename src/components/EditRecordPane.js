
import React, { Component } from 'react';
import {connect} from 'react-redux';
import { addExtendedDetails, editRecord,handleCardExpanded,setErrorMessage, showValidations,saveCurrentRecord,createRecord,goHome} from '../actions/AppActions'
import {RecordStruct} from '../constants/RecordStruct'
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import FormField from './FormField';
import EditAddress from './EditAddress';
import DependentPanel from './DependentPanel';
import Divider from 'material-ui/Divider';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import ContentSave from 'material-ui/svg-icons/content/save';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import ActionHome from 'material-ui/svg-icons/action/home';
import { Redirect } from 'react-router';
import {Link} from 'react-router-dom';
import Snackbar from 'material-ui/Snackbar';
import PhotoUpload from './PhotoUpload';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import StatusSelect from './StatusSelect';


const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  inline: {
   display:'inline-flex',
   verticalAlign:'bottom'
  },
  refresh: { 
    display: 'inline-block',
    position: 'relative',
    marginLeft: '40%',
    marginTop: '15%',
  },
 overlay1: {
 position: 'fixed',
 left: 0,
 right: 0,
 top: 0,
 bottom: 0,
 backgroundColor: 'white',
 filter:'alpha(opacity=80)',
 opacity:.80,
 zIndex: 10000000,
},

overlay: {
    display: 'flex',
    width:'90%',
    marginLeft:'5%',
    verticalAlign: 'bottom',
    flexDirection: 'column',
    background: 'rgba(0, 0, 0, 0.5)',
},

test: {
    display: 'flex',
    width:'90%',
    marginLeft:'5%',
    verticalAlign: 'bottom',
    flexDirection: 'column',
},

  gridList: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
  },
  exampleImageInput: {
    cursor: 'pointer',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
  },

};


class EditRecordPane extends Component {

  constructor(props) {
    super(props);
    this.handleExtended = this.handleExtended.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.closingSnack = this.closingSnack.bind(this);
    this.redirectHome = this.redirectHome.bind(this);
    this.basicValid = true;
    this.extendedValid = true;
    this.validate = false;
  }

static contextTypes = {
  router: React.PropTypes.shape({
    history: React.PropTypes.shape({
      push: React.PropTypes.func.isRequired,
      replace: React.PropTypes.func.isRequired
    }).isRequired,
    staticContext: React.PropTypes.object
  }).isRequired
};

  closingSnack() {
	this.props.dispatch(setErrorMessage(""));
  }

  redirectHome() {
	this.props.dispatch(goHome());
  }
	

  handleSave() {
	if(this.props.uiState.isEditing === false) {
		this.props.dispatch(editRecord());
	} else {
		this.props.dispatch(createRecord(this.props.editRecord));
	}
  }

  handleNext(id,expanded) {
     if(expanded === true) {
	if(id > 0 && !this.basicValid) {
		this.validate=true;
	        this.props.dispatch(showValidations());
		return;
	} 
     }
     if(expanded === false) {
	if((id === 0 && !this.basicValid) || (id ===1 && (!this.basicValid && !this.extendedValid))) {
		this.validate=true;
	    this.props.dispatch(showValidations());
	    return;
	}
     }
     this.props.dispatch(handleCardExpanded( id,expanded ));
  }


  render() {

    var children = [];
    this.basicValid = true;
    this.extendedValid = true;
    var disabled = this.props.uiState.isEditing === false;
    RecordStruct.basicInfo.fields.map((field) =>  {
	if(field.type === 'status') {
		children.push(<StatusSelect disabled={disabled} validate={this.validate} key={-10}/>);
	} else {
        children.push(<FormField key={field.id} validate={this.validate} disabled={disabled} field={field} dispatch={this.props.dispatch} record={this.props.editRecord}/>)
	if(field.required === true && ((typeof this.props.editRecord[field.id] === 'undefined') ||  !this.props.editRecord[field.id])){
		this.basicValid = false;
	}
	}
	return "";
    } )
	children.push(<br key={-30}/>);
	children.push(<PhotoUpload disabled={disabled} key={-40}/>);

 
    var detailed = [];

    RecordStruct.detailedInfo.fields.map((field) =>  {
        detailed.push(<FormField key={field.id} recordFieldName="detailedInfo"  disabled={disabled} validate={this.validate} field={field} dispatch={this.props.dispatch} record={this.props.editRecord.detailedInfo}/>)
	if(field.required === true && ((typeof this.props.editRecord.detailedInfo[field.id] === 'undefined') ||  !this.props.editRecord.detailedInfo[field.id])) {
		this.extendedValid = false;
	}
        return "";
    } )
	var noneStyle = this.props.editView.showBusy !== true ? {display:'none'}: {};
	var label = "";
	if(this.props.uiState.isEditing === true) {
	    label = "Save";	
	} else {
	   label = "Edit";
	}
	
    return (
		<div>
		<div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" > 
		    <RefreshIndicator
	                size={40}
        	        left={50}
           	        top={50}
                	status={this.props.editView.showBusy === true ? "loading":"hide"}
                	style={styles.refresh}
            	/>

	        </div> 
      <div style={styles.test} className="recordview"> 
	<Toolbar>
        <ToolbarGroup firstChild={true} >
          <DropDownMenu value={this.props.editView.expandedCard} labelStyle={{fontSize:'15px'}} onChange={(event,index,value) => this.handleNext(value,true)}>
            <MenuItem value={0} primaryText="Basic Info" />
            <MenuItem value={1} primaryText="Detailed Info" />
            <MenuItem value={2} primaryText="Dependents Info" />
            <MenuItem value={3} primaryText="Addresses" />
          </DropDownMenu>
        </ToolbarGroup>
        <ToolbarGroup >
	<IconButton onTouchTap={this.redirectHome} tooltip="Font Icon" >
	 <ActionHome color="rgb(0, 188, 212)"/>
    	</IconButton>

          <ToolbarSeparator />
          <RaisedButton label={this.props.uiState.editTouched ? label+"*" : label} 
			onTouchTap={this.handleSave} 
			disabled={this.basicValid === false || this.extendedValid === false} 
			primary={true} 
			labelPosition="before" 
	              icon={<ContentSave/>}/>
        </ToolbarGroup>
      </Toolbar>

	
  <Card initiallyExpanded={true} 
	onExpandChange={(expanded)=>this.handleNext(0,expanded)}
	expanded={this.props.editView.expandedCard === 0} >
    <CardHeader
      title={RecordStruct.basicInfo.title}
      actAsExpander={this.basicValid}
    />
    <CardText expandable={true}>
	<Divider/>
	<div style={{display:'inline'}}>
        {children}
	</div>
	<div style={{display:'flex',justifyContent:'flex-end'}} >
        <FlatButton label="Next" primary={true} onTouchTap={(e) => {e.preventDefault();this.handleNext(0,false)}}/>
        </div>
    </CardText>
  </Card>

  <Card  
	onExpandChange={(expanded)=>this.handleNext(1,expanded)}
	expanded={this.props.editView.expandedCard === 1}>
   <CardHeader 
     title={RecordStruct.detailedInfo.title}
     actAsExpander={this.basicValid&&this.extendedValid}
   />
   <CardText expandable={true}>
	<Divider/>
        <div style={{display:'inline'}}>
        {detailed}
        </div>
        <div style={{display:'flex',justifyContent:'flex-end'}} >
        <FlatButton label="Next" primary={true} onTouchTap={(e)=> {e.preventDefault();this.handleNext(1,false)}}/>
        </div>
   </CardText>
  </Card>

  <Card 
	onExpandChange={(expanded)=>this.handleNext(2,expanded)}
	expanded={this.props.editView.expandedCard === 2}>
   <CardHeader 
     title={RecordStruct.dependentInfo.title}
     actAsExpander={this.basicValid&&this.extendedValid}
   />
   <CardText expandable={true}>
	<Divider/>
	<DependentPanel dispatch={this.props.dispatch} editRecord={this.props.editView.editRecord} editView={this.props.editView} disabled={disabled}/>
   </CardText>
  </Card>

  <Card 
	onExpandChange={(expanded)=>this.handleNext(3,expanded)}
	expanded={this.props.editView.expandedCard === 3}>
   <CardHeader 
     title={RecordStruct.addresses.title}
     actAsExpander={this.basicValid&&this.extendedValid}
   />
   <CardText expandable={true}>
	<EditAddress dispatch={this.props.dispatch} editRecord={this.props.editView.editRecord} editView={this.props.editView} disabled={disabled}/>
   </CardText>
  </Card>

        <Snackbar
          open={this.props.errorMessage != null && this.props.errorMessage != ""  ? true : false}
          message={this.props.errorMessage}
          autoHideDuration={4000}
	  onRequestClose={this.closingSnack}
        />

     </div>
   </div>
    );
  };

  handleExtended() {
	this.props.dispatch(addExtendedDetails())
  }

}

function select(state) {
  return {
    editRecord: state.editView.editRecord,
    editView: state.editView,
    uiState: state.uiState,
    errorMessage: state.errorMessage,
  }
}

EditRecordPane.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  uiState: React.PropTypes.object.isRequired,
}

export default connect(select)(EditRecordPane);

