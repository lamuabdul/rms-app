import React, { Component } from 'react';

import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import {List, ListItem, makeSelectable} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import classnames from 'classnames';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';
import ContentReport from 'material-ui/svg-icons/content/report';
import AccountCircle from 'material-ui/svg-icons/action/account-circle';
import ActionHome from 'material-ui/svg-icons/action/home';
import ActionSettings from 'material-ui/svg-icons/action/settings';
import Eject from 'material-ui/svg-icons/action/eject';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import AccountSign from './AccountSign'
import { logout,sideNavOpen } from '../actions/AppActions';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';
import { push } from 'react-router-redux'
import style from '../sidenav.css';

const cx = classNames.bind(style);
//Logged.muiName = 'IconMenu';

const titleStyles = {
  title: {
    fontSize: '3vw' 
  }
};
	
class Sidenav extends Component {

  constructor(props){
    super(props);
    this.handleToggle = this.handleToggle.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleAccount = this.handleAccount.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.goAdmin = this.goAdmin.bind(this);
  }

  goAdmin(){
	this.props.dispatch(push('/admin'));
  }

  componentWillMount(){   
  }


  componentWillReceiveProps(nextProps){
  }

	render() {

	const dispatch = this.props.dispatch;
	const open = this.props.mainView.sideNavOpen
    	return (
         <div>
            <AppBar
            className={cx('app-bar', {'expanded': open})}
            onLeftIconButtonTouchTap={this.handleToggle}
            title="Refugee Management System"
	    titleStyle={titleStyles.title}
	    iconElementRight={<AccountSign/>}
            />
            <Drawer
            docked={true}
            open={open}
            onRequestChange={(open) => this.handleOpen({open})}
            >
          <List>
            <Subheader>Welcome!</Subheader>
            <ListItem primaryText="Home" leftIcon={<ActionHome />} />
            <ListItem primaryText="Reports" leftIcon={<ContentReport />} />
	     { typeof this.props.user !== 'undefined' && (this.props.user.perms.includes("ADD_ROLE") || 
					this.props.user.perms.includes("EDIT_ROLE") || this.props.user.perms.includes("ADD_USER") || this.props.user.perms.includes("EDIT_USER") 
					|| this.props.user.perms.includes("VIEW_AUDIT_TRAIL")
					) ?
            	<ListItem primaryText="Admin" leftIcon={<ActionSettings />} onTouchTap={this.goAdmin}/>
		: ""
	     }
          </List>
           </Drawer>
          {  <div className={cx('app-content', {'expanded': open})}> { this.props.children } </div>}
        </div>
	)
	}


  handleToggle() {
    this.props.dispatch(sideNavOpen(!this.props.mainView.sideNavOpen))
  }
  
  handleOpen(open) {
    this.dispatch(sideNavOpen(open))
  }

  handleAccount() {
  }
  handleClose() {
    this.dispatch(sideNavOpen(true))
  }


}

function select(state) {
  return {
    refugeeList: state.refugeeList,
    mainView: state.mainView,
    user: state.loginState.user,
  };
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(Sidenav);



