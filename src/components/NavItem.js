
import React, { Component } from 'react';
//import style from '../sidenav.css';

class NavItem extends Component {

        render() {
        return (
   	  <div className='menuitem' onClick={this.props.onClick}> <div href="#" className='menulink'> {this.props.navItemName}</div></div>
        )
        }
}

export default NavItem

NavItem.propTypes = {
//  onClick: React.PropTypes.func.isRequired
}

