
import {RecordStruct} from '../constants/RecordStruct';



export const ValidateBasicDetails = ( record ) => {

    var isValid = true;

    for (var i = 0; i < RecordStruct.basicInfo.fields.length; i++) {
	var field = RecordStruct.basicInfo.fields[i];
	if(field.required === true && record[field.id] == null  ){
		isValid = false;
		break;
	}
    }
    return isValid;

}
