
import React , {Component} from 'react';

import {RecordStruct} from '../constants/RecordStruct';
import {handleEventDone, showValidations, handleEventCancel} from '../actions/AppActions';
import RaisedButton from 'material-ui/RaisedButton';
import FormField from './FormField';
import Subheader from 'material-ui/Subheader';
import Dialog from 'material-ui/Dialog';
import Paper from 'material-ui/Paper';
import {connect} from 'react-redux';


  const styles = {

	}

class EditEvent extends Component {

    constructor(props) {
          super(props);
          this.handleEventDone = this.handleEventDone.bind(this);
          this.handleEventCancel = this.handleEventCancel.bind(this);
	  this.valid = true;
	  this.validate=false;
    }

    handleEventDone() {
	if(this.valid === false) {
		this.props.dispatch(showValidations());
	        this.validate = true;
		return;
	}
	this.props.dispatch(handleEventDone())
    }

    handleEventCancel() {
	this.props.dispatch(handleEventCancel());
    }

    render() {

           var children = [];
		this.valid = true;
            RecordStruct.events.fieldMap.forEach((field,k) =>  {
		console.log("pushing field " + field.id)
                children.push(<FormField key={field.id} recordFieldName="editEvent" validate={this.validate} field={field} dispatch={this.props.dispatch} record={this.props.editView.editRecord.editEvent}/>)
		if(field.required === true && ((typeof this.props.editRecord.editEvent[field.id] === 'undefined') ||  !this.props.editRecord.editEvent[field.id])){
                  this.valid = false;
        	}
            }); 
	var eventType = this.props.editView.editRecord.editEvent['eventType'];
	console.log(eventType )
	console.log(RecordStruct.eventDetails.fieldMap.get(eventType) )
	if(typeof eventType !== 'undefined' && eventType && typeof RecordStruct.eventDetails.fieldMap.get(eventType) !== 'undefined') {
		
		RecordStruct.eventDetails.fieldMap.get(eventType).fieldMap.forEach((field,k) => {
                	children.push(<FormField key={field.id} recordFieldName="editEvent" validate={this.validate} field={field} dispatch={this.props.dispatch} record={this.props.editView.editRecord.editEvent}/>)
			if(field.required === true && ((typeof this.props.editRecord.editEvent[field.id] === 'undefined') ||  !this.props.editRecord.editEvent[field.id])){
                  		this.valid = false;
        		}

		})

	}

        return (
                <Dialog modal={true} open={this.props.editView.isEventDetails} title="Add Event" autoScrollBodyContent={true} contentStyle={{width:'100%',maxWidth:"none"}}>
                <Paper zDepth={3} style={{paddingLeft:'20px', paddingRight:'10px' , paddingBottom:'10px', width:'100%'}}>

                <div style={styles.test} className="recordview">

                <form >
                        <Subheader>Add Event</Subheader>
                        <div style={{display:'inline'}}>
                        {
                        children
                        }
                </div>  

                </form>
                </div>

                <RaisedButton label="Done" primary={true} onTouchTap={this.handleEventDone}/>
                <RaisedButton label="Cancel"  onTouchTap={this.handleEventCancel}/>
                </Paper>
                </Dialog>
              )
    }

}


function select(state) {
  return {
    editView: state.editView,
    editRecord: state.editView.editRecord,
  }
}



export default EditEvent;

