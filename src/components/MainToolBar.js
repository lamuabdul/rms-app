
import React, { Component } from 'react';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';


class MainToolBar extends Component {


  render() {
    return (
      <Toolbar>
        <ToolbarGroup >
        </ToolbarGroup >
        <ToolbarGroup >
          <ToolbarTitle text="Options" />
          <FontIcon className="muidocs-icon-custom-sort" />
          <ToolbarSeparator />
          <RaisedButton label="Add New Applicant"  onTouchTap={this.props.addNewApplicant} primary={true} />
        </ToolbarGroup>
      </Toolbar>
    );
  }

}

MainToolBar.propTypes = {
  addNewApplicant: React.PropTypes.func.isRequired,
}

export default MainToolBar;

