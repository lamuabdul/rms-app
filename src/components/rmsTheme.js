import {cyan500,cyan700,lightBlack,pinkA200,grey100,grey500,deepPurpleA700,white,grey300,darkBlack} from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';
import Spacing from 'material-ui/styles/spacing';
import zIndex from 'material-ui/styles/zIndex';

export default {
  spacing: {
      iconSize: 24,
      desktopGutter: 24,
      desktopGutterMore: 32,
      desktopGutterLess: 16,
      desktopGutterMini: 8,
      desktopKeylineIncrement: 60,  // left-nav width = this * 4
      desktopDropDownMenuItemHeight: 22,
      desktopDropDownMenuFontSize: 12,
      desktopLeftNavMenuItemHeight: 30,
      desktopSubheaderHeight: 48,
      desktopToolbarHeight: 56
     },
    dialog: {
      titleFontSize: 22,
      bodyFontSize: 12,
    },
    textField:{
	spacing:10,
	bodyFontSize:12,
    },

  zIndex: zIndex,
  fontFamily: 'Roboto, sans-serif',
};

