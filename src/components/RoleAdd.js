
import React, {Component} from 'react';
import {connect} from 'react-redux';
import { push } from 'react-router-redux'
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import {Rights,Status,StatusMap, RecordStruct} from '../constants/RecordStruct';
import {editRole,addRoleCancel, addRoleSave, addObjectPerms,addRoleValidate,setErrorMessage,addStatusPerms, addFieldViewPerms, permsChanged, addFieldEditPerms,addRoleChange, objectPermsDone, objectPermsCancel} from '../actions/AppActions';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import SelectField from 'material-ui/SelectField';
import Checkbox from 'material-ui/Checkbox';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import FlatButton from 'material-ui/FlatButton';
import _ from 'lodash';
import Chip from 'material-ui/Chip';
import RefreshIndicator from 'material-ui/RefreshIndicator';

const styles = {

  refresh: {
    display: 'inline-block',
    position: 'relative',
    marginLeft: '40%',
    marginTop: '15%',
  },
 overlay1: {
 position: 'fixed',
 left: 0,
 right: 0,
 top: 0, 
 bottom: 0,
 backgroundColor: 'white',
 filter:'alpha(opacity=80)',
 opacity:.80,
 zIndex: 10000000,
},

  chip: {
    margin: 4,
  },
  rows: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection:'column',
  },
  item: {
    display: 'flex',
    flexFlow:'row',
    marginLeft:'5%',
    marginTop:'5%',
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection:'column',
    justifyContent: 'space-around',
  },
  second: {
	display:'table'
  },
  inline: {
   display:'inline-flex',
   verticalAlign:'bottom'
  },

test: {
    display: 'flex',
    width:'90%',
    marginLeft:'5%',
    verticalAlign: 'bottom',
    flexDirection: 'column-reverse',
},

  gridList: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
  },
};



class RoleAdd extends Component {


	constructor(props) {
		super(props);
                this.handleChange = this.handleChange.bind(this);
                this.handleSave = this.handleSave.bind(this);
	}

	handleChange(evt){
                var newChange = {};
                newChange[evt.target.id] = evt.target.value
		console.log(newChange);
		this.props.dispatch(addRoleChange(newChange));
	}
	
	handleSave(){
		if(this.props.record.id === "" || this.props.record.name === "" ) {
			this.props.dispatch(addRoleValidate());	
			return;
		}
		var record = this.props.record;	
		record.perms = this.props.perms;
		this.props.dispatch(addRoleSave(record));
	}

	getEditFields(values,fields, recordName) {
		return  fields.filter((field) => (field.pi !== true && field.id !== 'status')).map((fld) => (
		    this.getEditField(values, fld, recordName)
		))
	}

	getEditField(values,fld, recordName) {

		return (
	 	<ListItem
		style={{fontSize:14}}
		innerDivStyle={{fontSize:14,lineHeight:'10px'}}
		leftCheckbox={
      		<Checkbox
		disabled={_.includes(this.props.perms,"VIEW_ALL")}
        	checked={(values && values.indexOf("EDIT_"+fld.id) > -1) ||  _.includes(this.props.perms,"EDIT_ALL")}
        	key={fld.name}
		onCheck={(e,checked)=>this.props.dispatch(permsChanged("EDIT_"+fld.id,checked))}
      	        />}
        	primaryText={fld.name}
        	secondaryText={"Permission to edit " + fld.name + " of a " + recordName}
		/>)
	}
	getViewField(values, fld, recordName) {
	    return (
	 	<ListItem
		style={{fontSize:14}}
		innerDivStyle={{fontSize:14,lineHeight:'10px'}}
		leftCheckbox={
      		<Checkbox
		disabled={_.includes(this.props.perms,"VIEW_ALL") || _.includes(this.props.perms,"EDIT_ALL")}
        	key={fld.name}
		onCheck={(e,checked)=>this.props.dispatch(permsChanged("VIEW_"+fld.id,checked))}
        	checked={(values && values.indexOf("VIEW_"+fld.id) > -1) || _.includes(this.props.perms,"VIEW_ALL") || _.includes(this.props.perms,"EDIT_ALL")}
      	        />}
        	primaryText={fld.name}
        	secondaryText={"Permission to view " + fld.name + " of a " + recordName}
		/>)
	}

	getViewFields(values, fields, recordName) {
		return fields.map((fld) => (
		   this.getViewField(values, fld, recordName)
		))
	}


	editFields(values){
		var child = this.getEditFields(values,RecordStruct.basicInfo.fields, " refugee record");
		child.push( this.getEditFields(values,RecordStruct.detailedInfo.fields, " refugee record"));
		child.push( this.getEditField(values,{id:"addresses", name:"Addresses"}, " refugee record"));
		child.push( this.getEditField(values,{id:"dependentInfo", name:"Dependents"}, " refugee record"));
		//child.push( this.getEditFields(values,RecordStruct.addresses.fields, " refugee address"));
		//child.push( this.getEditFields(values,RecordStruct.dependentInfo.fields, " refugee's dependent"));
		return child;
	}

	viewFields(values) {
		var child = this.getViewFields(values, RecordStruct.basicInfo.fields, "refugee record");
		child.push( this.getViewFields(values, RecordStruct.detailedInfo.fields, "refugee record"));
		child.push( this.getViewField(values, {id:"addresses",name:"Addresses"}, "refugee record"));
		child.push( this.getViewField(values, {id:"dependentInfo",name:"Dependents"}, "refugee record"));
		//child.push( this.getViewFields(values, RecordStruct.addresses.fields, "refugee address"));
		//child.push( this.getViewFields(values, RecordStruct.dependentInfo.fields, "refugee's dependent"));
		return child
	}

        rights(values) {
    	  return Rights.fields.filter((field)=> field.type === 'single' ).map((fld) => (
	 	<ListItem
		style={{fontSize:14}}
		innerDivStyle={{fontSize:14,lineHeight:'10px'}}
		leftCheckbox={
      		<Checkbox
        	key={fld.name}
        	checked={values && values.indexOf(fld.id) > -1}
		onCheck={(e,checked)=>this.props.dispatch(permsChanged(fld.id,checked))}
      	        />}
        	primaryText={fld.name}
        	secondaryText={fld.detail}
		/>
    	  ));
  	}

	statusPerms(values) {
		
		return Status.map((stat) => (
	 	<ListItem
		style={{fontSize:14}}
		innerDivStyle={{fontSize:14,lineHeight:'10px'}}
		leftCheckbox={
      		<Checkbox
        	key={stat}
        	checked={values && values.indexOf("STATUS_"+stat) > -1}
		onCheck={(e,checked)=>this.props.dispatch(permsChanged("STATUS_"+stat,checked))}
      	        />}
        	primaryText={StatusMap[stat].title}
        	secondaryText={"Update Status from " + StatusMap[stat].title}
		/>
		))
	}


	componentWillReceiveProps(nextProps) {
		if(nextProps.addRole === false) {
			nextProps.dispatch(push('/admin')) 
		}
	

	}

	shouldComponentUpdate(nextProps, nextState){
		return  nextProps.addRole ;
	}
	


	render() {
		
	   var noneStyle = this.props.roleSaveRequested !== true ? {display:'none'}: {};
	   var isError = this.props.validate === true  && (this.props.record.id === "" || this.props.record.name === "" );
		
	   var givenPerms = this.props.perms.map(( perm ) => {
		 var fieldName ;
		 var value;
		if(typeof Rights.rightMap.get(perm) !== 'undefined'){
			value = Rights.rightMap.get(perm).name;
			return <Chip style={styles.chip} >{value}</Chip>;	
		}
		if( perm.startsWith("VIEW_")){
			fieldName = perm.substring(perm.indexOf("VIEW_") + 5);	
			if(typeof RecordStruct.map.get(fieldName) !== 'undefined'){
				value = "View " + RecordStruct.map.get(fieldName).name;
				return <Chip style={styles.chip}>{value}</Chip>;
			} else if (typeof RecordStruct[fieldName] !== 'undefined') {
				value = "View " + RecordStruct[fieldName].title;
				return <Chip style={styles.chip}>{value}</Chip>;
			}
		}
		if( perm.startsWith("EDIT_")){
			fieldName = perm.substring(perm.indexOf("EDIT_") + 5);	
			if(typeof RecordStruct.map.get(fieldName) !== 'undefined'){
				value = "Edit " + RecordStruct.map.get(fieldName).name;
				return <Chip style={styles.chip}>{value}</Chip>;
			} else if (typeof RecordStruct[fieldName] !== 'undefined') {
				value = "Edit " + RecordStruct[fieldName].title;
				return <Chip style={styles.chip}>{value}</Chip>;

			}
		}
		if( perm.startsWith("STATUS_")){
			fieldName = perm.substring(perm.indexOf("STATUS_") + 7);	
			if(typeof StatusMap[fieldName] !== 'undefined'){
				value = "Update Status From " + StatusMap[fieldName].title;
				return <Chip style={styles.chip}>{value}</Chip>;
			}
		}
		return <Chip>Error</Chip>
		})

	    const actions = [
	      <FlatButton
       	 	label="Cancel"
        	primary={true}
		onTouchTap = {() => this.props.dispatch(objectPermsCancel())}
      		/>,
      		<FlatButton
        	label="Done"
        	primary={true}
        	keyboardFocused={true}
		onTouchTap = {() => this.props.dispatch(objectPermsDone())}
      		/>,
    		];



		var disabled = false;
	  return (

		<div>
               <div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" >
                    <RefreshIndicator
                        size={40}
                        left={50}
                        top={50}
                        status={this.props.roleSaveRequested === true ? "loading":"hide"}
                        style={styles.refresh}
                     /> 
                 </div> 

        <Dialog
          actions={[      <FlatButton
        	label="Ok"
        	primary={true}
        	onTouchTap={()=>this.props.dispatch(setErrorMessage(""))}
      		/>,
	  ]}
          modal={false}
          open={this.props.errorMessage !== ""}
          onRequestClose={()=>this.props.dispatch(setErrorMessage(""))}
        >
          {"Error Saving Role: "+this.props.errorMessage}
        </Dialog>



      <Toolbar style={{backgroundColor:'transparent'}}>
        <ToolbarGroup firstChild={true}>
        <ToolbarTitle text="Add New Role" />
        </ToolbarGroup>
        <ToolbarGroup>
                <RaisedButton label="Add" primary={true} onTouchTap={this.handleSave}/>
                <RaisedButton label="Cancel"  onTouchTap={() => this.props.dispatch(addRoleCancel())}/>
          <ToolbarSeparator />
        </ToolbarGroup>
      </Toolbar>
		<div style={styles.rows}>
		<div style={styles.item} >
                <div key={0}  >
                        <TextField type="text"
                         disabled={disabled}
			 hintText="e.g. admin"
                         name="ID" onChange={this.handleChange} floatingLabelText={"Role Id"}
			 floatingLabelFixed={true}
			 style={{marginLeft:'5%', marginRight:'5%'}}
                         errorText= {isError ? "This field is required":null}
                         id="id" value={this.props.record["id"]} />
                </div>
                <div key={1} >
                        <TextField type="text"
                         disabled={disabled}
			 hintText="e.g. Administrator"
			 style={{marginLeft:'5%', marginRight:'5%'}}
			 floatingLabelFixed={true}
                         name="Name" onChange={this.handleChange} floatingLabelText={"Role Name"}
                         errorText= {isError ? "This field is required":null}
                         id="name" value={this.props.record["name"]} />
                </div>
			</div>
		<div style={styles.rows}>
			<List style={{display:'inline-flex', justifyContent:'space-around',marginLeft:'5%'}}>
				<ListItem  primaryText="Object Permissions" onTouchTap = {() => this.props.dispatch(addObjectPerms())}>
				</ListItem>
				<ListItem primaryText="View Field Permissions" onTouchTap = {() => this.props.dispatch(addFieldViewPerms())}/>
				<ListItem primaryText="Edit Field Permissions" onTouchTap={() => this.props.dispatch(addFieldEditPerms())}/>
				<ListItem primaryText="Status Permissions" onTouchTap={() => this.props.dispatch(addStatusPerms())}/>
				</List>
			</div>
			<div style={{display:'flex', marginLeft:'5%', flexDirection:'row' , flexWrap:'wrap',  marginTop:'2%'}}> 
					{givenPerms}
			</div>
			<Dialog actions={actions} 
				modal={false} 
				autoScrollBodyContent={true} 
				title="Add Permissions" 
				open = {this.props.objectPerms === true || this.props.fieldView === true || this.props.fieldEdit == true || this.props.statusPerms === true}>
				{this.props.objectPerms === true ?
				<Paper zDepth={1} style={{marginTop:'5%'}} >
			<Subheader>Object Permissions</Subheader>
				<List>
				{this.rights(this.props.editPermVals)}
				</List>
				</Paper>
				: (this.props.fieldView === true) ?
				<Paper zDepth={1} style={{marginTop:'5%'}} >
			<Subheader>View Field Permissions</Subheader>
				<List>
				{this.viewFields(this.props.editPermVals)}
				</List>
				</Paper>
				: this.props.fieldEdit === true ? 
				<Paper zDepth={1} style={{marginTop:'5%'}} >
			<Subheader>Edit Field Permissions</Subheader>
				<List>
				{this.editFields(this.props.editPermVals)}
				</List>
				</Paper>  : 
				<Paper zDepth={1} style={{marginTop:'5%'}} >
			<Subheader>Status Permissions</Subheader>
			<List>
        		{this.statusPerms(this.props.editPermVals)}
			</List>
			</Paper>
			}
		</Dialog>
		</div>
		</div>
	)
	}




}

function select(state) {
  return {
	record: state.roleAdd.newRole,
	addRole: state.roleAdd.addRole,
	objectPerms: state.roleAdd.objectPerms,
	statusPerms: state.roleAdd.statusPerms,
	fieldView: state.roleAdd.fieldView,
	fieldEdit: state.roleAdd.fieldEdit,
	perms: state.roleAdd.perms,
	editPermVals: state.roleAdd.editPermVals,
	validate: state.roleAdd.validate,
	roleSaveRequested: state.roleAdd.roleSaveRequested,
	errorMessage: state.errorMessage,
  }

}

RoleAdd.propTypes = {
	record: React.PropTypes.object,	
	addRole: React.PropTypes.bool,
}

export default connect(select)(RoleAdd);




