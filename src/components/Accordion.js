import React,{Component} from 'react';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import {connect} from 'react-redux';


class Accordion extends Component {

   constructor(props){
	super(props)
        this.cards = [];
   }

   handleExpand(cardId, expanded) {
     console.log("handleExpand called for " + cardId); 
   }

   render() {
			
	this.props.children.map( (card, idx) => {
	
	  this.cards.push(card);
	  card.onExpandChange = this.handleExpand.bind(this,idx);
	} )	 
	
	return ( <div> {this.props.children} </div>);
	
   }


}


function select(state) {
  return {
    editRecord: state.uiState.editRecord,
    isExtendedDetails: state.uiState.isExtendedDetails
  }
}

Accordion.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  isExtendedDetails: React.PropTypes.bool.isRequired
}

export default connect(select)(Accordion);

