
import React , {Component} from 'react';

import {RecordStruct} from '../constants/RecordStruct';
import {handleDependentDone, showValidations, handleDependentCancel} from '../actions/AppActions';
import RaisedButton from 'material-ui/RaisedButton';
import FormField from './FormField';
import Subheader from 'material-ui/Subheader';
import Dialog from 'material-ui/Dialog';
import Paper from 'material-ui/Paper';
import {connect} from 'react-redux';


  const styles = {

	}

class EditDependent extends Component {

    constructor(props) {
          super(props);
          this.handleDependentDone = this.handleDependentDone.bind(this);
          this.handleDependentCancel = this.handleDependentCancel.bind(this);
	  this.valid = true;
	  this.validate=false;
    }

    handleDependentDone() {
	if(this.valid === false) {
		this.props.dispatch(showValidations());
	        this.validate = true;
		return;
	}
	this.props.dispatch(handleDependentDone())
    }

    handleDependentCancel() {
	this.props.dispatch(handleDependentCancel());
    }

    render() {

           var children = [];
		this.valid = true;
           RecordStruct.dependentInfo.fields.map((field) =>  {
                children.push(<FormField key={field.id} recordFieldName="editDependent" validate={this.validate} field={field} dispatch={this.props.dispatch} record={this.props.editView.editRecord.editDependent}/>)
		if(field.required === true && ((typeof this.props.editRecord.editDependent[field.id] === 'undefined') ||  !this.props.editRecord.editDependent[field.id])){
                  this.valid = false;
        	}
		return "";
            } )

        return (
                <Dialog modal={true} open={this.props.editView.isDependentDetails} title="Add Dependent" autoScrollBodyContent={true} contentStyle={{width:'100%',maxWidth:"none"}}>
                <Paper zDepth={3} style={{paddingLeft:'20px', paddingRight:'10px' , paddingBottom:'10px', width:'100%'}}>

                <div style={styles.test} className="recordview">

                <form >
                        <Subheader>Add Dependent Information</Subheader>
                        <div style={{display:'inline'}}>
                        {
                        children
                        }
                </div>  

                </form>
                </div>

                <RaisedButton label="Done" primary={true} onTouchTap={this.handleDependentDone}/>
                <RaisedButton label="Cancel"  onTouchTap={this.handleDependentCancel}/>
                </Paper>
                </Dialog>
              )
    }

}


function select(state) {
  return {
    editView: state.editView,
    editRecord: state.editView.editRecord,
  }
}



export default EditDependent;

