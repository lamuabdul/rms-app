import React, {Component} from 'react';
import {connect} from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import {Status,StatusMap, RecordStruct} from '../constants/RecordStruct';
import Menu from 'material-ui/Menu';
import Popover from 'material-ui/Popover';
import MenuItem from 'material-ui/MenuItem';



export class EventDetails extends Component {

	constructor(props){
		super(props);
	}


	render() {
		var items = []
		var fieldMap = {}; 
		if( typeof RecordStruct.eventDetails.fieldMap.get(this.props.record.eventType) !== 'undefined') {
			fieldMap = RecordStruct.eventDetails.fieldMap.get(this.props.record.eventType).fieldMap;
		Object.keys(this.props.record).forEach((key) =>  {
			var text = "";
		    if(fieldMap.get(key)) {
			text = fieldMap.get(key).name + ":" + this.props.record[key];
		    } else if (RecordStruct.events.fieldMap.get(key)) {
			text = RecordStruct.events.fieldMap.get(key).name + ":" + this.props.record[key];
		    }
		    items.push(<MenuItem primaryText={text}/>)
		});
		}   
       return (
      <div>
        <FlatButton
          onTouchTap={this.props.handleTouchTap}
          label= {this.props.value}
	  primary ={true}
        />
        <Popover
          open={this.props.open}
          anchorEl={this.props.anchorEl}
          anchorOrigin={{horizontal: 'right', vertical: 'top'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.props.handleRequestClose}
        >
          <Menu>
		{items}
          </Menu>
        </Popover>
      </div>
		)

	}

}


export default EventDetails
