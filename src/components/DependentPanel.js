import React, {Component} from 'react';
import {connect} from 'react-redux';
import EditDependent from './EditDependent';
import {handleAddDependent, handleCardExpanded, editDependent} from '../actions/AppActions';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {RecordStruct} from '../constants/RecordStruct';
import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import dateFormat from 'dateformat';
import Popover from 'material-ui/Popover/Popover';
import {Menu, MenuItem} from 'material-ui/Menu';


import classNames from 'classnames';

const STYLE = {
  borderRadius: '0px 0px 2px',
  padding: '14px 24px 24px'
}
const STYLE_BOTTOM_LEFT_GRID = {
  borderRight: '1px solid #aaa',
  borderBottom: '1px solid #aaa',
  backgroundColor: '#f7f7f7'
}
const STYLE_TOP_LEFT_GRID = {
  borderBottom: '1px solid #aaa',
  borderRight: '1px solid #aaa',
  fontWeight: 'bold'
}
const STYLE_TOP_RIGHT_GRID = {
  fontWeight: 'bold'
}

const TABLE_STYLE= {
	backgroundColor: 'rgb(255, 255, 255)',
    width: '100%',
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    tableLayout: 'auto', 
    borderLeft:'1px solid #aaa',
    fontFamily: 'Roboto, sans-serif'

}



class DependentPanel extends Component {

  constructor(props) {
	super(props)
	this.handleAddDependent = this.handleAddDependent.bind(this);
	this.handleDone = this.handleDone.bind(this);
	this.tableHeaders = RecordStruct.dependentInfo.fields.map((field) => ( field.name) )
	this.onCellClick = this.onCellClick.bind(this);

  }


   onCellClick(rowNum, columnId) {
	if( columnId == 0 ) {
	   		
	}	
   }

   handleAddDependent() {
	this.props.dispatch(handleAddDependent());
   }

   handleDone() {
	this.props.dispatch(handleCardExpanded(2,false));
   }


  render() {

	const columnCount = this.tableHeaders.length;
	const rowCount = this.props.editRecord.dependents? this.props.editRecord.dependents.length:0;
	const disabled = this.props.disabled;
	var headerCols = [];
	
	RecordStruct.dependentInfo.fields.map((field) => {
                     headerCols.push(  <TableHeaderColumn tooltip={field.name}>{field.name}</TableHeaderColumn>)
                } )
	
	return (
        <div style={{paddingTop:'10px', marginBottom:'10px'}}>

        <RaisedButton primary={true} label="Add Dependent" disabled={disabled}  onTouchTap={this.handleAddDependent}></RaisedButton>

        {this.props.editView.isDependentDetails? <EditDependent dispatch={this.props.dispatch} editRecord={this.props.editView.editRecord} editView={this.props.editView} /> : <div></div> }
	<div style={STYLE}>
	  <Table
            height='200px'
            fixedHeader={false}
            fixedFooter={false}
            selectable={false} 
	    style={TABLE_STYLE}
	    bodyStyle= {{ overflowX: 'undefined', overflowY: 'undefined'}}
	    onCellClick={this.onCellClick}
	   >
            <TableHeader
             displaySelectAll={false}
             adjustForCheckbox={false}
             enableSelectAll={false}
            >
            <TableRow>
		{RecordStruct.dependentInfo.fields.map((field) => {
              		<TableHeaderColumn key={field.id} tooltip={field.name}>{field.name}</TableHeaderColumn>
		} )}
            </TableRow>
	    </TableHeader>

	    {(rowCount > 0 ? 
	    <TableBody
              showRowHover={true}
              stripedRows={true}
	      displayRowCheckbox={false}
	      >
		{
		 this.props.editRecord.dependents.map((dep,index) => (
		   <TableRow selectable={false}  key={index} >
			{ RecordStruct.dependentInfo.fields.map((fld,ind ) => {
				
				var value = (typeof dep[fld.id] === 'undefined' || !dep[fld.id]) ?"-" :dep[fld.id];	
				value = value instanceof Date ? dateFormat(value,'d mmm yyyy') : value;
				if(fld.id === 'fullName') {
					value = <FlatButton primary={true} label={value} onTouchTap={()=> this.props.dispatch(editDependent(index))} key={ind}   disabled={disabled}  ></FlatButton>
				}
				return <TableRowColumn key={ind} >{value}</TableRowColumn>
			}) }
		   </TableRow>
		   ))
		} 
	    </TableBody>
	     : <TableBody>No Dependents Found</TableBody>)}


        >
	</Table>
	</div>

        {rowCount >0 ? (
		""
                ):"" }
        {this.props.noNext !== true  ? <div style={{display:'flex',justifyContent:'flex-end'}} >
          <FlatButton label="Next" primary={true} onTouchTap={this.handleDone}/>
        </div> : ""}

        </div>
      )
  }



}

function getClassName ( columnIndex, rowIndex ) {
  const rowClass = rowIndex % 2 === 0 ? 'evenRow' : 'oddRow'
  return classNames(rowClass, 'cell', {
    ['centeredCell']: columnIndex >= 0
  })
}



function select(state) {
  return {
    editRecord: state.editView.editRecord,
    editView : state.editView,
  }
}

DependentPanel.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  editView: React.PropTypes.object.isRequired,
  disabled: React.PropTypes.bool,
}

export default DependentPanel;


