import React , {Component} from 'react';

import {RecordStruct} from '../constants/RecordStruct';
import {handleAddressDone,showValidations,handleAddressCancel} from '../actions/AppActions';
import RaisedButton from 'material-ui/RaisedButton';
import FormField from './FormField';
import Subheader from 'material-ui/Subheader';
import Dialog from 'material-ui/Dialog';
import Paper from 'material-ui/Paper';
import {connect} from 'react-redux';

const styles = {
}



class AddressPane extends Component {


	constructor(props){
	  super(props);
	  this.handleAddressDone = this.handleAddressDone.bind(this);
	  this.handleAddressCancel = this.handleAddressCancel.bind(this);
	  this.valid = true;
  	  this.validate = false;
	}

        handleAddressDone() {
	  if(!this.valid) {
		this.validate = true;
		this.props.dispatch(showValidations());
		return;
	  }
          this.props.dispatch(handleAddressDone());
        }
  	
	handleAddressCancel() {
		this.props.dispatch(handleAddressCancel());
	}
   


	render() {
	   var children = []; 
		this.valid=true;
	   RecordStruct.addresses.fields.map((field) =>  {
        	children.push(<FormField key={field.id} recordFieldName="editAddress" validate={this.validate} field={field} dispatch={this.props.dispatch} record={this.props.editView.editRecord.editAddress}/>)
		if(field.required === true && ((typeof this.props.editView.editRecord.editAddress[field.id] === 'undefined') ||  !this.props.editView.editRecord.editAddress[field.id])){
	                this.valid = false;
        	}

		return "";
	    } )

        return (
                <Dialog modal={true} open={this.props.editView.isAddressDetails} title="Add Address" autoScrollBodyContent={true} contentStyle={{width:'100%',maxWidth:"none"}}>
                <Paper zDepth={3} style={{paddingLeft:'20px', paddingRight:'10px' , paddingBottom:'10px', width:'100%'}}>

      		<div style={styles.test} className="recordview">

     		<form >
        		<Subheader>Add Address Information</Subheader>
        		<div style={{display:'inline'}}>
        		{
         		children
        		}
        	</div>

    		</form>
     		</div>

                <RaisedButton label="Done" primary={true} onTouchTap={this.handleAddressDone}/>
                <RaisedButton label="Cancel"  onTouchTap={this.handleAddressCancel}/>
                </Paper>
                </Dialog>
              )

        
		
	}

}

/*function select(state) {
  return {
    editView: state.editView,
    editRecord: state.editView.editRecord,
  }
}
*/


export default AddressPane;
