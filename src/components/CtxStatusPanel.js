import React,{Component} from 'react';
import DependentPanel from './DependentPanel';
import {connect} from 'react-redux';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import {createRecord,goHome} from '../actions/AppActions'
import {Status, StatusMap} from '../constants/RecordStruct';
import FlatButton from 'material-ui/FlatButton';
import FormField from './FormField';
import Dialog from 'material-ui/Dialog';
import {statusChange, cancelCtxStatusChange, confirmAndSaveStatusChange, statusEdit, confirmStatusNonNested} from '../actions/AppActions';






const STYLE = { 
  borderRadius: '0px 0px 2px',
  padding: '14px 24px 24px'
}
const STYLE_BOTTOM_LEFT_GRID = {
  borderRight: '1px solid #aaa',
  borderBottom: '1px solid #aaa',
  backgroundColor: '#f7f7f7'
}
const STYLE_TOP_LEFT_GRID = {
  borderBottom: '1px solid #aaa',
  borderRight: '1px solid #aaa',
  fontWeight: 'bold'
}
const STYLE_TOP_RIGHT_GRID = {
  fontWeight: 'bold'
}

const TABLE_STYLE= {
        backgroundColor: 'rgb(255, 255, 255)',
    width: '100%',
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    tableLayout: 'auto',
    borderLeft:'1px solid #aaa',
    fontFamily: 'Roboto, sans-serif'

}

const styles = {
 overlay1: {
 position: 'fixed',
 left: 0,
 right: 0,
 top: 0,
 bottom: 0,
 backgroundColor: 'white',
 filter:'alpha(opacity=80)',
 opacity:.80,
 zIndex: 10000000,
},

}


class CtxStatusPanel extends Component{


   constructor(props){
	super(props);
	this.handleSave = this.handleSave.bind(this);
	this.cancelStatus = this.cancelStatus.bind(this);
	this.confirmStatus = this.confirmStatus.bind(this);
   } 


  handleSave(status) {
             this.props.dispatch(createRecord(this.props.editRecord));
  }


  cancelStatus() {
      this.props.dispatch(cancelCtxStatusChange());
  }

        confirmStatus(status) {
                if( this.statusValid === false ) {
                  this.validate = true;
                  return false;
                }
                this.props.dispatch(confirmAndSaveStatusChange(this.props.editView.editRecord,this.props.editView.status, status));
        }





   render() {

	var noneStyle = this.props.editView.showBusy !== true ? {display:'none'}: {};
	var status = this.props.editRecord.status;
	var nextStatus = Status.indexOf(status) + 1;
	if(nextStatus < Status.length  && typeof Status[nextStatus] !== 'undefined')
		nextStatus = Status[nextStatus];

       const actions = [
                <FlatButton
                        label="Cancel"
                        primary={true}
                        onTouchTap={this.cancelStatus}
                />,
                <FlatButton
                        label="Save"
                        primary={true}
                        onTouchTap={() => this.confirmStatus(nextStatus)}
                />,
             ];



	var dialogChild = [];
	this.statusValid=true;
        StatusMap[nextStatus].fields.map( (field) => {
                        dialogChild.push(<FormField key={field.id} editChange= {statusEdit} validate={this.validate} field={field} dispatch={this.props.dispatch} recordFieldName = "status" record={this.props.editView.status}/>)
                        if(field.required === true && ((typeof this.props.editView.status[field.id] === 'undefined') ||  !this.props.editView.status[field.id])) {
                                 this.statusValid = false;
                        }
                } );


	return (	
	<div>
                <div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" >
                    <RefreshIndicator
                        size={40}
                        left={50}
                        top={50}
                        status={this.props.editView.showBusy === true ? "loading":"hide"}
                        style={styles.refresh}
                />

                </div>

      <Toolbar style={{backgroundColor:'transparent'}}>
        <ToolbarGroup firstChild={true}>
        </ToolbarGroup>
        <ToolbarGroup>
          <ToolbarSeparator />
	  <RaisedButton label="Save"  onTouchTap={() => this.handleSave(nextStatus)} primary={true} />
	  <RaisedButton label="Cancel"  onTouchTap={this.handleCancel} primary={false} />
        </ToolbarGroup>
      </Toolbar>
                <Dialog
                   title="Add Status Information"
                   actions={actions}
                   modal={true}
                   open={ true  }
                >
                   {dialogChild}
                </Dialog>


	</div>
	)
   }

}

function select(state) {
  return {
    editRecord: state.editView.editRecord,
    editView: state.editView,
  }
}

CtxStatusPanel.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  editView: React.PropTypes.object.isRequired,
}

export default connect(select)(CtxStatusPanel);

