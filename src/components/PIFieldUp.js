
import React,{Component} from 'react';
import {handleEditChange, editRequestOpen,confirmEditRequest,showValidations, cancelEditRequest, editRequestChange} from '../actions/AppActions';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {Values} from '../constants/RecordStruct';
import DatePicker from 'material-ui/DatePicker';
import Chip from 'material-ui/Chip';
import {ListItem} from 'material-ui/List';
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {cyan500, yellow500, blue500} from 'material-ui/styles/colors';
import FormField from './FormField';


const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  inline: {
   display:'inline-flex',
   verticalAlign:'bottom'
  },

test: {
    display: 'flex',
    width:'90%',
    marginLeft:'5%',
    verticalAlign: 'bottom',
    flexDirection: 'column-reverse',
},
  
  gridList: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
  },
};


class PIFieldUp extends Component {


	constructor(props){
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.handleSelectChange = this.handleSelectChange.bind(this);
		this.handleSave = this.handleSave.bind(this);
		this.handleDate = this.handleDate.bind(this);
		this.field = props.field;
		this.dispatch = props.dispatch;
		this.recordFieldName = props.recordFieldName;
		this.editChange = props.editChange;
		if(typeof this.editChange === 'undefined' || typeof this.editChange !== 'function') {
			this.editChange = handleEditChange;
		}  
	}

	handleChange(evt ) {
		var newChange = {};
		newChange[this.field.id] = evt.target.value
		this.dispatch( editRequestChange(newChange)  );
	}

 	handleDate(evt,date){
		var newChange = {};
		newChange[this.field.id] = date;
		this.dispatch(this.editChange(newChange,this.recordFieldName));
  	}
	handleSelectChange(evt,key,payload){
		var newChange= {};
		newChange[this.field.id]= payload;
		this.dispatch( this.editChange(newChange, this.recordFieldName)  );
	}

       handleSave() {
	if((typeof this.props.editView.editCR[this.props.field.id] === 'undefined') ||  !this.props.editView.editCR[this.props.field.id] || typeof this.props.editView.editCR["reason_"+this.props.field.id] === 'undefined' || !this.props.editView.editCR["reason_"+this.props.field.id])
	{
		this.props.dispatch(showValidations())
		return;
	}
	if(this.props.editView.editCR[this.props.field.id] === this.props.record[this.props.field.id])
	{
		this.props.dispatch(showValidations());
		return;
	}
	this.props.dispatch(confirmEditRequest())	
	}
    



 render() {

            const actions = [
                <FlatButton
                        label="Cancel"
                        primary={true}
                        onTouchTap={() => this.props.dispatch(cancelEditRequest())}
                />,
                <FlatButton
                        label="Submit"
                        primary={true}
                        onTouchTap={this.handleSave }
                />,
             ];



 	const field = this.props.field;



	const required = this.props.field.required === true;
	const validate = this.props.validate === true;
	const isError = (validate &&  ((typeof this.props.editView.editCR === 'undefined' || typeof this.props.editView.editCR[field.id] === 'undefined') ||  !this.props.editView.editCR[field.id] || typeof this.props.editView.editCR["reason_"+field.id] === 'undefined' || !this.props.editView.editCR["reason_"+field.id]));
	const disabled = this.props.disabled;
	var value = this.props.record[field.id];

	if(field.type === 'date') {
		console.log(field)
		console.log("Value of field " + value);
		value = value.toDateString();
	}

	if(typeof this.props.editView.editRequestOpen !== 'undefined' && this.props.editView.editRequestOpen === field.id ) {
	 	return (
		  <Dialog title="Request Update of un-editable field"
                   actions={actions}
                   modal={true}
                   open={ true  }
		> 
		
		
               <div key={"CRE"+field.id} style={styles.inline}>
		<FormField key={field.id}  recordFieldName={this.props.recordFieldName} disabled={true} field={field} dispatch={this.props.dispatch} record={this.props.record}/>
                </div>
	
               <div key={"CR"+field.id} style={styles.inline}>
		<FormField key={field.id}  editChange={editRequestChange} disabled={false} recordFieldName ={this.props.recordFieldName} field={field} dispatch={this.props.dispatch} record={this.props.editView.editCR}/>
                </div>
               <div key={"REASON"+field.id} style={styles.inline}>
		<FormField key={field.id} recordFieldName={this.props.recordFieldName} editChange={editRequestChange} disabled={false} validate={validate}
				field={ {type:"string",id:"reason_"+field.id,name:"Reason for change", required:true}} 
				dispatch={this.props.dispatch} record={this.props.editView.editCR}/>
                </div>

		
		  </Dialog>
		)	
	}

		var bgcolor = this.props.editView.primaryEdit.cr && this.props.editView.primaryEdit.cr[field.id]? cyan500:"";
                return (
	
                <div key={field.id} style={styles.inline}>
		<Chip onTouchTap={()=>this.props.dispatch(editRequestOpen(field.id))} backgroundColor={bgcolor} style={{marginLeft:'15px',marginRight:'15px', marginTop:'30px', fontSize:14}}>
		<EditorModeEdit/> Edit {field.name}
		</Chip>
                </div>
               )
	}
}


PIFieldUp.propTypes = {
  record: React.PropTypes.object.isRequired,
  editChange: React.PropTypes.func.isRequired,
  editView : React.PropTypes.object,
  validate : React.PropTypes.bool,
}

export default PIFieldUp;


