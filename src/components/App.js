import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import LoginPage from './LoginPage';
import Dashboard from './Dashboard';
import '../index.css';
import '../login.css';
import './ExampleImage.css';
import { createStore, combineReducers, applyMiddleware ,compose} from 'redux';
import createHistory from 'history/createBrowserHistory'
import ReduxThunk from 'redux-thunk'
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'
import server from '../server/server';
import {setAuthState, refugeeListUpdate,usersFetched,refreshRefugeeListConfirm,rolesFetched, loadingAllRecords, processRefugeeList,setUserDetails,loadingCompleted, setErrorMessage} from '../actions/AppActions';
import logger from 'redux-logger'

import { Provider } from 'react-redux';
import   {getInitialState} from '../reducers/reducers';
import  loginReducer  from '../reducers/loginState';
import  mainView  from '../reducers/mainView';
import  uiState  from '../reducers/uiState';
import  adminView  from '../reducers/adminView';
import  userAdd  from '../reducers/userAdd';
import  roleAdd  from '../reducers/roleAdd';
import  editRecord  from '../reducers/editRecord';
import  refugee  from '../reducers/refugee';
import  errorMessage  from '../reducers/errorMessage';
import  auth  from '../auth/auth.js';
import { BrowserRouter as Router,Redirect, Route } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Sidenav from './Sidenav'
import EditRecordPane from './EditRecordPane'
import RmsTheme from './rmsTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { firebaseAuth } from '../constants/config';
import Snackbar from 'material-ui/Snackbar';
import AdminView from './AdminView';
import RoleAdd from './RoleAdd';
import UserAdd from './UserAdd';
import CtxAddressPanel from './CtxAddressPanel';
import CtxStatusPanel from './CtxStatusPanel';
import CtxDependentPanel from './CtxDependentPanel';
import CtxPrimaryPanel from './CtxPrimaryPanel';
import CtxDetailedPanel from './CtxDetailedPanel';
import CtxEventPanel from './CtxEventPanel';
import CtxSocialStatusPanel from './CtxSocialStatusPanel';
import injectTapEventPlugin from 'react-tap-event-plugin';

const history = createHistory()

const rootReducer = combineReducers({
	loginState: loginReducer,
	mainView: mainView,
	refugee: refugee,
	uiState: uiState,
	editView: editRecord,
	adminView: adminView,
	roleAdd: roleAdd,
	userAdd: userAdd,
	errorMessage: errorMessage,
} )

//const createStoreWithMiddleware = applyMiddleware(ReduxThunk)(createStore);
const createStoreWithMiddleware = compose(
  applyMiddleware(
    ReduxThunk,
    routerMiddleware(history),
    logger,
  ),

  //, devTools()
)(createStore);


const store = createStoreWithMiddleware( rootReducer);
console.log(store.getState());

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

const PrivateRoute = ({ component, ...rest }) => (
  <Route {...rest} render={props => (
    auth.isLoggedIn() ? (
      React.createElement(component, props)
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

const DefaultLayout = ({component: Component, ...rest}) => {
  return (
    <Route {...rest} render={matchProps => (
      auth.isLoggedIn()? (
      <div className="DefaultLayout">
        <Sidenav>
          <Component {...matchProps} />
        </Sidenav>
      </div>
       ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: matchProps.location }
      }}/>

        )
    )} />
  )
};

export default class App extends Component {


  constructor(props) {
 	super(props);
	this.closeErrorMessage = this.closeErrorMessage.bind(this);
  }


  componentDidMount () {
    this.removeListener = firebaseAuth().onAuthStateChanged((user) => {
      if (user) {
	const token = "";
	user.getToken().then(token => {
		store.dispatch(setAuthState(true,token));
		localStorage.token = token;
		store.dispatch(push('/dashboard'));
	 	store.dispatch(refreshRefugeeListConfirm())		
		if(store.getState().refugee.refugeeList.length == -2 ) 
	        {
			store.dispatch(loadingAllRecords());
			server.getAll(token, function(ok,response) {
				processRefugeeList(response.refugees);
				store.dispatch(refugeeListUpdate(ok,response.refugees));
				if(response.users)
					store.dispatch(usersFetched(response.users));
				if(response.roles)
					store.dispatch(rolesFetched(response.roles));
				store.dispatch(setUserDetails(ok,response.user));
				store.dispatch(loadingCompleted());
			}, true); 
		}
	});

      } else {
	 localStorage.clear();
	
      }
    })
  }

  closeErrorMessage() {
	store.dispatch(setErrorMessage(""));
  }
  
  componentWillUnmount () {
    this.removeListener()
  }


  render() {
	return (
<MuiThemeProvider muiTheme={getMuiTheme(RmsTheme)}>
  <Provider store={store}>
    <ConnectedRouter history={history}>
     <div>
       <DefaultLayout  exact path="/dashboard" component = {Dashboard}/>
       <Route path="/login" render = { (props) => (
           auth.isLoggedIn() ? ( <Redirect to={{pathname: "/dashboard", state: { from : props.location}}}/> ) : ( <LoginPage/>)
        )}/>
       <DefaultLayout  exact path="/" component={Dashboard} />
       <DefaultLayout  exact path="/addnew" component={EditRecordPane} />
       <DefaultLayout  exact path="/admin" component={AdminView} />
       <DefaultLayout  exact path="/addRole" component={RoleAdd} />
       <DefaultLayout  exact path="/addUser" component={UserAdd} />
       <DefaultLayout  exact path="/address" component={CtxAddressPanel} />
       <DefaultLayout  exact path="/dependent" component={CtxDependentPanel} />
       <DefaultLayout  exact path="/status" component={CtxStatusPanel} />
       <DefaultLayout  exact path="/primary" component={CtxPrimaryPanel} />
       <DefaultLayout  exact path="/detailed" component={CtxDetailedPanel} />
       <DefaultLayout  exact path="/event" component={CtxEventPanel} />
       <DefaultLayout  exact path="/socialStatus" component={CtxSocialStatusPanel} />
       <Snackbar
          open={store.getState().errorMessage != null && store.getState().errorMessage != "" ? true : false}
          message={store.getState().errorMessage}
	  onRequestClose={this.closeErrorMessage}
          autoHideDuration={4000}
        />

     </div>
    </ConnectedRouter>
  </Provider>
 </MuiThemeProvider>
	);

  }
}






