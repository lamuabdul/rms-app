import React,{Component} from 'react';
import EditAddress from './EditAddress';
import {connect} from 'react-redux';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import {createRecord,goHome} from '../actions/AppActions'





const STYLE = { 
  borderRadius: '0px 0px 2px',
  padding: '14px 24px 24px'
}
const STYLE_BOTTOM_LEFT_GRID = {
  borderRight: '1px solid #aaa',
  borderBottom: '1px solid #aaa',
  backgroundColor: '#f7f7f7'
}
const STYLE_TOP_LEFT_GRID = {
  borderBottom: '1px solid #aaa',
  borderRight: '1px solid #aaa',
  fontWeight: 'bold'
}
const STYLE_TOP_RIGHT_GRID = {
  fontWeight: 'bold'
}

const TABLE_STYLE= {
        backgroundColor: 'rgb(255, 255, 255)',
    width: '100%',
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    tableLayout: 'auto',
    borderLeft:'1px solid #aaa',
    fontFamily: 'Roboto, sans-serif'

}

const styles = {
 overlay1: {
 position: 'fixed',
 left: 0,
 right: 0,
 top: 0,
 bottom: 0,
 backgroundColor: 'white',
 filter:'alpha(opacity=80)',
 opacity:.80,
 zIndex: 10000000,
},

}


class CtxAddressPanel extends Component{


   constructor(props){
	super(props);
	this.handleSave = this.handleSave.bind(this);
	this.handleCancel = this.handleCancel.bind(this);
   } 


  handleSave() {
             this.props.dispatch(createRecord(this.props.editRecord));
  }

  handleCancel() {
	this.props.dispatch(goHome());
  }




   render() {

	var noneStyle = this.props.editView.showBusy !== true ? {display:'none'}: {};


	return (	
	<div>
                <div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" >
                    <RefreshIndicator
                        size={40}
                        left={50}
                        top={50}
                        status={this.props.editView.showBusy === true ? "loading":"hide"}
                        style={styles.refresh}
                />

                </div>

      <Toolbar style={{backgroundColor:'transparent'}}>
        <ToolbarGroup firstChild={true}>
        </ToolbarGroup>
        <ToolbarGroup>
          <ToolbarSeparator />
	  <RaisedButton label="Save"  onTouchTap={this.handleSave} primary={true} />
	  <RaisedButton label="Cancel"  onTouchTap={this.handleCancel} primary={false} />
        </ToolbarGroup>
      </Toolbar>
		<EditAddress dispatch={this.props.dispatch} editRecord={this.props.editView.editRecord} editView={this.props.editView} disabled={false}/>
	</div>
	)
   }

}

function select(state) {
  return {
    editRecord: state.editView.editRecord,
    editView: state.editView,
  }
}

CtxAddressPanel.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  editView: React.PropTypes.object.isRequired,
}

export default connect(select)(CtxAddressPanel);

