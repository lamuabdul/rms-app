import React, { Component } from 'react';
import styles from '../header.css';

class Header extends Component {
	
	render() {
    	return (
	   <header className={styles.header}>
		<h1>Refugee Management System</h1>	
	   </header>
	)
	}
}

export default Header
