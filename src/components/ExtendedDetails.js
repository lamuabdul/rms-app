
import React, { Component} from 'react';
import FlatButton from 'material-ui/FlatButton';
import {RecordStruct} from '../constants/RecordStruct';
import { connect } from 'react-redux';
import {handleCardExpanded} from '../actions/AppActions';
import FormField from './FormField';
const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  inline: {
   display:'inline-flex',
   verticalAlign:'bottom',
  },

test: {
    display: 'flex',
    width:'90%',
    marginLeft:'5%',
    verticalAlign: 'bottom',
    overflowY:'scroll',
    flexDirection: 'column',
},

  gridList: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
  },
};


export class ExtendedDetails extends Component {

   constructor(props) {
	super(props);
	this.handleDone = this.handleDone.bind(this);
   }

   handleDone() {
	this.props.dispatch(handleCardExpanded(1,false));
   }

   render() {
 
    var children = [];

    RecordStruct.detailedInfo.fields.map((field) =>  {
        children.push(<FormField key={field.id} recordFieldName="detailedInfo" field={field} dispatch={this.props.dispatch} record={this.props.editRecord.detailedInfo}/>)
	return "";
    } )

	return (

	
	<div style={{display:'inline'}}>
        {
	 children
	}
	<div style={{display:'flex',justifyContent:'flex-end'}} >
	  <FlatButton label="Next" primary={true} onTouchTap={this.handleDone}/>
	</div>
	</div>
	 

	)

   }




}

function select(state) {
  return {
    editRecord: state.uiState.editRecord,
    isExtendedDetails: state.uiState.isExtendedDetails
  }
}

ExtendedDetails.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  isExtendedDetails: React.PropTypes.bool.isRequired
}

export default connect(select)(ExtendedDetails);

