import React, {Component} from 'react';
import {connect} from 'react-redux';
import EditEvent from './EditEvent';
import EventDetails from './EventDetails';
import {handleAddEvent, editEvent,openEVPopCtxMenu, closeEVPopCtxMenu} from '../actions/AppActions';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {RecordStruct} from '../constants/RecordStruct';
import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import dateFormat from 'dateformat';
import Popover from 'material-ui/Popover/Popover';
import {Menu, MenuItem} from 'material-ui/Menu';


import classNames from 'classnames';

const STYLE = {
  borderRadius: '0px 0px 2px',
  padding: '14px 24px 24px'
}
const STYLE_BOTTOM_LEFT_GRID = {
  borderRight: '1px solid #aaa',
  borderBottom: '1px solid #aaa',
  backgroundColor: '#f7f7f7'
}
const STYLE_TOP_LEFT_GRID = {
  borderBottom: '1px solid #aaa',
  borderRight: '1px solid #aaa',
  fontWeight: 'bold'
}
const STYLE_TOP_RIGHT_GRID = {
  fontWeight: 'bold'
}

const TABLE_STYLE= {
	backgroundColor: 'rgb(255, 255, 255)',
    width: '100%',
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    tableLayout: 'auto', 
    borderLeft:'1px solid #aaa',
    fontFamily: 'Roboto, sans-serif'

}



class EventPanel extends Component {

  constructor(props) {
	super(props)
	this.handleAddEvent = this.handleAddEvent.bind(this);
	this.handleDone = this.handleDone.bind(this);
	//this.tableHeaders = RecordStruct.events.fields.map((field) => ( field.name) )
//	this.onCellClick = this.onCellClick.bind(this);

  }


   onCellClick(rowNum, columnId) {
	if( columnId == 0 ) {
	   		
	}	
   }

   handleAddEvent() {
	this.props.dispatch(handleAddEvent());
   }

   handleDone() {
   }


  render() {

	const rowCount = this.props.editRecord.events? this.props.editRecord.events.length:0;
	const disabled = this.props.disabled;
	var headerCols = [];
	
	RecordStruct.events.fieldMap.forEach((field,i) => {
                     headerCols.push(  <TableHeaderColumn tooltip={field.name}>{field.name}</TableHeaderColumn>)
                } )
	
	return (
        <div style={{paddingTop:'10px', marginBottom:'10px'}}>

        <RaisedButton primary={true} label="Add Event" disabled={disabled}  onTouchTap={this.handleAddEvent}></RaisedButton>

        {this.props.editView.isEventDetails? <EditEvent dispatch={this.props.dispatch} editRecord={this.props.editView.editRecord} editView={this.props.editView} /> : <div></div> }
	<div style={STYLE}>
	  <Table
            height='200px'
            fixedHeader={false}
            fixedFooter={false}
            selectable={false} 
	    style={TABLE_STYLE}
	    bodyStyle= {{ overflowX: 'undefined', overflowY: 'undefined'}}
	    onCellClick={this.onCellClick}
	   >
            <TableHeader
             displaySelectAll={false}
             adjustForCheckbox={false}
             enableSelectAll={false}
            >
            <TableRow>
		{ RecordStruct.events.fields.map( (field)  =>  {
              		return <TableHeaderColumn key={field.id} tooltip={field.name}>{field.name}</TableHeaderColumn>
		} )}
            </TableRow>
	    </TableHeader>

	    {(rowCount > 0 ? 
	    <TableBody
              showRowHover={true}
              stripedRows={true}
	      displayRowCheckbox={false}
	      >
		{
		 this.props.editRecord.events.map((dep,index) => (
		   <TableRow selectable={false}  key={index} >
			{ 
			   RecordStruct.events.fields.map( (fld,ind) => {
				var value = (typeof dep[fld.id] === 'undefined' || !dep[fld.id]) ?"-" :dep[fld.id];	
				value = value instanceof Date ? dateFormat(value,'d mmm yyyy') : value;
				if(fld.type === 'date') {
	
                                return <EventDetails value={value}  id={ind} key={ind}
                                handleTouchTap={(event) => (this.props.dispatch(openEVPopCtxMenu(event, index)))} handleSelect={console.log("")}
                                handleRequestClose={()=> (this.props.dispatch(closeEVPopCtxMenu(index)))} record={dep}
                                anchorEl={this.props.editView.ctxAnchor} open={this.props.editView.ctxMenuOpen && this.props.editView.key === index} status={dep.status} />



				} else {
				return <TableRowColumn key={ind} >{value}</TableRowColumn>
				}
			} ) 
			}

		   </TableRow>
		   ))
		} 
	    </TableBody>
	     : <TableBody>No Events Found</TableBody>)}


        >
	</Table>
	</div>

        {rowCount >0 ? (
		""
                ):"" }
        <div style={{display:'flex',justifyContent:'flex-end'}} >
        </div>

        </div>
      )
  }



}

function getClassName ( columnIndex, rowIndex ) {
  const rowClass = rowIndex % 2 === 0 ? 'evenRow' : 'oddRow'
  return classNames(rowClass, 'cell', {
    ['centeredCell']: columnIndex >= 0
  })
}



function select(state) {
  return {
    editRecord: state.editView.editRecord,
    editView : state.editView,
  }
}

EventPanel.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  editView: React.PropTypes.object.isRequired,
  disabled: React.PropTypes.bool,
}

export default EventPanel;


