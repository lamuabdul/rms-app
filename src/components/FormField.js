
import React,{Component} from 'react';
import {handleEditChange} from '../actions/AppActions';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {Values} from '../constants/RecordStruct';
import DatePicker from 'material-ui/DatePicker';



const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  inline: {
   display:'inline-flex',
   verticalAlign:'bottom'
  },

test: {
    display: 'flex',
    width:'90%',
    marginLeft:'5%',
    verticalAlign: 'bottom',
    flexDirection: 'column-reverse',
},
  
  gridList: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
  },
};


class FormField extends Component {


	constructor(props){
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.handleSelectChange = this.handleSelectChange.bind(this);
		this.handleCheckBox = this.handleCheckBox.bind(this);
		this.handleDate = this.handleDate.bind(this);
		this.field = props.field;
		this.dispatch = props.dispatch;
		this.recordFieldName = props.recordFieldName;
		this.editChange = props.editChange;
		if(typeof this.editChange === 'undefined' || typeof this.editChange !== 'function') {
			this.editChange = handleEditChange;
		}  
	}

	handleChange(evt ) {
		var newChange = {};
		newChange[this.field.id] = evt.target.value
		this.dispatch( this.editChange(newChange, this.recordFieldName)  );
	}

 	handleDate(evt,date){
		var newChange = {};
		newChange[this.field.id] = date;
		this.dispatch(this.editChange(newChange,this.recordFieldName));
  	}
	handleSelectChange(evt,key,payload){
		var newChange= {};
		newChange[this.field.id]= payload;
		this.dispatch( this.editChange(newChange, this.recordFieldName)  );
	}

       handleCheckBox(evt, isInputChecked) {
		var newChange= {};
		newChange[this.field.id] = isInputChecked;
		this.dispatch( this.editChange(newChange, this.recordFieldName)  );
	}
    

 render() {
 	const field = this.props.field;
	const required = this.props.field.required === true;
	const validate = this.props.validate === true;
	const isError = (validate && required && ((typeof this.props.record[field.id] === 'undefined') ||  !this.props.record[field.id]));
	const disabled = this.props.disabled;

	switch(field.type) {
           case "string" :
           case "phone":
           case "email":
	   case "float":
           case "country":
                {
                return (
	
                <div key={field.id} style={styles.inline}>
                        <TextField type="text" 
			 disabled={disabled}
			 style={{marginLeft:'10px',marginRight:'10px', fontSize:14}} 
			 name={field.id} onChange={this.handleChange} floatingLabelText={field.name} 
			 errorText= {isError ? "This field is required":null}
			 id={field.id} value={this.props.record[field.id]} />
                </div>
               )
           }
           case "boolean": {
                return (
                <div key={field.id} style={styles.inline}>
                <Checkbox
		disabled={disabled}
                label={field.name}
                checked={this.props.record[field.id]}
                value={this.props.record[field.id]}
		onCheck={this.handleCheckBox}
		style={{marginLeft:'10px',marginRight:'10px', fontSize:14}}
                />
                </div>
                )
           }
          case "enum" : {

        return (
                <div key={field.id} style={styles.inline}>
                <SelectField autoWidth={true}
		disabled={disabled}
                  floatingLabelText={field.name}
                  hintText={field.name}
                  value={this.props.record[field.id]}
		   errorText={isError ? "This field is required": null}
		  onChange={this.handleSelectChange}
		 style={{marginLeft:'10px',marginRight:'10px',fontSize:14}}
                >
                {  
                        Values[field.id].map( (val,i) =>  (
                        <MenuItem key={i}value={val} primaryText={val} />)
                        )
                }      
                </SelectField>
                </div>
                )

          }
          case "date": {
		return (
                <div key={field.id} style={styles.inline}>
		<DatePicker key={field.id} autoOk={true} 
		disabled={disabled}
		container='inline'
		textFieldStyle={{marginLeft:'10px',marginRight:'10px',fontSize:14}} 
 		errorText={isError ? "This field is required": null}
		onChange={this.handleDate} value={this.props.record[field.id]} floatingLabelText={field.name}/>
		</div>
		)

	  }
          default: {
		return (<div style={{display:'none'}}> </div>);
	  }
         }
	}
}


FormField.propTypes = {
  record: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  field: React.PropTypes.object.isRequired,
  validate: React.PropTypes.bool,
  editChange: React.PropTypes.func,
  disabled: React.PropTypes.bool,
}

export default FormField;


