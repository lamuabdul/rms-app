import React, { Component } from 'react';

import AccountCircle from 'material-ui/svg-icons/action/account-circle';
import Eject from 'material-ui/svg-icons/action/eject';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import { logout } from '../actions/AppActions';
import { connect } from 'react-redux';

class AccountSign extends Component {

  constructor(props){
    super(props);
    this.handleSignout = this.handleSignout.bind(this)
  }

  componentWillMount(){   
  }


  componentWillReceiveProps(nextProps){
  }

	render() {

    	return (
  	<IconMenu
    		iconButtonElement={
      			<IconButton><AccountCircle /></IconButton>
    		}
    		targetOrigin={{horizontal: 'right', vertical: 'top'}}
    		anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  		>
    		<MenuItem primaryText="Help" />
    		<MenuItem leftIcon={<Eject/>} primaryText="Sign out" onTouchTap={(event) => this.handleSignout(event)} />
  	</IconMenu>

	)
	}

  handleSignout(evt) {
	evt.preventDefault();
    console.log("Account clicked")
	this.props.dispatch(logout());
  }
}
function select(state) {
  return {
  };
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(AccountSign);



