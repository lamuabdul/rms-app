import React,{Component} from 'react';
import {connect} from 'react-redux';
import Paper from 'material-ui/Paper';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import IconLocationOn from 'material-ui/svg-icons/communication/location-on';
import SocialPeople from 'material-ui/svg-icons/social/people';
import SocialPersonAdd from 'material-ui/svg-icons/social/person-add';
import SocialGroupAdd from 'material-ui/svg-icons/social/group-add';
import ContentAdd from 'material-ui/svg-icons/content/add';
import MapsDirectionsWalk from 'material-ui/svg-icons/maps/directions-run';
import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import {User,RecordStruct,Rights,Role} from '../constants/RecordStruct';
import Refresh from 'material-ui/svg-icons/av/loop';
import {cyan500, yellow500, blue500} from 'material-ui/styles/colors';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import Subheader from 'material-ui/Subheader';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import {addUser, addRole,editRole, editUser, goHome} from '../actions/AppActions';
import Chip from 'material-ui/Chip';
import RoleAdd from './RoleAdd';
import { push } from 'react-router-redux';
import ActionHome from 'material-ui/svg-icons/action/home';


const favoritesIcon = <FontIcon className="material-icons">audittrail</FontIcon>;
const nearbyIcon = <IconLocationOn />;

const TABLE_STYLE= {
        backgroundColor: 'rgb(255, 255, 255)',
    width: '100%',
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    tableLayout: 'auto',
    fontFamily: 'Roboto, sans-serif'

}

const style = {
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
  cell: {
  whiteSpace: 'normal',
  wordWrap: 'break-word',
  display:'flex',
  flexWrap:'wrap',
   },
  chip: {
    margin: 4,
  },



}



class AdminView extends Component {


	constructor(props){
		super(props);
		this.onAddEvent = this.onAddEvent.bind(this);
		this.redirectHome = this.redirectHome.bind(this);
	}

	componentWillReceiveProps() {

	}

	onAddEvent (e, c) {
		if(c.key ==='user') {
			this.props.dispatch(addUser(this.props.roles)); 
			this.props.dispatch(push('/addUser'))

		} else {
			this.props.dispatch(addRole()); 
			this.props.dispatch(push('/addRole'))
		}
	}

  redirectHome() {
        this.props.dispatch(goHome());
  }



	render() {


    return (

     <div>

      <Toolbar style={{backgroundColor:'transparent'}}>
        <ToolbarGroup firstChild={true}>
	<ToolbarTitle text="Users, Roles and Permissions" />
        </ToolbarGroup>
        <ToolbarGroup>
        <IconButton onTouchTap={this.redirectHome} tooltip="Go Home" >
         <ActionHome color="rgb(0, 188, 212)"/>
        </IconButton>

            <IconButton tooltip="Refresh" onTouchTap={this.refreshData}>
              <Refresh color={cyan500}/>
            </IconButton>
            <RefreshIndicator
                size={40}
                left={10}
                top={0}
                status={this.props.loadingAllRecords === true ? "loading":"hide"}
                style={style.refresh}
            />
            <IconMenu
            iconButtonElement={
                <IconButton><SocialPersonAdd color={cyan500}/></IconButton>
                }
            targetOrigin={{horizontal: 'right', vertical: 'top'}}
            anchorOrigin={{horizontal: 'right', vertical: 'center'}}
	    onItemTouchTap={this.onAddEvent}
          >
                <MenuItem key='user' primaryText="Add User" />
                <MenuItem key='role' primaryText="Add Role" />
          </IconMenu>


          <ToolbarSeparator />
        </ToolbarGroup>
      </Toolbar>

  <div style={{display:'flex',flexDirection:'column'}}>
	
	<div>
	<Subheader> Users </Subheader>
	 <div>
	{ typeof this.props.users != 'undefined' && this.props.users.length > 0 ?  
	 
          <Table
            height='200px'
            fixedHeader={false}
            fixedFooter={false}
            selectable={false}
            style={TABLE_STYLE}
            bodyStyle= {{ overflowX: 'undefined', overflowY: 'undefined'}}
            showBorder={true}
           >
            <TableHeader
             displaySelectAll={false}
             adjustForCheckbox={false}
             enableSelectAll={false}
            >
            <TableRow>
                <TableHeaderColumn key='name' >Name</TableHeaderColumn>
                <TableHeaderColumn key='email' >Email</TableHeaderColumn>
                <TableHeaderColumn key='role' >Role</TableHeaderColumn>
            </TableRow>
            </TableHeader>
            <TableBody
              showRowHover={true}
              stripedRows={true}
              displayRowCheckbox={false}
              >
		{
		this.props.users.map( (user,index) => (
			<TableRow selectable={false}  key={index} >
                        <TableRowColumn key={-1}><FlatButton onTouchTap= {() => {this.props.dispatch(editUser(user));this.props.dispatch(push('/addUser'))}} primary={true}>{user.name}</FlatButton></TableRowColumn>
                        <TableRowColumn key={0}>{user.email}</TableRowColumn>
                        <TableRowColumn key={2}>{user.role}</TableRowColumn>
			</TableRow>	
		) )


		}
	     </TableBody>
	    </Table>
		: <div> <Subheader> No Users Defined, Add users to see list</Subheader> </div>
	}
	 </div>
	</div>
	
	<div>
	<Subheader> Roles </Subheader>
	<div>
	{ typeof this.props.roles !== 'undefined' && this.props.roles.length > 0 ?  
	 
          <Table
            height='200px'
            fixedHeader={false}
            fixedFooter={false}
            selectable={false}
            style={TABLE_STYLE}
            bodyStyle= {{ overflowX: 'undefined', overflowY: 'undefined'}}
            showBorder={true}
           >
            <TableHeader
             displaySelectAll={false}
             adjustForCheckbox={false}
             enableSelectAll={false}
            >
            <TableRow>
                <TableHeaderColumn key='id' >Id</TableHeaderColumn>
                <TableHeaderColumn key='name' >Name</TableHeaderColumn>
            </TableRow>
            </TableHeader>
            <TableBody
              showRowHover={true}
              stripedRows={true}
              displayRowCheckbox={false}
              >
		{
		this.props.roles.map( (role,index) => (
			<TableRow selectable={false}  key={index} >
                        <TableRowColumn key={-1}><FlatButton onTouchTap= {() => {this.props.dispatch(editRole(role));this.props.dispatch(push('/addRole'))}} primary={true}>{role.id}</FlatButton></TableRowColumn>
                        <TableRowColumn key={0}>{role.name}</TableRowColumn>
			</TableRow>	
		) )


		}
	     </TableBody>
	    </Table>
		: <div> <Subheader> No Roles Defined, Add roles to see list</Subheader> </div>
		}
	</div>
	</div>
	

  </div>
      <Paper zDepth={1}>
        <BottomNavigation selectedIndex={0}>
          <BottomNavigationItem
            label="Users"
            icon={<SocialPeople/>}
            onTouchTap={() => this.select(0)}
          />
          <BottomNavigationItem
            label="Audit Trail"
            icon={<MapsDirectionsWalk/>}
            onTouchTap={() => this.select(1)}
          />
        </BottomNavigation>
      </Paper>
     </div>
    );


	// Two Tabs 1) Users, 2) Audit Trail	
	// Users 
	// Table with list of users and details
	// Buttons to Add User, Delete User, Edit User
	// Edit/Add user - change everything except email
	// Add Rights, Groups 
	// Create Groups 
	// Group Name , Read rights, Write rights
	
	}


}

function select(state) {
  return {
     users: state.loginState.users,
     addUser: state.adminView.addUser,
     addRole: state.adminView.addRole,
     roles: state.loginState.roles,
  }

}

AdminView.propTypes = {
    users: React.PropTypes.array,
    roles: React.PropTypes.array,

}

export default connect(select)(AdminView);

