import React, {Component} from 'react';
import {connect} from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import {RecordStruct, Status,StatusMap} from '../constants/RecordStruct';
import Menu from 'material-ui/Menu';
import Popover from 'material-ui/Popover';
import MenuItem from 'material-ui/MenuItem';
import chunk from 'lodash';


export class MainViewContextMenu extends Component {

	constructor(props){
		super(props);
		this.hasEditPerm = this.hasEditPerm.bind(this);
		this.hasOnlyViewPerm = this.hasOnlyViewPerm.bind(this);
		this.hasObjectPerm = this.hasObjectPerm.bind(this);
		this.hasAnyDetailedEdit = this.hasAnyDetailedEdit.bind(this);
		this.hasAnyBasicEdit = this.hasAnyBasicEdit.bind(this);
		this.hasAnyDetailedView = this.hasAnyDetailedView.bind(this);
		this.hasAnyBasicView = this.hasAnyBasicView.bind(this);
	}

	hasEditPerm(perm) {
		return this.props.permMap && ( this.props.permMap.has("EDIT_"+perm) || this.props.permMap.has("EDIT_ALL"));
	}

	hasOnlyViewPerm(perm) {
		return this.props.permMap && !this.props.permMap.has("EDIT_"+perm) && !this.props.permMap.has("EDIT_ALL") && ( this.props.permMap.has("VIEW_"+perm) || this.props.permMap.has("VIEW_ALL"));
	}

	hasObjectPerm(perm) {
		return this.props.permMap && this.props.permMap.has(perm);
	}

	hasAnyDetailedEdit() {
		return this.props.permMap.has("EDIT_ALL") || chunk.intersection(this.props.editFields,RecordStruct.detailedInfo.fieldNames).length > 0
	}

	hasAnyBasicEdit() {
		return this.props.permMap.has("EDIT_ALL") || chunk.intersection(this.props.editFields,RecordStruct.basicInfo.fieldNames).length > 0
	}

	hasAnyDetailedView() {
		return this.props.permMap.has("VIEW_ALL") || chunk.intersection(this.props.viewFields,RecordStruct.detailedInfo.fieldNames).length > 0
	}

	hasAnyBasicView() {
		return this.props.permMap.has("VIEW_ALL") || chunk.intersection(this.props.viewFields,RecordStruct.basicInfo.fieldNames).length > 0
	}

	render() {
	var showNextStatus = this.props.permMap.has("STATUS_"+this.props.status)
	var nextStatus = Status.indexOf(this.props.status) + 1;
	var status = "";
	if(nextStatus < Status.length  && typeof Status[nextStatus] !== 'undefined')
	  status = "Change Status to " + StatusMap[Status[nextStatus]].title;
	
	var editInfo = false;
       if(this.props.permMap.has("EDIT_ALL")) {
		editInfo = true;	
	} else {
	}	

       return (
      <div>
        <FlatButton
          onTouchTap={this.props.handleTouchTap}
          label= {this.props.value}
	  primary ={true}
        />
        <Popover
          open={this.props.open}
          anchorEl={this.props.anchorEl}
          anchorOrigin={{horizontal: 'right', vertical: 'top'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.props.handleRequestClose}
        >
          <Menu>
            { showNextStatus && status !== "" && <MenuItem onTouchTap = {(ev) => this.props.handleSelect('status',this.props.id)} primaryText={status} /> }
            { this.hasEditPerm("dependentInfo")  && <MenuItem onTouchTap = {(ev) => this.props.handleSelect('dependent',this.props.id)} primaryText="Add/View Dependent" /> }
            { this.hasOnlyViewPerm("dependentInfo") && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('dependent',this.props.id)} primaryText="View Dependent" /> }
            { this.hasObjectPerm("ADD_EVENT") && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('event',this.props.id)} primaryText="Add Event" /> }
            { this.hasObjectPerm("VIEW_EVENT") && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('event',this.props.id)} primaryText="View Events" /> }
            { this.hasOnlyViewPerm("addresses")  && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('address',this.props.id)} primaryText="View Address" /> }
            { this.hasEditPerm("addresses") && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('address',this.props.id)} primaryText="Add/Edit Address" /> }
            { this.hasAnyBasicEdit() && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('primary',this.props.id)} primaryText="Edit Primary Info" /> }
            { !this.hasAnyBasicEdit() && this.hasAnyBasicView() && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('primary',this.props.id)} primaryText="View Primary Info" /> }
            { this.hasEditPerm("SOCIAL_STATUS") && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('socialStatus',this.props.id)} primaryText="Edit Social Status" /> }
            { this.hasOnlyViewPerm("SOCIAL_STATUS")&& <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('socialStatus',this.props.id)} primaryText="View Social Status" /> }
            { this.hasAnyDetailedEdit() && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('detailed',this.props.id)} primaryText="Edit Detailed Info" /> }
            { !this.hasAnyDetailedEdit() && this.hasAnyDetailedView()  && <MenuItem  onTouchTap = {(ev) => this.props.handleSelect('detailed',this.props.id)} primaryText="View Detailed Info" /> }
          </Menu>
        </Popover>
      </div>
	)

	}

}



export default MainViewContextMenu
