

import React, { Component } from 'react';
import { changeForm } from '../actions/AppActions';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {setErrorMsg} from '../actions/AppActions';
import style from '../login.css';
import RefreshIndicator from 'material-ui/RefreshIndicator';

//import LoadingButton from './LoadingButton';
//import ErrorMessage from './ErrorMessage';
// Object.assign is not yet fully supported in all browsers, so we fallback to
// a polyfill
const assign = Object.assign ;

const styles = {

 overlay1: {
 position: 'fixed',
 left: 0,
 right: 0,
 top: 0, 
 bottom: 0,
 backgroundColor: 'white',
 filter:'alpha(opacity=80)',
 opacity:.80,
 zIndex: 10000000,
},


}

class LoginForm extends Component {

  constructor(props) {
    super(props);

    this.handleUserChange = this.handleUserChange.bind(this);
    this.handlePwdChange = this.handlePwdChange.bind(this);
    this.emitChange = this.emitChange.bind(this);
  }


  // Merges the current state with a change
  _mergeWithCurrentState(change) {
    return assign(this.props.loginState, change);
  }


  // Emits a change of the form state to the application state
  emitChange(newState) {
    this.props.dispatch(setErrorMsg(""));
    this.props.dispatch(changeForm(newState));
  }

  handleUserChange(event ) {
    var newState = assign(this.props.loginState,{
      username: event.target.value
    });
    this.emitChange(newState);
  } 

  handlePwdChange(event ) {
    var newState = assign(this.props.loginState,{
      password: event.target.value
    });
    this.emitChange(newState);
  } 

  render() {

 var noneStyle = this.props.isLoginRequested !== true ? {display:'none'}: {};
    return(
	<div>
	<div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" >
                    <RefreshIndicator
                        size={40}
                        left={50}
                        top={50}
                        status={this.props.isLoginRequested === true ? "loading":"hide"}
                        style={styles.refresh}
                />

      </div>

     <div className={style.login}>
      <form onSubmit={this._onSubmit.bind(this)}>
       <h1>Sign In!</h1>
    	  <TextField type="email" name="username" floatingLabelText="Username"  errorText = {this.props.errorMessage} onChange={this.handleUserChange} id="username" autoCorrect="off" autoCapitalize="off" spellCheck="false" hintText="you@yourcompany.com"/>
	  <TextField type="password" name="password" floatingLabelText="Password" id="password" errorText={this.props.errorMessage}  onChange={this.handlePwdChange} hintText="********" />
        <RaisedButton label="Login" primary={true} onTouchTap={this._onSubmit.bind(this)} />
      </form>
      </div>
      </div>
    );
  }

  

  // onSubmit call the passed onSubmit function
  _onSubmit(evt) {
    evt.preventDefault();
    this.props.onSubmit(this.props.loginState.username, this.props.loginState.password);
  }
}

LoginForm.propTypes = {
  onSubmit: React.PropTypes.func.isRequired,
  buttonText: React.PropTypes.string.isRequired,
  loginState: React.PropTypes.object.isRequired,
  errorMessage: React.PropTypes.string,
  isLoginRequested: React.PropTypes.bool,
}

export default LoginForm;

