import React,{Component} from 'react';
import AddressPane from './AddressPane';
import {handleAddAddress, editAddress} from '../actions/AppActions';
import RaisedButton from 'material-ui/RaisedButton';
import {connect} from 'react-redux';
import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import dateFormat from 'dateformat';
import {RecordStruct} from '../constants/RecordStruct';
import FlatButton from 'material-ui/FlatButton';


const STYLE = { 
  borderRadius: '0px 0px 2px',
  padding: '14px 24px 24px'
}
const STYLE_BOTTOM_LEFT_GRID = {
  borderRight: '1px solid #aaa',
  borderBottom: '1px solid #aaa',
  backgroundColor: '#f7f7f7'
}
const STYLE_TOP_LEFT_GRID = {
  borderBottom: '1px solid #aaa',
  borderRight: '1px solid #aaa',
  fontWeight: 'bold'
}
const STYLE_TOP_RIGHT_GRID = {
  fontWeight: 'bold'
}

const TABLE_STYLE= {
        backgroundColor: 'rgb(255, 255, 255)',
    width: '100%',
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    tableLayout: 'auto',
    borderLeft:'1px solid #aaa',
    fontFamily: 'Roboto, sans-serif'

}



class EditAddress extends Component{


   constructor(props){
	super(props);
	this.handleAddAddress = this.handleAddAddress.bind(this);
   } 

   handleDone() {
   }

   handleAddAddress() {
	this.props.dispatch(handleAddAddress());
   }

   render() {
	const disabled = this.props.disabled;
	return (	
	<div style={{paddingTop:'10px'}}>
   	
	<RaisedButton primary={true} disabled={disabled} label="Add Address" onTouchTap={this.handleAddAddress}></RaisedButton>
	{this.props.editView.isAddressDetails? <AddressPane dispatch={this.props.dispatch} editView={this.props.editView}  disabled={disabled} /> : <div></div> }



        <div style={STYLE}>
          <Table
            height='200px'
            fixedHeader={false}
            fixedFooter={false}
            selectable={false}
            style={TABLE_STYLE}
            bodyStyle= {{ overflowX: 'undefined', overflowY: 'undefined'}}
           >
            <TableHeader
             displaySelectAll={false}
             adjustForCheckbox={false}
             enableSelectAll={false}
            >
            <TableRow>
                {RecordStruct.addresses.fields.map((field) => {
                   return (
                        <TableHeaderColumn key={field.id} tooltip={field.name}>{field.name}</TableHeaderColumn>
                        )
                } )}
            </TableRow>
            </TableHeader>

            {(this.props.editRecord.addresses.length > 0 ?
            <TableBody
              showRowHover={true}
              stripedRows={true}
              displayRowCheckbox={false}
              >
                {
                 this.props.editRecord.addresses.map((dep,index) => (
                   <TableRow selectable={false}  key={index} >
                        { RecordStruct.addresses.fields.map((fld,ind) => {

                                var value = (typeof dep[fld.id] === 'undefined' || !dep[fld.id]) ?"-" :dep[fld.id];
                                value = value instanceof Date ? dateFormat(value,'d mmm yyyy') : value;
				if( fld.id === 'addresses.line1') {
				  value = <FlatButton primary={true} label={value} onTouchTap={()=> this.props.dispatch(editAddress(index))} key={ind}   disabled={disabled}  ></FlatButton>
				}
                                return <TableRowColumn key={ind} >{value}</TableRowColumn>
                        }) }
                   </TableRow>
                   ))
                }
            </TableBody>
             : <TableBody>No Dependents Found</TableBody>)}


        >
        </Table>
        </div>







	</div>


	)
   }


}

/*function select(state) {
  return {
    editRecord: state.editView.editRecord,
    editView: state.editView,
  }
}*/

EditAddress.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  editView: React.PropTypes.object.isRequired,
  disabled: React.PropTypes.bool,
}

export default EditAddress;

