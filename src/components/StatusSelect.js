import React ,{Component} from 'react';
import {connect} from 'react-redux';
import SelectField from 'material-ui/SelectField';

import {RecordStruct,StatusMap} from '../constants/RecordStruct';
import {statusChange, cancelStatusChange, confirmStatusChange, statusEdit, confirmStatusNonNested} from '../actions/AppActions';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import FormField from './FormField';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  inline: {
   display:'inline-flex',
   verticalAlign:'bottom'
  },

test: {
    display: 'flex',
    width:'90%',
    marginLeft:'5%',
    verticalAlign: 'bottom',
    flexDirection: 'column-reverse',
},

  gridList: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
  },
};



class StatusSelect extends Component {

  	constructor(props) {
		super(props);
		this.selectionRenderer = this.selectionRenderer.bind(this);
		this.handleSelectChange = this.handleSelectChange.bind(this);
		this.confirmStatus = this.confirmStatus.bind(this);
		this.cancelStatus = this.cancelStatus.bind(this);
		this.validate = false;
		this.statusValid = true;
	}


        handleSelectChange(evt,key,payload){
                var newChange= {};
		if(StatusMap[payload].fields.length === 0) {
			this.props.dispatch(confirmStatusNonNested(payload));
		} else {
                	this.props.dispatch( statusChange(payload)  );
		}
        }
	
	selectionRenderer( value) {
		/*var show = "";
		Object.keys(this.props.editView.editRecord.statusMap).forEach(function(key,index) {
			key = StatusMap[key].title;
			if(index == 0 )
			   show = key;
			else
			show =   key + ":" + show ;
		})*/
		return StatusMap[value].title;
	}

	cancelStatus() {
		this.props.dispatch(cancelStatusChange());
	}

	confirmStatus() {
		if( this.statusValid === false ) {
		  this.validate = true;
		  return false;
		}
		this.props.dispatch(confirmStatusChange());
	}

	render() {

	const isError = (this.props.validate &&  ((typeof this.props.editView.editRecord.status === 'undefined') ||  !this.props.editView.editRecord.status || this.props.editView.editRecord.status==={}));
	    const actions = [
      		<FlatButton
        		label="Cancel"
        		primary={true}
        		onTouchTap={this.cancelStatus}
      		/>,
      		<FlatButton
        		label="Submit"
        		primary={true}
        		onTouchTap={this.confirmStatus}
      		/>,
    	     ];



	 var children = [];
    	 this.statusValid = true;	
	 var statusFieldObject = this.props.editView.editRecord.status;
	 if(statusFieldObject === null || typeof statusFieldObject === 'undefined') 
		statusFieldObject = {};
	var statusValue = this.props.editView.editRecord.status;
	//Object.keys(statusFieldObject).forEach( (key,index) => {
	//	statusValue = key;
	//})
		
	Object.keys(StatusMap).forEach(function(key,index) {
		children.push(
		 <MenuItem key={index} value={key} primaryText={StatusMap[key].title}/>
		);
	});

	var statusChange = this.props.editView.statusChangeRequest;
	var dialogChild = [];
	var showDialog = false;
	if(typeof statusChange !== 'undefined' && statusChange !== ""  ) {
		StatusMap[statusChange].fields.map( (field) => {
			showDialog = true;
			dialogChild.push(<FormField key={field.id} editChange= {statusEdit} validate={this.validate} field={field} dispatch={this.props.dispatch} recordFieldName = "status" record={this.props.editView.status}/>)
			if(field.required === true && ((typeof this.props.editView.status[field.id] === 'undefined') ||  !this.props.editView.status[field.id])) {
               			 this.statusValid = false;
        		}
		} ); 
		if(showDialog === false) {
			// Means the status has no nested fields 
		
		}
	}
	
	return (
               <div key='status' style={styles.inline}>
               <SelectField autoWidth={true}
		  disabled={this.props.disabled}
                  floatingLabelText="Status"
                  hintText="Status"
                  value={statusValue}
                  errorText={isError ? "This field is required": null}
                  onChange={this.handleSelectChange}
		 selectionRenderer={this.selectionRenderer}
                 style={{marginLeft:'10px',marginRight:'10px',fontSize:14}}
                >
                {
		  children
                }
                </SelectField>


		<Dialog
		   title="Add Status Information"
          	   actions={actions}
          	   modal={true}
          	   open={ showDialog  }
        	>
        	   {dialogChild}
		</Dialog>


                </div>
	      )
	}
}

function select(state) {
  return {
    editView: state.editView,
  }

}

StatusSelect.propTypes = {
  editView: React.PropTypes.object.isRequired,
  validate: React.PropTypes.bool,
  disabled: React.PropTypes.bool,
}

export default connect(select)(StatusSelect);

