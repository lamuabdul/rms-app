
import React, {Component} from 'react';
import {connect} from 'react-redux';
import { push } from 'react-router-redux'
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import {Rights, RecordStruct} from '../constants/RecordStruct';
import {addUserCancel, addUserSave, addObjectPerms,addUserValidate, addUserChange } from '../actions/AppActions';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import SelectField from 'material-ui/SelectField';
import Checkbox from 'material-ui/Checkbox';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import FlatButton from 'material-ui/FlatButton';
import _ from 'lodash';
import Chip from 'material-ui/Chip';
import RefreshIndicator from 'material-ui/RefreshIndicator';


const styles = {
  chip: {
    margin: 4,
  },
  rows: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection:'column',
  },
  item: {
    display: 'flex',
    flexFlow:'row',
    marginLeft:'5%',
    marginTop:'5%',
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection:'column',
    justifyContent: 'space-around',
  },
  second: {
	display:'table'
  },
  inline: {
   display:'inline-flex',
   verticalAlign:'bottom'
  },
 overlay1: {
 position: 'fixed',
 left: 0,
 right: 0,
 top: 0,
 bottom: 0,
 backgroundColor: 'white',
 filter:'alpha(opacity=80)',
 opacity:.80,
 zIndex: 10000000,
},


test: {
    display: 'flex',
    width:'90%',
    marginLeft:'5%',
    verticalAlign: 'bottom',
    flexDirection: 'column-reverse',
},

  gridList: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
  },
};



class UserAdd extends Component {


	constructor(props) {
		super(props);
                this.handleChange = this.handleChange.bind(this);
		this.handleSelectChange = this.handleSelectChange.bind(this);
                this.handleSave = this.handleSave.bind(this);
	}

        handleSelectChange(evt,key,payload){
                var newChange= {};
                newChange['role']= payload;
                this.props.dispatch( addUserChange(newChange)  );
        }

	handleChange(evt){
                var newChange = {};
                newChange[evt.target.id] = evt.target.value
		this.props.dispatch(addUserChange(newChange));
	}
	
	handleSave(){
		if(this.props.record.email === "" || this.props.record.name === ""  || this.props.record.role === "") {
			this.props.dispatch(addUserValidate());	
			return;
		}
		var record = this.props.record;	
		this.props.dispatch(addUserSave(record));
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.addUser === false) {
			nextProps.dispatch(push('/admin')) 
		}
	

	}

	shouldComponentUpdate(nextProps, nextState){
		return  nextProps.addUser ;
	}
	


	render() {
	   var noneStyle = this.props.userSaveRequested !== true ? {display:'none'}: {};
		
	   var isError = this.props.validate === true  && (this.props.record.name === "" || this.props.record.email === "" || this.props.record.role === ""  );
	
		
		var disabled = false;
	  return (

		<div>
	
	               <div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" >
                    <RefreshIndicator
                        size={40}
                        left={50}
                        top={50}
                        status={this.props.userSaveRequested === true ? "loading":"hide"}
                        style={styles.refresh}
                     />
                 </div>





      <Toolbar style={{backgroundColor:'transparent'}}>
        <ToolbarGroup firstChild={true}>
        <ToolbarTitle text="Add New User" />
        </ToolbarGroup>
        <ToolbarGroup>
                <RaisedButton label="Add" primary={true} onTouchTap={this.handleSave}/>
                <RaisedButton label="Cancel"  onTouchTap={() => this.props.dispatch(addUserCancel())}/>
          <ToolbarSeparator />
        </ToolbarGroup>
      </Toolbar>
		<div style={styles.rows}>
		<div style={styles.item} >
                <div key={0}  >
                        <TextField type="text"
                         disabled={disabled}
			 hintText="Name"
                         name="Name" onChange={this.handleChange} floatingLabelText={"User Name"}
			 floatingLabelFixed={true}
			 style={{marginLeft:'5%', marginRight:'5%'}}
                         errorText= {isError ? "This field is required":null}
                         id="name" value={this.props.record["name"]} />
                </div>
                <div key={1} >
                        <TextField type="text"
                         disabled={disabled}
			 hintText="e.g. email@domain.com"
			 style={{marginLeft:'5%', marginRight:'5%'}}
			 floatingLabelFixed={true}
                         name="Email" onChange={this.handleChange} floatingLabelText={"Email Id"}
                         errorText= {isError ? "This field is required":null}
                         id="email" value={this.props.record["email"]} />
                </div>
                <div key={2} >
               <SelectField autoWidth={true}
                  disabled={disabled}
                  floatingLabelText="Role"
                  hintText="Select A Role"
                  value={this.props.record['role']}
                   errorText={isError ? "This field is required": null}
                  onChange={this.handleSelectChange}
                 style={{marginLeft:'10px',marginRight:'10px',fontSize:14}}
                >
                {
                        this.props.roles.map( (val,i) =>  (
                        <MenuItem key={i}value={val.id} primaryText={val.name} />)
                        )
                }
                </SelectField>

                </div>
			</div>
		</div>
		</div>
	)
	}




}

function select(state) {
  return {
	record: state.userAdd.newUser,
	roles: state.loginState.roles,
	validate: state.userAdd.validate,
	addUser: state.userAdd.addUser,
	userSaveRequested: state.userAdd.userSaveRequested,
  }

}

UserAdd.propTypes = {
	record: React.PropTypes.object,	
	addRole: React.PropTypes.bool,
}

export default connect(select)(UserAdd);




