import React, {Component} from 'react';
import {connect} from 'react-redux';


import {photoSelected, setErrorMessage } from '../actions/AppActions';
import RaisedButton from 'material-ui/RaisedButton';
import { filter } from 'lodash';


var max_width = 220;
var max_height = 220;


var styles = {
  photoUpload: {
    cursor: 'pointer',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
  },
  preview: {
    background: '#efefef',
    backgroundSize: '100%',
    display: 'inline-block',
    height: '120px',
    width: '120px',
}


}

class PhotoUpload extends Component {
	
	constructor(props) {
	  super(props);
	  this.handleFileSelect = this.handleFileSelect.bind(this);
	  this.processfile = this.processfile.bind(this);
	  this.resizeMe = this.resizeMe.bind(this);
	  this.readfiles = this.readfiles.bind(this);
	}

	handleFileSelect ( e) {
		this.props.dispatch(setErrorMessage(""));
		this.readfiles(e.target.files);
	}


 	processfile(file) {
  
    		if( !( /image/i ).test( file.type ) )
        	{
			this.props.dispatch(setErrorMessage(  "File "+ file.name +" is not an image."));
            		//alert( "File "+ file.name +" is not an image." );
            		return false;
        	}

    		// read the files
    		var reader = new FileReader();
    		reader.readAsArrayBuffer(file);
    
   		 reader.onload =  (event) =>  {
      		// blob stuff
      		var blob = new Blob([event.target.result]); // create blob...
      		window.URL = window.URL || window.webkitURL;
      		var blobURL = window.URL.createObjectURL(blob); // and get it's URL
      
      		// helper Image object
      		var image = new Image();
      		image.src = blobURL;
      		image.onload = ()  => {
        		// have to wait till it's loaded
        		var resized = this.resizeMe(image); // send it to canvas
			this.props.dispatch(photoSelected(resized));
			//var decoded  = atob(resized.substr(23)); 
			//console.log(decoded.length);
      		}
    		};
	}

 	readfiles(files) {
    	   for (var i = 0; i < files.length; i++) {
      		this.processfile(files[i]); // process each file at once
    	   }
	}


        dataURItoBlob(dataURI) {
    		var byteString;
    		if (dataURI.split(',')[0].indexOf('base64') >= 0)
        		byteString = atob(dataURI.split(',')[1]);
    		else
        		byteString = unescape(dataURI.split(',')[1]);

    		// separate out the mime component
    		var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    		// write the bytes of the string to a typed array
    		var ia = new Uint8Array(byteString.length);
    		for (var i = 0; i < byteString.length; i++) {
        		ia[i] = byteString.charCodeAt(i);
    	}

    		return new Blob([ia], {type:mimeString});
	}



	 resizeMe(img) {
  
  	  var canvas = document.createElement('canvas');

  	  var width = img.width;
  	  var height = img.height;

  	  // calculate the width and height, constraining the proportions
  	  if (width > height) {
    	  if (width > max_width) {
      		//height *= max_width / width;
      		height = Math.round(height *= max_width / width);
      		width = max_width;
    	  }  
  	  } else {
    		if (height > max_height) {
      		//width *= max_height / height;
      		width = Math.round(width *= max_height / height);
      		height = max_height;
    	  }
  	 }
  
  	 // resize the canvas and draw the image data into it
  	 canvas.width = width;
  	 canvas.height = height;
  	 var ctx = canvas.getContext("2d");
  	 ctx.drawImage(img, 0, 0, width, height);
  	  return canvas.toDataURL("image/jpeg",0.7);
	}	




	render() {
		var p = this.props.photo;
		if(typeof p !== 'undefined' && p !== null && p !== ""  && p.includes("rms-profile/")) {
			p = p + this.props.photoFile;
		} else {

		} 
		var style = typeof p !== 'undefined' && p !== null && p !== ""  ?
		{ backgroundImage : 'url(' + p + ')', backgroundSize: '100% 100%' } : undefined;
	

		return (
		<div style={{display:'inline-flex',flexDirection:'column-reverse',alignItems:'center'}}>
		<RaisedButton 
			disabled={this.props.disabled}
			label="Add Photo" 
			labelPosition="before" 
			style={styles.button} 
			primary={true}
			containerElement="label" > 
			<input disabled={this.props.disabled} accept="image/*;capture=camera" type="file" onChange = {this.handleFileSelect} style={styles.photoUpload} /> 
		</RaisedButton>

		<div id="preview" style={{...styles.preview, ...style }}> 
		</div>
		</div>
		);
	}


}
function select(state) {
  return {
    photo: state.editView.editRecord.photoUrl,
    photoFile: state.editView.editRecord.key,
    errorMessage: state.errorMessage,
  }
}

PhotoUpload.propTypes = {
  photo: React.PropTypes.string,
  disabled: React.PropTypes.bool,
  photoFile: React.PropTypes.number,
}

export default connect(select)(PhotoUpload);





