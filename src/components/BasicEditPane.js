import Divider from 'material-ui/Divider';
import React, {Component} from 'react';
import FlatButton from 'material-ui/FlatButton';
import {Card, CardHeader, CardText} from 'material-ui/Card';



class BasicEditPane extends Component {



   render() {

	var children = [];
	var valid = true;
	var field = this.props.field;
        children.push(<FormField key={field.id} validate={thieditRecords.validate} field={field} dispatch={this.props.dispatch} record={this.props.editRecord}/>)
        if(field.required === true && ((typeof this.props.record[field.id] === 'undefined') ||  !this.props.record[field.id])){
                valid = false;
        }
        return "";
	
	
  <Card initiallyExpanded={this.props.initiallyExpanded}
        onExpandChange={this.props.onExpand(expanded)}
        expanded={this.props.expanded} >
    <CardHeader
      title={this.props.title}
      actAsExpander={this.props.actAsExpander}
    />
    <CardText expandable={this.props.expandable}>
        <Divider/>
        <div style={{display:'inline'}}>
        {children}
        </div>
        <div style={{display:'flex',justifyContent:'flex-end'}} >
        <FlatButton label="Next" primary={true} onTouchTap={(e) => {e.preventDefault();this.handleNext(0,false)}}/>
        </div>
    </CardText>
  </Card>


   }


}

BasicEditPane.propTypes = {
  field: React.PropTypes.object.isRequired,
  record: React.PropTypes.object.isRequired,
  valid: React.PropTypes.function.isRequired,
  initiallyExpanded: React.PropTypes.boolean,
  actAsExpander : React.PropTypes.boolean,
  title : React.PropTypes.string,
  expanded : React.PropTypes.boolean,
  nextClicked : React.PropTypes.function,
  onExpand: React.PropTypes.function,
}

export default BasicEditPane;

