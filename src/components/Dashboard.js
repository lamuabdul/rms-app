

import React, { Component } from 'react';
import Sidenav from './Sidenav.js';
import Header from './Header.js';
import MainView from './MainView.js';
import style from '../sidenav.css';
import { connect } from 'react-redux';
//import { changeForm } from '../actions/AppActions';
//import LoadingButton from './LoadingButton';
//import ErrorMessage from './ErrorMessage';
// Object.assign is not yet fully supported in all browsers, so we fallback to
// a polyfill
const assign = Object.assign ;

class Dashboard extends Component {

  constructor(props) {
    super(props);
  }



  render() {
    const props = this.props;
    const dispatch = this.props.dispatch;
    return(
      <div id="sdlayout">
	<MainView dispatch={dispatch} refugeeList={this.props.refugeeList}/>
      </div>

    );
  }

}

function select(state) {
  return {
    refugeeList: state.refugee.refugeeList,
    uiState: state.uiState
  };
}


Dashboard.propTypes = {
  refugeeList: React.PropTypes.array.isRequired,
  uiState: React.PropTypes.object.isRequired
}

export default connect(select)(Dashboard);

