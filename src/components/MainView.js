
import React, { Component } from 'react';
import MainToolBar from './MainToolBar.js';
import {connect} from 'react-redux';
import {clearCtxMenu,addressPanel, statusPanel,socialStatusPanel,  primaryPanel, detailedPanel, eventPanel,dependentPanel,filterRefugeeList,addNewApplicant,setErrorMessage,viewRecord,filterSearchId,filterSearchName,refreshRefugeeListConfirm,openMVPopCtxMenu,closeMVPopCtxMenu,refreshRefugeeListRequest,filterSearchCountry} from '../actions/AppActions'
import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {RecordStruct, StatusMap} from '../constants/RecordStruct';
import dateFormat from 'dateformat';
import Divider from 'material-ui/Divider';
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import DropDownMenu from 'material-ui/DropDownMenu';
import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import TextField from 'material-ui/TextField';
import ExampleImage from './ExampleImage';
import exStyles from './ExampleImage.css';
import IconButton from 'material-ui/IconButton';
import Refresh from 'material-ui/svg-icons/av/loop';
import {cyan500, yellow500, blue500} from 'material-ui/styles/colors';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import ActionSettings from 'material-ui/svg-icons/action/settings';
import MainViewContextMenu from './MainViewContextMenu';


const STYLE = {
  borderRadius: '0px 0px 2px',
  padding: '14px 24px 24px'
}
const TABLE_STYLE= {
        backgroundColor: 'rgb(255, 255, 255)',
    width: '100%',
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    tableLayout: 'auto',
    fontFamily: 'Roboto, sans-serif'

}

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  inline: {
   display:'inline-flex',
   verticalAlign:'bottom'
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
    marginLeft: '40%',
    marginTop: '15%',
  },
 overlay1: {
 position: 'fixed',
 left: 0,
 right: 0,
 top: 0, 
 bottom: 0,
 backgroundColor: 'white',
 filter:'alpha(opacity=80)',
 opacity:.80,
 zIndex: 10000000,
},
}


const style = {
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },

}


class MainView extends Component {

  constructor(props) {
    super(props);
    this.addNewApplicant = this.addNewApplicant.bind(this)
    this.handleChangeFilter = this.handleChangeFilter.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeCountry = this.handleChangeCountry.bind(this)
    this.handleSearchById = this.handleSearchById.bind(this)
    this.refreshData = this.refreshData.bind(this)
    this.refreshCancel = this.refreshCancel.bind(this)
    this.refreshConfirm = this.refreshConfirm.bind(this)
    this.closingSnack = this.closingSnack.bind(this)
    this.handleContextMenuChange = this.handleContextMenuChange.bind(this)

  }


  handleContextMenuChange(type, key) {
	var refugeeRecord = this.props.refugeeList[this.props.refugeeMap[key]];
	this.props.dispatch(clearCtxMenu());
	switch(type) {
	case 'address':
		this.props.dispatch(addressPanel(refugeeRecord))
		return ;
	case 'dependent':
		this.props.dispatch(dependentPanel(refugeeRecord))
		return ;
	case 'status':
		this.props.dispatch(statusPanel(refugeeRecord))
		return ;
	case 'primary':
		var flds = []
		flds.push.apply(flds,RecordStruct.basicInfo.fields.filter((fld) => ((this.props.mainView.permMap.has("VIEW_ALL") || this.props.mainView.permMap.has("VIEW_"+fld.id) || this.props.mainView.permMap.has("EDIT_"+fld.id) || this.props.mainView.permMap.has("EDIT_ALL")  ) )))
		//flds.push.apply(flds,RecordStruct.detailedInfo.fields.filter((fld) => ((this.props.mainView.permMap.has("VIEW_ALL") || this.props.mainView.permMap.has("VIEW_"+fld.id) ) )))
		this.props.dispatch(primaryPanel(refugeeRecord, flds))
		return ;
	case 'detailed':
		var flds = []
		flds.push.apply(flds,RecordStruct.detailedInfo.fields.filter((fld) => ((this.props.mainView.permMap.has("VIEW_ALL") || this.props.mainView.permMap.has("VIEW_"+fld.id) || this.props.mainView.permMap.has("EDIT_"+fld.id) || this.props.mainView.permMap.has("EDIT_ALL")  ) )))
		this.props.dispatch(detailedPanel(refugeeRecord,flds,true))
		return ;
	case 'event':
		this.props.dispatch(eventPanel(refugeeRecord))
		return ;
	case 'socialStatus':
		this.props.dispatch(socialStatusPanel(refugeeRecord))
		return ;
	default: 
		console.log("Error: Unknown type for context menu change " + type);

	}
  }

  addNewApplicant() {
     this.props.dispatch(addNewApplicant());
  }

 refreshConfirm() {
	this.props.dispatch(refreshRefugeeListConfirm());
 }
 
 refreshCancel() {
	this.props.dispatch(refreshRefugeeListRequest(false));
 }

  closingSnack() {
        this.props.dispatch(setErrorMessage(""));
  }

 refreshData() {
	this.props.dispatch(refreshRefugeeListRequest(true));	
 }

 handleChange(evt ) {
        this.props.dispatch( filterSearchName(evt.target.value)  );
 }

 handleSearchById(evt ) {
        this.props.dispatch( filterSearchId(evt.target.value)  );
 }

 handleChangeCountry(evt ) {
        this.props.dispatch( filterSearchCountry(evt.target.value)  );
 }

 handleChangeFilter(event, index, value) {
	this.props.dispatch(filterRefugeeList(value));
 }

  render() {

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.refreshCancel}
      />,
      <FlatButton
        label="Confirm"
        primary={true}
        onTouchTap={this.refreshConfirm}
      />,
    ];

var noneStyle = this.props.loadingAllRecords !== true ? {display:'none'}: {};
	const viewAll = this.props.mainView.permMap && (this.props.mainView.permMap.has("VIEW_ALL") || this.props.mainView.permMap.has("EDIT_ALL"))
	const viewPhoto = this.props.mainView.permMap && (this.props.mainView.permMap.has("VIEW_PHOTO") || this.props.mainView.permMap.has("EDIT_PHOTO") || viewAll)
	const showFilters = viewAll ||  (this.props.mainView.permMap && this.props.mainView.permMap.has("VIEW_STATUS"));		

    if(this.props.loadingAllRecords === true ) {
	return (
	                <div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" >
                    <RefreshIndicator
                        size={40}
                        left={50}
                        top={50}
                        status={this.props.loadingAllRecords === true ? "loading":"hide"}
                        style={styles.refresh}
                />

                </div>

	)
    }

    return (
      <div className="mainview"> 
	                <div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" >
                    <RefreshIndicator
                        size={40}
                        left={50}
                        top={50}
                        status={this.props.loadingAllRecords === true ? "loading":"hide"}
                        style={styles.refresh}
                />

                </div>


        <Dialog
          title="Refresh Caution!!"
          actions={actions}
          modal={true}
          open={this.props.mainView.refreshRequest === true}
        >
	  Refreshing will reload entire data from cloud, Please use it with caution and only if required as the bandwidth on the cloud is costly. Confirm?
        </Dialog>


        { this.props.mainView.permMap && this.props.mainView.permMap.has("ADD_REFUGEE") && <MainToolBar addNewApplicant={this.addNewApplicant}/>}

        <div style={STYLE}>
	<br/>
	<br/>
	<br/>


      <Toolbar style={{backgroundColor:'transparent'}}>
        <ToolbarGroup firstChild={true}>
          <TextField type="text"
               name="Search" onChange={this.handleChange} floatingLabelText="Search By Name"
	       underlineShow={false}
               value={this.props.mainView.searchName} />
          <TextField type="text"
               name="Search" onChange={this.handleChangeCountry} floatingLabelText="Search By Country"
	       underlineShow={false}
               value={this.props.mainView.searchCountry} />
          <TextField type="text"
               name="Search" onChange={this.handleSearchById} floatingLabelText="Search By COVA-Id"
	       underlineShow={false}
               value={this.props.mainView.searchId} />
	  { showFilters ?
          <DropDownMenu value={this.props.mainView.filterValue} onChange={this.handleChangeFilter}
    	    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    	    anchorOrigin={{horizontal: 'right', vertical: 'center'}}>
            <MenuItem value={1} primaryText="All Records" />
            <MenuItem value={2} primaryText="New Arrivals" />
            <MenuItem value={3} primaryText="RSD Applied" />
            <MenuItem value={4} primaryText="RSD Expiry in a week" />
            <MenuItem value={5} primaryText="LTV Holders" />
            <MenuItem value={6} primaryText="LTV Expiry in a week" />
          </DropDownMenu>
	  : <div></div>}
        </ToolbarGroup>
        <ToolbarGroup>
	    <IconButton tooltip="Refresh" onTouchTap={this.refreshData}>
	      <Refresh color={cyan500}/>
    	    </IconButton>
	    <RefreshIndicator
      		size={40}
      		left={10}
      		top={0}
      		status={this.props.loadingAllRecords === true ? "loading":"hide"}
      		style={style.refresh}
    	    />
	    <IconMenu
    	    iconButtonElement={
      		<IconButton><MoreVertIcon /></IconButton>
    		}
    	    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    	    anchorOrigin={{horizontal: 'right', vertical: 'center'}}
  	  >
    		<MenuItem primaryText="Customise" leftIcon={<ActionSettings />} />
          </IconMenu>

	  <ToolbarSeparator />
        </ToolbarGroup>
      </Toolbar>



          <Table
            height='450px'
            fixedHeader={false}
            fixedFooter={false}
            selectable={false}
            style={TABLE_STYLE}
            bodyStyle= {{ overflowX: 'undefined', overflowY: 'undefined'}}
	    showBorder={true}
           >
            <TableHeader
             displaySelectAll={false}
             adjustForCheckbox={false}
             enableSelectAll={false}
            >
            <TableRow>
                { viewPhoto &&  <TableHeaderColumn key='pic' >Photo</TableHeaderColumn>}
                <TableHeaderColumn key='id' >COVA-Id</TableHeaderColumn>
                {this.props.displayFields.map((fld) => {
		   var field = RecordStruct.basicInfo.fieldMap.get(fld);
	           if (!field) {
			field = RecordStruct.detailedInfo.fieldMap.get(fld);
		   }
                   return (
                        <TableHeaderColumn key={field.id} tooltip={field.name}>{field.name}</TableHeaderColumn>
                        )
                } )}
            </TableRow>
            </TableHeader>

            {(this.props.refugeeList.length > 0 ?
            <TableBody
              showRowHover={true}
              stripedRows={true}
              displayRowCheckbox={false}
              >
                {
                 this.props.refugeeList.filter((r) => {
		  if (this.props.mainView.searchName === 'undefined' || !this.props.mainView.searchName) return true;
		  else return r.fullName.includes(this.props.mainView.searchName) 
		}).
	        filter((a) => {
		  if (this.props.mainView.searchCountry === 'undefined' || !this.props.mainView.searchCountry) return true;
		  else return a.originCountry.includes(this.props.mainView.searchCountry) 
	         }
		) .filter((b) => {
		  if (this.props.mainView.filterValue === 'undefined' || !this.props.mainView.filterValue) return true;
		  else {
			switch(this.props.mainView.filterValue) {
			case 1: 
			  return true;
			case 2: 
			  return  b.status === 'NEW_ARRIVAL';
			case 3: 
			  return typeof b.statusMap['RSD'] !== 'undefined' && typeof b.statusMap['RSD']['rsdStatus'] !== 'undefined' && b.statusMap['RSD']['rsdStatus']=== "Applied";

			case 4: 
			     
				// RSD Expiry in a week
			  return typeof b.statusMap['RSD'] !== 'undefined' && 
				typeof b.statusMap['RSD']['rsdStatus'] !== 'undefined' 
				&& b.statusMap['RSD']['rsdStatus'] === "Accepted" &&   b.statusMap['RSD']['rcExpiryDate']  instanceof Date && 
					(Math.abs(b.statusMap['RSD']['rcExpiryDate']  - new Date().getTime()) / (1000 * 3600 * 24) < 7 );
			case 5: 
				//LTV Holders
			return typeof b.statusMap['LTV'] !== 'undefined' && typeof b.statusMap['LTV']['issue'] !== 'undefined' && b.statusMap['LTV']['issue'] instanceof Date;
			case 6: 
				// LTV Expiry in a week
			return typeof b.statusMap['LTV'] !== 'undefined' 
					&& typeof b.statusMap['LTV']['expiry'] !== 'undefined' && 
					b.statusMap['LTV']['expiry'] instanceof Date && 
				(Math.abs(b.statusMap['LTV']['expiry']  - new Date().getTime()) / (1000 * 3600 * 24) < 7 );
			default:
			  return true;
			}
		    }
		} ).map((dep,index) => (
                   <TableRow selectable={false}  key={dep.key} >
			{ viewPhoto &&  <TableRowColumn key={-1}><ExampleImage src={dep.photoUrl + dep.key} /> </TableRowColumn>}
			<TableRowColumn key={-2}> 
				<MainViewContextMenu value={"COVA-"+dep.key} permMap={this.props.mainView.permMap}  editFields = {this.props.mainView.editFields} 
				viewFields = {this.props.mainView.viewFields} id={dep.key}
				handleTouchTap={(event) => (this.props.dispatch(openMVPopCtxMenu(event, dep.key)))} handleSelect={this.handleContextMenuChange}
				handleRequestClose={()=> (this.props.dispatch(closeMVPopCtxMenu(dep.key)))} 
				anchorEl={this.props.mainView.ctxAnchor} open={this.props.mainView.ctxMenuOpen && this.props.mainView.key === dep.key} status={dep.status} /> 

			</TableRowColumn>
                        { this.props.displayFields.map((ff,ind) => {
				var fld = RecordStruct.basicInfo.fieldMap.get(ff);
				if (!fld) {
					fld = RecordStruct.detailedInfo.fieldMap.get(ff);
				}
                                var value = (typeof dep[fld.id] === 'undefined' || !dep[fld.id]) ?"<NA>" :dep[fld.id];
				if(fld.type === 'date' && value !== "<NA>") {
				   value = new Date(value);
			        }
                                value = value instanceof Date ? dateFormat(value,'d mmm yyyy') : value;
				if(fld.type === 'status') {
					value = StatusMap[value].title;
				}
				if(fld.type == 'boolean') {
					value = value === true ? 'yes' : 'no';
				}
				if(fld.id === 'photoUrl') {
					value = value + dep.key;
				}
                                return <TableRowColumn key={ind} >{value}</TableRowColumn>
                        }) }
                   </TableRow>

                   ))
                }
            </TableBody>
             : <TableBody></TableBody>)}


        >
        </Table>
        </div>

        <Snackbar
          open={this.props.errorMessage != null && this.props.errorMessage != ""  ? true : false}
          message={this.props.errorMessage}
          autoHideDuration={4000}
          onRequestClose={this.closingSnack}
        />

     </div>
    );
  }

}

function select(state) {
  return {
    refugeeList: state.refugee.refugeeList,
    refugeeMap: state.refugee.refugeeMap,
    mainView: state.mainView,
    uiState: state.uiState,
    loadingAllRecords: state.mainView.loadingAllRecords,
    errorMessage: state.errorMessage,
    displayFields : state.mainView.displayFields,
  }

}

MainView.propTypes = {
  refugeeList: React.PropTypes.array.isRequired,
  refugeeMap: React.PropTypes.object.isRequired,
  uiState: React.PropTypes.object.isRequired,
  errorMessage: React.PropTypes.string,
  displayFields: React.PropTypes.array,
  loadingAllRecords: React.PropTypes.bool,
}

export default connect(select)(MainView);

