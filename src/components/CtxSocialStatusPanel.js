import React,{Component} from 'react';
import DependentPanel from './DependentPanel';
import {connect} from 'react-redux';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import {createRecord,goHome} from '../actions/AppActions'
import {RecordStruct} from '../constants/RecordStruct';
import FlatButton from 'material-ui/FlatButton';
import FormField from './FormField';
import Dialog from 'material-ui/Dialog';
import {socialStatusChange, cancelCtxSocialStatusChange, confirmAndSaveSocialStatusChange, socialStatusEdit, confirmStatusNonNested} from '../actions/AppActions';






const STYLE = { 
  borderRadius: '0px 0px 2px',
  padding: '14px 24px 24px'
}
const STYLE_BOTTOM_LEFT_GRID = {
  borderRight: '1px solid #aaa',
  borderBottom: '1px solid #aaa',
  backgroundColor: '#f7f7f7'
}
const STYLE_TOP_LEFT_GRID = {
  borderBottom: '1px solid #aaa',
  borderRight: '1px solid #aaa',
  fontWeight: 'bold'
}
const STYLE_TOP_RIGHT_GRID = {
  fontWeight: 'bold'
}

const TABLE_STYLE= {
        backgroundColor: 'rgb(255, 255, 255)',
    width: '100%',
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    tableLayout: 'auto',
    borderLeft:'1px solid #aaa',
    fontFamily: 'Roboto, sans-serif'

}

const styles = {
 overlay1: {
 position: 'fixed',
 left: 0,
 right: 0,
 top: 0,
 bottom: 0,
 backgroundColor: 'white',
 filter:'alpha(opacity=80)',
 opacity:.80,
 zIndex: 10000000,
},

}


class CtxSocialStatusPanel extends Component{


   constructor(props){
	super(props);
	this.handleSave = this.handleSave.bind(this);
	this.cancelStatus = this.cancelStatus.bind(this);
	this.confirmStatus = this.confirmStatus.bind(this);
	this.readOnly = typeof this.props.perms !== 'undefined' && this.props.perms.has("VIEW_SOCIAL_STATUS") && !this.props.perms.has("EDIT_SOCIAL_STATUS")
   } 


  handleSave(status) {
             this.props.dispatch(createRecord(this.props.editRecord));
  }


  cancelStatus() {
      this.props.dispatch(cancelCtxSocialStatusChange());
  }

        confirmStatus(status) {
                if( this.statusValid === false ) {
                  this.validate = true;
		  console.log("Invalid ");
                  return false;
                }
                this.props.dispatch(confirmAndSaveSocialStatusChange(this.props.editView.editRecord,this.props.editView.socialStatus, status));
        }





   render() {

	var noneStyle = this.props.editView.showBusy !== true ? {display:'none'}: {};
	var status = this.props.editRecord.socialStatus;

       const actions = [
                <FlatButton
                        label="Cancel"
                        primary={true}
                        onTouchTap={this.cancelStatus}
                />,
                <FlatButton
                        label="Save"
                        primary={true}
			disabled={this.readOnly}
                        onTouchTap={() => this.confirmStatus(status)}
                />,
             ];



	var dialogChild = [];
	this.statusValid=true;
        RecordStruct.socialStatus.fields.map( (field) => {
                        dialogChild.push(<FormField key={field.id} disabled={this.readOnly} editChange= {socialStatusEdit} validate={this.validate} field={field} dispatch={this.props.dispatch} recordFieldName = "socialStatus" record={this.props.editView.editRecord.socialStatus}/>)
                        if(field.required === true && ((typeof this.props.editView.editRecord.socialStatus[field.id] === 'undefined') ||  !this.props.editView.editRecord.socialStatus[field.id])) {
                                 this.statusValid = false;
                        }
                } );


	return (	
	<div>
                <div style={{ ...styles.overlay1, ...noneStyle }} className="recordview" >
                    <RefreshIndicator
                        size={40}
                        left={50}
                        top={50}
                        status={this.props.editView.showBusy === true ? "loading":"hide"}
                        style={styles.refresh}
                />

                </div>

      <Toolbar style={{backgroundColor:'transparent'}}>
        <ToolbarGroup firstChild={true}>
        </ToolbarGroup>
        <ToolbarGroup>
          <ToolbarSeparator />
	  <RaisedButton label="Save"  onTouchTap={() => this.handleSave(status)} primary={true} />
	  <RaisedButton label="Cancel"  onTouchTap={this.handleCancel} primary={false} />
        </ToolbarGroup>
      </Toolbar>
                <Dialog
                   title={this.readOnly? "View Social Status" :"Add Social Status"}
                   actions={actions}
                   modal={true}
                   open={ true  }
                >
                   {dialogChild}
                </Dialog>


	</div>
	)
   }

}

function select(state) {
  return {
    editRecord: state.editView.editRecord,
    editView: state.editView,
    perms: state.mainView.permMap,
  }
}

CtxSocialStatusPanel.propTypes = {
  editRecord: React.PropTypes.object.isRequired,
  editView: React.PropTypes.object.isRequired,
}

export default connect(select)(CtxSocialStatusPanel);

