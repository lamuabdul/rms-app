
import React, { Component} from 'react';
import { connect } from 'react-redux';
import Form from './LoginForm';
import auth from '../auth/auth';
//import LoadingIndicator from '../LoadingIndicator.react';
import {login,setErrorMsg, changeForm} from '../actions/AppActions';


export  class LoginPage extends Component {

  contextTypes: {
    router: React.PropTypes.object
  }




        render() {
                const dispatch = this.props.dispatch;
                const { isLoggedIn, isLoginRequested,userName, password } = this.props.loginState;
    return (
                        <div className="">
                                <div className="">
                                        {/* While the form is sending, show the loading indicator,
                                                otherwise show "Log in" on the submit button */}
                        <Form loginState={this.props.loginState} dispatch={dispatch} errorMessage={this.props.errorMessage} location={location} history={this.props.history} onSubmit={this._login} buttonText={"Login"} isLoginRequested={isLoginRequested}/>
                                </div>
                        </div>
                );
  }

        _login(username, password) {
		
		  this.dispatch(login(username, password ));
		  
		 /* .catch((error) => {
          		this.dispatch(setErrorMsg('Invalid username/password.'))
        	  })
		*/
        }
}

// Which props do we want to inject, given the global state?
function select(state) {
  return {
    loginState: state.loginState,
    errorMessage: state.errorMessage,
  };
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(LoginPage);
