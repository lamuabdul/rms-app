

import { SET_AUTH, CHANGE_FORM,  SET_ERROR_MESSAGE } from '../constants/AppConstants';
import auth from '../auth/auth';
import * as errorMessages  from '../constants/MessageConstants';
import { push } from 'react-router-redux'
import server from '../server/server';
import {RecordStruct, StatusMap} from '../constants/RecordStruct';



export function createRecord(record) {

   return (dispatch) => {
	dispatch(saveRequested(true));
 	dispatch(showBusy(true));	
        server.createRecord(record,localStorage.token, (success, err, savedRecord ) => {
		dispatch(saveRequested(false));
		dispatch(showBusy(false))
		if( success === true) {
		   dispatch(editing(false))
		   dispatch(setErrorMessage(""))
		   dispatch(handleCardExpanded(0,true))
		   dispatch(saveSuccessfull( savedRecord ));
		   dispatch(clearEditData())
		//reset error, editrecord, isEditing, expandedCard , editTouched , showBusy
		   dispatch(push('/dashboard'));
		} else {
		   dispatch(setErrorMessage(err + savedRecord))
		}

	});	
	


   }


}


export function createChangeRequest(record, recordId) {
	return createRecord(record);
}


export function confirmAndSaveSocialStatusChange(record, statusMap, status) {
	return (dispatch) => {
		var rec = record;
		rec.socialStatus = status;
		dispatch(createRecord(rec));
	}

}

export function confirmAndSaveStatusChange(record, statusMap, status) {
	return (dispatch) => {
		var rec = record;
		if(typeof rec.statusMap === 'undefined')
		  rec.statusMap = {};
		statusMap.statusDate = new Date();
		rec.statusMap[status] = statusMap
		rec.status = status;
		rec.statusDate = new Date();
		dispatch(createRecord(rec));
	}

}

export function addUserSave(record) {
	return (dispatch) => {
		dispatch(userSaveRequested(true));
		server.createUser(record,localStorage.token, (success, err, savedUser) => {
			dispatch(userSaveRequested(false));			
			if ( success === true ) {
			     dispatch(newUserFetched(savedUser))
			     dispatch(userSaveSuccessfull(savedUser))
			     dispatch(setErrorMessage(""));
			     dispatch(push("/admin"));
			} else {
				dispatch(setErrorMessage(err));
			}


		})
	} 
}


export function addRoleSave(record) {
	return (dispatch) => {
		dispatch(roleSaveRequested(true));
		server.createRole(record,localStorage.token, (success, err, savedRole) => {
			dispatch(roleSaveRequested(false));			
			if ( success === true ) {
			     dispatch(newRoleFetched(savedRole))
			     dispatch(roleSaveSuccessfull(savedRole))
			     dispatch(setErrorMessage(""));
			     dispatch(push("/admin"));
			} else {
				dispatch(setErrorMessage(err));
			}


		})
	} 
}

export function processRefugeeList( refugeeList) {
	
	for(var i = refugeeList.length ; i--; ) {
		processRefugee(refugeeList[i]);
	}

}
function processRefugee(refugee) {
	if( typeof refugee === 'undefined') return 
	RecordStruct.basicInfo.fields.map((field) => {
		
		if( field.type === 'date' && typeof refugee[field.id] !== 'undefined' && refugee[field.id] != null ){
			refugee[field.id] = new Date(refugee[field.id]);
		}
		if(field.type === 'status' && typeof refugee[field.id] !== 'undefined' && refugee[field.id] != null) {
			Object.keys(refugee[field.id]).map( (key) => {
				if(typeof StatusMap[key] !== 'undefined' && StatusMap[key] != null)
				StatusMap[key].fields.map((sfld) => {
					if(sfld.type === 'date' && typeof refugee[field.id][key] !== 'undefined' && refugee[field.id][key] != null ){
					  refugee[field.id][key][sfld.id] = new Date(refugee[field.id][key][sfld.id]);
					}
				})
			})
		}
	} );

	RecordStruct.detailedInfo.fields.map((field) => {
		if( field.type === 'date' && typeof refugee.detailedInfo !== 'undefined' && typeof refugee.detailedInfo[field.id] !== 'undefined' && refugee.detailedInfo[field.id] != null ){
			refugee.detailedInfo[field.id] = new Date(refugee.detailedInfo[field.id]);
		}
	});	

	var dates = [];
	RecordStruct.dependentInfo.fields.map((field) => {
		if(field.type == 'date') 
			dates.push(field);
	});
	var afields = []
	RecordStruct.addresses.fields.map((field) => {
		if(field.type == 'date') 
			afields.push(field);
	});

	if(typeof refugee.dependents !== 'undefined') {

	for( var i = refugee.dependents.length ; i--;){
		var dep = refugee.dependents[i];
		for(var j = dates.length; j--; ) {
		var fld = dates[j];
		if(typeof dep[fld.id] !== 'undefined' && dep[fld.id] !== null) {
			refugee.dependents[i][fld.id] = new Date(refugee.dependents[i][fld.id]);
		}	
		}
	}
	}

	if(typeof refugee.addresses !== 'undefined') {
	for( var i = refugee.addresses.length ; i--;){
		var add = refugee.addresses[i];
		for(var j = afields.length; j--;){
		  var fld = afields[j];
		  if(typeof add[fld.id] !== 'undefined' && add[fld.id] !== null) {
			refugee.addresses[i][fld.id] = new Date(refugee.addresses[i][fld.id]);
		  }	
		  
		}
	}
	}

}

function getEditFields(perms) {
   var fields = perms.filter((perm) => {
                return (perm.startsWith("EDIT_"));
        }).map( (p) => {
                return p.substring(5);
        })

   return fields;

}

function getViewFields(perms) {
   var fields = perms.filter((perm) => {
                return (perm.startsWith("VIEW_") || perm.startsWith("EDIT_"));
        }).map( (p) => {
                return p.substring(5);
        })

   return fields;

}


function getPermMap(perms) {
	
	var permMap = new Map();
	perms.map((perm) => {
		permMap.set(perm,"1");
	})
	
	return permMap;
}


export function refreshRefugeeListConfirm( )  {

     return (dispatch) => {
	   dispatch(refreshRefugeeListRequest(false)); 
           dispatch(loadingAllRecords());
           server.getAll(localStorage.token, function(ok,response) {
		  if(ok == false){
			dispatch(setErrorMessage("Error Loading Refugee List - Server returned error"));
		   } else { 
		   processRefugeeList(response.refugees);
                   dispatch(refugeeListUpdate(ok,response.refugees));
                   if(response.users)
                         dispatch(usersFetched(response.users));
                   if(response.roles)
                         dispatch(rolesFetched(response.roles));
                   dispatch(setUserDetails(ok,response.user));
		   if(response.user && response.user.perms)
			dispatch(setUserPerms(getPermMap(response.user.perms)))
			var df = getViewFields(response.user.perms);
                   	dispatch(setViewFields(df));
			if(!df.includes("ALL")) {
                   		dispatch(setDisplayFields(df));
			}
			var ef = getEditFields(response.user.perms)
			dispatch(setEditFields(ef))
		   }
                   dispatch(loadingCompleted());
           });

     }

}


/**
 * Logs an user in
 * @param  {string} username The username of the user to be logged in
 * @param  {string} password The password of the user to be logged in
 */
export function login(username, password) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    dispatch(loginRequested(true));
    // If no username or password was specified, throw a field-missing error
    if (anyElementsEmpty({ username, password })) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      dispatch(loginRequested(false));
      return;
    }
      // Use auth.js to fake a request
      auth.login(username, password, (success, err) => {
        // When the request is finished, hide the loading indicator
        dispatch(loginRequested(false));
        dispatch(setAuthState(success));
        if (success === true) {
          // If the login worked, forward the user to the dashboard and clear the form
          dispatch(push('/dashboard'));
          dispatch(changeForm({
            username: "",
            password: ""
          }));
        } else {
	 dispatch(setErrorMessage(err));
	} 
      });
  }
}

/**
 * Logs the current user out
 */
export function logout() {
  return (dispatch) => {
    dispatch(loginRequested(true));
    auth.logout((success, err) => {
      if (success === true) {
        dispatch(loginRequested(false))
        dispatch(clearState())
        dispatch(setAuthState(false));
        dispatch(push( '/login'));
      } else {
        dispatch(loginRequested(false))
        dispatch(clearState())
        dispatch(setAuthState(false));
        dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
      }
    });
  }
}

/**
 * Sets the authentication state of the application
 * @param {boolean} newState True means a user is logged in, false means no user is logged in
 */
export function setAuthState(newState,token) {
  return { type: SET_AUTH, newState , token};
}

export function clearState() {
  return {type: "CLEAR_ALL_STATE"};
}

/**
 * Sets the form state
 * @param  {object} newState          The new state of the form
 * @param  {string} newState.username The new text of the username input field of the form
 * @param  {string} newState.password The new text of the password input field of the form
 * @return {object}                   Formatted action for the reducer to handle
 */
export function changeForm(newState) {
  return { type: CHANGE_FORM, newState };
}

/**
 * Sets the requestSending state, which displays a loading indicator during requests
 * @param  {boolean} sending The new state the app should have
 * @return {object}          Formatted action for the reducer to handle
 */
export function loginRequested(sending) {
  return { type: "LOGIN_REQUESTED", sending };
}

export function sideNavOpen(open){
  return {type: "SIDENAV_OPEN",open}
}

export function addNewApplicant() {
	return (dispatch) => {
	  dispatch(editing(true))
	  dispatch(push('/addnew'));
	}
}

export function goHome() {
	
	return (dispatch) => {
		dispatch(clearEditData());
		dispatch(editing(false));
		dispatch(push('/dashboard'));
	}
}

export function clearEditData() {
	
	return {type: "CLEAR_EDIT_DATA"};
}

export function editing(isEditing){
	return {type:"EDITING", value:isEditing};
}

export function addExtendedDetails(){
      return {type : "ADD_EXTENDED_DETAILS"}
}

export function handleExtendedDone() {
     return {type: "EXTENDED_DONE"}
}

export function handleAddressDone() {
     return {type: "ADDRESS_DETAILS_DONE"}
}

export function handleAddressCancel() {
     return {type: "ADDRESS_DETAILS_CANCEL"}
}
export function handleAddAddress() {
     return {type: "ADDRESS_DETAILS"}
}

export function handleEventDone() {
    return {type:"EVENT_DONE"}
}

export function handleAddEvent() {
    return {type:"EVENT_DETAILS"}
}

export function handleAddDependent() {
    return {type:"DEPENDENT_DETAILS"}
}

export function handleDependentDone() {
     return {type: "DEPENDENT_DETAILS_DONE"}
}

export function handleDependentCancel() {
     return {type: "DEPENDENT_DETAILS_CANCEL"}
}
export function handleEventCancel() {
     return {type: "EVENT_CANCEL"}
}


export function handleEditChange( newState, field) {
     return {type:"EDIT_CHANGE", newState,field}
}


export function handleCardExpanded(cardIdx, expanded){
     return {type:"CARD_EXPANDED", cardIdx, expanded};
}

export function showValidations(record){
     return {type:"VALIDATE", record};
}

export function saveCurrentRecord(){
     return {type:"SAVE_RECORD" };
}

export function showBusy(value) {
	return { type: "SHOW_BUSY", value}
}
export function saveRequested(value){
     return {type:"SAVE_REQUESTED", value };
}

export function saveSuccessfull(savedRecord){
     return {type:"SAVE_SUCCESSFULL" , savedRecord};
}

export function cancelEditRequest() {
	return {type:"CANCEL_EDIT_REQUEST"};
}

export function editRequestChange(newState) {
	return {type: "EDIT_REQUEST_CHANGE", newState}
}

export function confirmEditRequest() {
	return {type: "CONFIRM_EDIT_REQUEST"}
}


export function editRequestOpen(field){
     return {type:"EDIT_REQUEST_OPEN" , field};
}

export function saveFailed(err){
     return {type:"SAVE_FAILED" , err};
}

export function filterRefugeeList(value){
     return {type:"FILTER_REFUGEE_LIST", value };
}

export function filterSearchId(value){
     return {type:"FILTER_SEARCH_ID", value };
}
export function filterSearchName(value){
     return {type:"FILTER_SEARCH_NAME", value };
}
export function filterSearchCountry(value){
     return {type:"FILTER_SEARCH_COUNTRY", value };
}

export function editUser(user) {
     return {type: "EDIT_USER", user};
}
export function addUser(roles) {

     return {type: "ADD_USER", roles};
}

export function addUserCancel() {
	return {type: "ADD_USER_CANCEL"};
} 

export function newUserFetched(value) {
     return {type: "NEW_USER_FETCHED", value};
}
export function addUserValidate(){
	return {type: "ADD_USER_VALIDATE"};
} 
export function addUserChange(value) {
	return {type: "ADD_USER_CHANGE", value};
}
export function userSaveRequested(value) {
	return {type: "USER_SAVE_REQUESTED", value};
}
export function userSaveSuccessfull(value) {
	return {type: "USER_SAVE_SUCCESSFULL", value};
}

export function roleSaveRequested(value) {
	return {type: "ROLE_SAVE_REQUESTED", value};
}

export function roleSaveSuccessfull(value) {
	return {type: "ROLE_SAVE_SUCCESSFULL", value};
}
export function addRole() {
     return {type: "ADD_ROLE"};
}

export function addRoleValidate() {
     return {type: "ADD_ROLE_VALIDATE"};
}
export function addObjectPerms() {
	return {type: "ADD_OBJECT_PERMS"};
}
export function addStatusPerms() {
	return {type: "ADD_STATUS_PERMS"};
}

export function permsChanged(fieldId, checked){
	return {type:"PERMS_CHANGED", fieldId, checked}
}

export function openEVPopCtxMenu(evt, key){
	return {type:"OPEN_EV_POP", target: evt.currentTarget, key}
}

export function openMVPopCtxMenu(evt, key){
	return {type:"OPEN_MV_POP", target: evt.currentTarget, key}
}

export function closeEVPopCtxMenu() {
	return {type:"CLOSE_EV_POP"}
}

export function closeMVPopCtxMenu(){
	return {type:"CLOSE_MV_POP"}
}

export function addFieldViewPerms() {
	return {type: "ADD_FIELD_VIEW_PERMS"};
}
export function addFieldEditPerms() {
	return {type: "ADD_FIELD_EDIT_PERMS"};
}
export function objectPermsDone() {
     return {type: "OBJECT_PERMS_DONE"};
}
export function objectPermsCancel() {
     return {type: "OBJECT_PERMS_CANCEL"};
}
export function editRole(role) {
     return {type: "EDIT_ROLE", role};
}
export function addRoleCancel() {
     return {type: "ADD_ROLE_CANCEL"};
}
export function newRoleFetched(value) {
     return {type: "NEW_ROLE_FETCHED", value};
}

export function addRoleChange(record) {
     return {type: "ADD_ROLE_CHANGE", record};
}
export function refugeeListUpdate(ok,response) {
     return {type: "REFUGEE_LIST_UPDATE", ok, response};
}

export function rolesFetched(roles) {
    return {type:"ROLES_FETCHED",roles}
}

export function usersFetched(users) {
    return {type:"USERS_FETCHED",users}
}

export function setDisplayFields(fields) {
     return {type:"SET_DISPLAY_FIELDS",fields};
}
export function setViewFields(fields) {
     return {type:"SET_VIEW_FIELDS",fields};
}

export function setEditFields(fields) {
     return {type:"SET_EDIT_FIELDS",fields};
}


export function setUserDetails(ok,response) {
     return {type:"SET_USER_DETAILS", ok, response};
}

export function setErrorMsg( msg)  {
     return {type: "SET_ERROR_MESSAGE", msg};
}

export function loadingAllRecords( )  {
     return {type: "LOADING_ALL_RECORDS"};
}

export function loadingCompleted( )  {
     return {type: "LOADING_COMPLETED"};
}

export function photoSelected(url )  {
     return {type: "PHOTO_SELECTED", url};
}

export function refreshRefugeeListRequest( request )  {
     return {type: "REFRESH_REFUGEE_LIST_REQUEST", request};
}

export function socialStatusChange( status )  {
     return {type: "SOCIAL_STATUS_CHANGE", status};
}

export function statusChange( status )  {
     return {type: "STATUS_CHANGE", status};
}

export function setUserPerms(permMap) {
    return {type:"SET_USER_PERMS",permMap};
}	

export function cancelStatusChange() {
	return {type:"CANCEL_STATUS_CHANGE"};

}

export function cancelCtxSocialStatusChange(  )  {
	return (dispatch)=> {
     		dispatch({type: "CANCEL_CTX_SOCIAL_STATUS_CHANGE"});
		dispatch(push('/dashboard'));
	}
}

export function cancelCtxStatusChange(  )  {
	return (dispatch)=> {
     		dispatch({type: "CANCEL_CTX_STATUS_CHANGE"});
		dispatch(push('/dashboard'));
	}
}

export function confirmSocialStatusChange(  )  {
     return {type: "CONFIRM_SOCIAL_STATUS_CHANGE" };
}

export function confirmStatusChange(  )  {
     return {type: "CONFIRM_STATUS_CHANGE" };
}

export function clearPrimary( ) {
     return {type:"CLEAR_PRIMARY"}
}
export function primaryEdit( newState, field) {
     return {type:"PRIMARY_EDIT", newState,field}
}

export function socialStatusEdit( newState, field) {
     return {type:"SOCIAL_STATUS_EDIT", newState,field}
}

export function statusEdit( newState, field) {
     return {type:"STATUS_EDIT", newState,field}
}

export function confirmStatusNonNested( value  )  {
     return {type: "CONFIRM_STATUS_NON_NESTED", value };
}

export function primaryView(value, fields){
	return {type:"VIEW_PRIMARY",value, fields};
}

export function viewRecord1(value){
	return {type:"VIEW_RECORD",value};
}

export function eventPanel(record) {
	return (dispatch) => {
		dispatch(viewRecord1(record)) 
		dispatch(editRecord1());
		dispatch({type:"EVENT_PANEL"});
		dispatch(push('/event'));
	}
}

/*export function detailedPanel(key) {
	return (dispatch) => {
		dispatch({type:"DETAILED_PANEL",key});
		dispatch(push('/detailed'));
	}
}*/

export function detailedPanel(record, flds, detailed) {
	return (dispatch) => {
		dispatch(viewRecord1(record)) 
		dispatch(editRecord1());
		dispatch(primaryView(record, flds)) 
		dispatch({type:"PRIMARY_PANEL",record});
		dispatch(push('/detailed'));
	}
}

export function primaryPanel(record, flds, detailed) {
	return (dispatch) => {
		dispatch(viewRecord1(record)) 
		dispatch(editRecord1());
		dispatch(primaryView(record, flds, detailed)) 
		dispatch({type:"PRIMARY_PANEL",record});
		dispatch(push('/primary'));
	}
}

export function socialStatusPanel(record) {
	return (dispatch) => {
		dispatch(viewRecord1(record)) 
		dispatch(editRecord1());
		dispatch(socialStatusEdit({}));
		dispatch({type:"SOCIAL_STATUS_PANEL",record});
		dispatch(push('/socialStatus'));
	}
}

export function statusPanel(record) {
	return (dispatch) => {
		dispatch(viewRecord1(record)) 
		dispatch(editRecord1());
		dispatch(statusEdit({}));
		dispatch({type:"STATUS_PANEL",record});
		dispatch(push('/status'));
	}
}

export function dependentPanel(record) {
	return (dispatch) => {
		dispatch(viewRecord1(record)) 
		dispatch(editRecord1());
		dispatch({type:"DEPENDENT_PANEL",record});
		dispatch(push('/dependent'));
	}
}

export function addressPanel(record) {
	return (dispatch) => {
		dispatch(viewRecord1(record)) 
		dispatch(editRecord1());
		dispatch(editAddress());
		dispatch({type:"ADDRESS_PANEL",record});
		dispatch(push('/address'));
	}
}

export function editRecord(){
       return (dispatch) => {
	dispatch(editRecord1());
	dispatch(editing(true));
	}
}
export function editRecord1() {
	return {type:"EDIT_RECORD"};
}
export function editDependent(value) {
	return {type: "EDIT_DEPENDENT", value};
}

export function editAddress(value) {
	return {type: "EDIT_ADDRESS", value};
}

export function clearCtxMenu() {
	return {type:"CLEAR_CTX_MENU"};
}

export function viewRecord( value  )  {
	return (dispatch) => {
	  dispatch(viewRecord1(value))
	  dispatch(push('/addnew'));
	}
}


/**
 * Sets the errorMessage state, which displays the ErrorMessage component when it is not empty
 * @param message
 */
export function setErrorMessage(message) {
  return (dispatch) => {
    dispatch(setErrorMsg(message));

/*    const form = document.querySelector('.form-page__form-wrapper');
    if (form) {
      form.classList.add('js-form__err-animation');
      // Remove the animation class after the animation is finished, so it
      // can play again on the next error
      setTimeout(() => {
        form.classList.remove('js-form__err-animation');
      }, 150);

      // Remove the error message after 3 seconds
      setTimeout(() => {
        dispatch({ type: SET_ERROR_MESSAGE, message: '' });
      }, 3000);
    }*/
  }
}

/**
 * Forwards the user
 * @param {string} location The route the user should be forwarded to
 */
function forwardTo(location) {
  return {type: "FORWARD_TO",location};
  //browserHistory.push(location);
}


/**
 * Checks if any elements of a JSON object are empty
 * @param  {object} elements The object that should be checked
 * @return {boolean}         True if there are empty elements, false if there aren't
 */
function anyElementsEmpty(elements) {
  for (let element in elements) {
    if (!elements[element]) {
      return true;
    }
  }
  return false;
}

