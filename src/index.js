import React from 'react';
import ReactDOM from 'react-dom';
import LoginPage from './components/LoginPage';
import Dashboard from './components/Dashboard';
import './index.css';
import { createStore, combineReducers, applyMiddleware ,compose} from 'redux';
import createHistory from 'history/createBrowserHistory'
import ReduxThunk from 'redux-thunk'
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'
import server from './server/server';
import {refugeeListUpdate} from './actions/AppActions'; 


import { Provider } from 'react-redux';
import { loginReducer } from './reducers/reducers';
import  auth  from './auth/auth.js';
import { BrowserRouter as Router,Redirect, Route } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Sidenav from './components/Sidenav'
import EditRecordPane from './components/EditRecordPane'
import RmsTheme from './components/rmsTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import App from './components/App';



import injectTapEventPlugin from 'react-tap-event-plugin';




ReactDOM.render(
 <App/>,
  document.getElementById('root')
);
