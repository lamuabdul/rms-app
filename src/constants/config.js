
import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyBZT5SGaIm62TcEIjQtOM58z6Kf-5YcwTA",
    authDomain: "cova-rms.firebaseapp.com",
    databaseURL: "https://cova-rms.firebaseio.com",
    projectId: "cova-rms",
    storageBucket: "cova-rms.appspot.com",
    messagingSenderId: "47260851737"
  };
  firebase.initializeApp(config);

export const PHOTOBASEURL = "https://storage.googleapis.com/rms-profile/";
export const firebaseAuth = firebase.auth

