/**
 * This file provided by Facebook is for non-commercial testing and evaluation
 * purposes only. Facebook reserves all rights not expressly granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * FACEBOOK BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var faker = require('faker');

class FakeObjectDataListStore {
  constructor(/*number*/ size){
    this.size = size || 2000;
    this._cache = [];
  }


  createFakeRowObjectData(/*number*/ index) /*object*/ {
    return {
      fullName: faker.name.firstName() ,
      alias: faker.name.lastName(),
      fatherName: faker.name.firstName(),
      motherName: faker.name.lastName(),
      spouseName: faker.name.firstName(),
      sex: faker.random.arrayElement(['male', 'female']),
      maritalStatus: faker.random.arrayElement(["Married","Single","Unmarried","Divorced","Separated","Widow","Engaged"]),
      age: faker.random.number(90),
      dob: faker.date.past(90),
      phone: faker.phone.phoneNumber(),
      occupation: faker.name.jobTitle(),
      status: faker.random.arrayElement(["New Arrival","AMRS","UCC","UCC-Reject","UCC-Accepted","UCC-Issued","UCC-Untraced","RSD-Applied","RSD-Done","RSD-Accepted","RSD-Reject","RSD-Appeal","RSD-Appeal-Reject" ]),
      email: faker.internet.email(),
      unhcrNo: faker.finance.account(),
      originCountry: faker.address.country(),
      photoUrl: faker.image.avatar(),
      communityLeader: faker.random.arrayElement(['Yes','No',' '])
    };
  }

  getObjectAt(/*number*/ index) /*?object*/ {
    if (index < 0 || index > this.size){
      return undefined;
    }
    if (this._cache[index] === undefined) {
      this._cache[index] = this.createFakeRowObjectData(index);
    }
    return this._cache[index];
  }

  /**
  * Populates the entire cache with data.
  * Use with Caution! Behaves slowly for large sizes
  * ex. 100,000 rows
  */
  getAll() {
    if (this._cache.length < this.size) {
      for (var i = 0; i < this.size; i++) {
        this.getObjectAt(i);
      }
    }
    return this._cache.slice();
  }

  getSize() {
    return this.size;
  }
}

module.exports = FakeObjectDataListStore;

