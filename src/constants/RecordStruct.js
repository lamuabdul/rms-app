
 var  RecordStruct_ = {
  basicInfo: {
    title:'Basic Information',
    arity:"single",
    fieldMap: new Map( [
     ["fullName",{name:"Full Name", id:"fullName", type:"string" , required:true, pi:true}],
     ["alias", {name:"Alias/Other Name", id:"alias", type:"string", pi:true}],
     ["fatherName",{name:"Father’s Name", id:"fatherName", type:"string", required:true, pi:true}],
     ["motherName",{name:"Mother’s Name", id:"motherName",type:"string", required:true, pi:true}],
     ["spouseName",{name:"Spouse Name", id:"spouseName", type:"string"}],
     ["sex",{name:"Sex", id:"sex", type:"enum", values:["Male","Female"], required:true, pi:true}],
     ["status",{name:"Status", id:"status",type:"status" , values:[], required:true}],
     ["maritalStatus",{name:"Marital Status", id:"maritalStatus", type:"enum", values:["Married","Single","Unmarried","Divorced","Separated","Widow","Engaged"]}],
     ["dob",{name:"Date Of Birth", id:"dob", type:"date",required:true, pi:true}],
     ["phone",{name:"Phone Number",id:"phone", type:"phone"}],
     ["occupation",{name:"Profession/Occupation", id:"occupation",type:"string"}],
     ["email",{name:"Email", id:"email", type:"email"}],
     ["originCountry",{name:"Country Of Origin", id:"originCountry", type:"country", required:true, pi:true}],
     ["photoUrl",{name:"Photo", id:"photoUrl", type:"photo"}],
     ["communityLeader",{name:"Is Community Leader" , id:"communityLeader", type:"boolean"}],
  ])
    },
 detailedInfo: { 
    title:"Detailed Information",
    arity:"single",
    fieldMap: new Map([
    ["spouseLocation",{name:"Location Of Spouse", id:"spouseLocation", type:"string"}],
    ["height",{name:"Height", id:"height", type:"float"}],
    ["placeOfBirth",{name:"Place of Birth", id:"placeOfBirth", type:"string"}],
    ["birthCity",{name:"Birth City", id:"birthCity", type:"string"}],
    ["visibleMark1",{name:"Visible Marks 1", id:"visibleMark1", type:"string"}],
    ["visibleMark2",{name:"Visible Marks 2", id:"visibleMark2", type:"string"}],
    ["acqNationality",{name:"Manner of Acquiring Present Nationality", id:"acqNationality", type:"string"}],
    ["acqNationalityDate",{name:"Date of Acquiring Present Nationality", id:"acqNationalityDate", type:"date"}],
    ["passportDetails",{name:"Passport Details", id:"passportDetails", type:"string"}],
    ["passport",{name:"Passport Number", id:"passport", type:"string"}],
    ["arrivalDate",{name:"Arrival Date to Current City", id:"arrivalDate", type:"date"}],
    ["arrivalDateIndia",{name:"Date of Arrival In India", id:"arrivalDateIndia", type:"date"}],
    ["disemAddr",{name:"Disembarkation Address", id:"disembAddr" , type:"string"}],
    ["journeyMode",{name:"Mode of Journey", id:"journeyMode", type:"string"}],
    ["transportId",{name:"Transport(Flight/Train/Bus Number", id:"transportId", type:"string"}],
    ["purposeVisit",{name:"Purpose of Visit to India", id:"purposeVisit", type:"string"}],
    ["religion",{name:"Religion", id:"religion",type:"string"}],
    ["ethnicity",{name:"Ethnicity/Clan/Sub-clan", id:"ethnicity", type:"string"}],
    ["deliveryLoc",{name:"Home/Hospital Delivery", id:"deliveryLoc", type:"string"}],
    ["birthCertAvlbl",{name:"Birth Certificate Available", id:"birthCertAvlbl", type:"boolean"}],
    ["bimsCapt",{name:"BIMS captured", id:"bimsCapt", type:"boolean"}],
    ["dualNational",{name :"Is Dual National", id:"dualNational", type:"boolean"}],
    ["indianOrigin",{name :"Is Indian Origin", id:"indianOrigin", type:"boolean"}],
   ]),
   },
addresses: {
    title:"Addresses",
    arity:"multiple",
    fieldMap: new Map([
    ["line1",{name:"Address Line 1", id:"line1", type:"string", required:true}],
    ["line2",{name:"Address Line 2", id:"line2", type:"string"}],
    ["owner",{name:"House Owner Name", id:"owner", type:"string"}],
    ["city",{name:"City ", id:"city", type:"string", required:true}],
    ["state",{name:"State", id:"state",type:"string", required:true}],
    ["country",{name:"Country", id:"country", type:"country", required:true}],
    ["pin",{name:"Pin", id:"pin", type:"string"}],
    ["sDate",{name:"Start Date", id:"sDate", type:"date"}],
    ["eDate",{name:"End Date", id:"eDate", type:"date"}],
    ["cur",{name:"Current", id:"cur", type:"boolean", arity:"single" }],
    ["type",{name:"Address Type", id:"type", type:"string"}]
   ])
    
  },
  dependentInfo: {
    title: "Dependents",
    arity:"single",
    fieldMap: new Map([
     ["fullName",{name:"Full Name", id:"fullName", type:"string" , required:true,pi:true}],
     ["alias",{name:"Alias/Other Name", id:"alias", type:"string",pi:true}],
     ["fatherName",{name:"Father’s Name", id:"fatherName", type:"string", required:true,pi:true}],
     ["motherName",{name:"Mother’s Name", id:"motherName",type:"string", required:true,pi:true}],
     ["spouseName",{name:"Spouse Name", id:"spouseName", type:"string"}],
     ["sex",{name:"Sex", id:"sex", type:"enum", values:["Male","Female"], required:true,pi:true}],
     ["maritalStatus",{name:"Marital Status", id:"maritalStatus", type:"enum", values:["Married","Single","Unmarried","Divorced","Separated","Widow","Engaged"]}],
     ["relationship",{name: "Relationship ", id:"relationship", type:"enum", values: ["Spouse","Child","Sibling","Parent","Other-Blood Relative", "Other-Acquaintance"]}],
     ["age",{name:"Age", id:"age", type:"calculated", ref:"dob",pi:true}],
     ["dob",{name:"Date Of Birth", id:"dob", type:"date",required:true,pi:true}],
     ["phone",{name:"Phone Number",id:"phone", type:"phone"}],
     ["occupation",{name:"Profession/Occupation", id:"occupation",type:"string"}],
    // ["status",{name:"Status", id:"status",type:"enum" , values:[]}],
     ["email",{name:"Email", id:"email", type:"email"}],
     ["unhcrNo",{name:"UNHCR Reg Number" , id:"unhcrNo",type:"string"}],
     ["originCountry",{name:"Country Of Origin", id:"originCountry", type:"country", required:true,pi:true}],
     ["photoUrl",{name:"Photo", id:"photoUrl", type:"photo"}],
     ["communityLeader",{name:"Is Community Leader" , id:"communityLeader", type:"boolean"}],
    ]) 
    },

   events: {
	title: "Events",
	fieldMap: new Map([
         ["eventType",{name:"Type Of Event", id:"eventType", type:"enum" , values:["Medical","Social", "Pregnancy","Childbirth"], required:true}],
         ["eventDate",{name:"Event Date", id:"eventDate", type:"date" , required:true}],
	]),

   },
  
	eventDetails: {
	 title : "Event Details",
	 fieldMap: new Map([
         ["Medical",{ fieldMap: new Map([
		  ["problem",{name:"Problem/Disease",id:"problem",type:"string", required: true}],
		  ["diagnosis",{name:"Diagnosis",id:"diagnosis",type:"string" }],
		  ["cost",{name:"Cost of treatment",id:"cost",type:"string" }],
		  ["date",{name:"Date of diagnosis",id:"date",type:"date" }],
		  ["support",{name:"Support given",id:"support",type:"boolean" }],
		  ["amount",{name:"Amount Given by COVA",id:"amount",type:"number" }],
		])
	  	}],
         ["Social",{ 
		fieldMap: new Map([
		  ["descr",{name:"Description",id:"descr",type:"string", required: true}],
		])
	  	}],
         ["Pregnancy",{ 
		fieldMap: new Map([
		  ["lmp",{name:"LMP Date",id:"lmp",type:"date", required: true}],
		  ["number",{name:"Number of pregnancies",id:"number",type:"number" }]
		])
	  	}],
         ["Childbirth",{ 
		fieldMap: new Map([
		  ["date",{name:"Delivery date",id:"date",type:"date", required: true}],
		  ["location",{name:"Delivery Location",id:"location",type:"enum" , values:["Hospital","Home"] }],
		  ["hospital",{name:"Name of hospital",id:"hospital",type:"string" }],
		  ["cert",{name:"Birth Certificate Issued",id:"cert",type:"boolean" }],
		])
	  	}],
	])
	},
   socialStatus: {
	title: "Socio Economic Status",
	fieldMap: new Map([
         ["socialCategory",{name:"Category", id:"socialCategory", type:"enum" , values:["0 - Poor","1", "2","3","4","5","6","7","8","9 - Rich"], required:true}],
         ["monthlyIncome",{name:"Monthly Income", id:"monthlyIncome", type:"string" , required:true}],
         ["supportRequested",{name:"Economic Support Requested?", id:"supportRequested", type:"boolean" }],
         ["supportProvided",{name:"Support Provided", id:"supportProvided", type:"boolean" }],
	])
   },
	
   fieldNotes :{
	title : "Field Notes",
	fieldMap: new Map([
         ["date",{name:"Date of Notes", id:"date", type:"date" , required:true}],
         ["notes",{name:" Notes", id:"notes", type:"string" , required:true}],
	])
   }


};


function getRecordStruct() {
	
	var ret = RecordStruct_;
	ret.basicInfo.fields = Array.from(RecordStruct_.basicInfo.fieldMap.values());
	ret.basicInfo.fieldNames = ret.basicInfo.fields.map(function(value) { return value.id} ) ;
	ret.detailedInfo.fields = Array.from(RecordStruct_.detailedInfo.fieldMap.values());
	ret.detailedInfo.fieldNames = ret.detailedInfo.fields.map(function(value) {  return value.id} ) ;
	ret.dependentInfo.fields = Array.from(RecordStruct_.dependentInfo.fieldMap.values());
	ret.addresses.fields = Array.from(RecordStruct_.addresses.fieldMap.values());
	ret.events.fields = Array.from(RecordStruct_.events.fieldMap.values());
	ret.socialStatus.fields = Array.from(RecordStruct_.socialStatus.fieldMap.values());
	ret.eventDetails.fields = Array.from(RecordStruct_.eventDetails.fieldMap.values());
	ret.map =  new Map([...RecordStruct_.basicInfo.fieldMap, ...RecordStruct_.detailedInfo.fieldMap, ...RecordStruct_.dependentInfo.fieldMap, ...RecordStruct_.addresses.fieldMap])

	return ret;

}

export const RecordStruct = getRecordStruct();



var User_ = {
    title: "User",
    fieldMap: new Map( [
     ["id",{name:"id", id:"id", type:"string" , required:true}],
     ["name",{name:"Name", id:"name", type:"string" , required:true}],
     ["email",{name:"Email", id:"email", type:"string" , required:true}],
     ["role",{name:"Role", id:"role", type:"string" , required:true}],
    ])
}


export const Role = {
   title:"Role",
   fields: [
	   { name:"id", id:"id", type:"string", required:true },
	   { name:"Name", id:"name", type:"string", required:true },
	   { name:"Rights", id:"rights", type:"rights" }
	]

}


var Rights_ = {
	rightMap: new Map( [
	['ADD_REFUGEE',{name:"Add New Refugee", id:"ADD_REFUGEE" , type:"single", detail: "Permission to add a new refugee record"}],
	['VIEW_ALL',{name:"View All", id:"VIEW_ALL" , type:"single", detail: "Permission to view all fields of a refugee record"}],
	['EDIT_ALL',{name:"Edit All", id:"EDIT_ALL",type:"single", detail: "Permission to edit non-basic fields of a refugee record"}],
	['APPROVE_PI_CHANGE',{name:"Approve Basic Changes", id:"APPROVE_PI_CHANGE",type:"single", detail: "Permission to approve changes to basic fields"}],
	['EDIT_STATUS',{name:"Change Status", id:"EDIT_STATUS", from :[], type:"status"}],
	['VIEW_SOCIAL_STATUS',{name:"View Social Status", id:"VIEW_SOCIAL_STATUS", detail:"Permission to view social status", type:"single"}],
	['EDIT_SOCIAL_STATUS',{name:"Edit Social Status", id:"EDIT_SOCIAL_STATUS", detail:"Permission to edit social status", type:"single"}],
	['ADD_USER',{name:"Add User", id:"ADD_USER",type:"single", detail:"Permission to add a new RMS user"}],
	['ADD_EVENT',{name:"Add Event", id:"ADD_EVENT",type:"single", detail:"Permission to add a new Refugee Event"}],
	['VIEW_EVENT',{name:"View Event", id:"VIEW_EVENT",type:"single", detail:"Permission to View Refugee Events"}],
	['EDIT_USER',{name:"Edit User", id:"EDIT_USER",type:"single", detail: "Permission to edit an existing RMS user"}],
	['ADD_ROLE',{name:"Add Role", id:"ADD_ROLE",type:"single", detail:"Permission to add a new RMS role"}],
	['EDIT_ROLE',{name:"Edit Role", id:"EDIT_ROLE",type:"single", detail: "Permission to edit an existing RMS role"}],
	['VIEW_AUDIT_TRAIL',{name:"View Audit Trail", id:"VIEW_AUDIT_TRAIL",type:"single", detail:"Permission to view audit trail"}],
	])
};

function getRights(){
	var ret = Rights_;
	ret.fields = Array.from(Rights_.rightMap.values());
	return ret;
}
export const Rights = getRights();

export const User = getUser();

function getUser() {
  var ret = User_;
  ret.fields = Array.from(User_.fieldMap.values());
  return ret;
};

export const Status = [
	"NEW_ARRIVAL",
	"AMRS",
	"UCC",
	"RSD",
	"RC",
	"LTV"	
]

export const StatusMap = {
	
	NEW_ARRIVAL: {
		title: "New Arrival",
		fields :[],

	},
	
	AMRS : {
		title : "AMRS",
		fields: [
			{name: "AMRS Date", id:"amrsDate", type:"date", required:true},
			{name: "AMRS Form Sent to UNHCR", id:"formSentDate", type:"date", required:true},
			{name: "Interviewed By", id:"interviewedBy", type:"string", required:true},
			{name: "Interpreter", id:"interpreter", type:"string"},
		] 
		},

	UCC: {
		title : "UCC Details",
		fields: [
			{name: "UCC Application Date", id:"uccAppDate", type:"date", required:true},
			{name: "Date of Receipt", id:"uccRecvDate", type:"date", required:true },
			{name: "Reference Number (Family)", id:"uccCaseFamily", type:"string" },
			{name: "Individual Number", id:"uccCaseInd", type:"string" },
			{name: "Date of Issue(COVA)", id:"issueDate", type:"date" },
			{name: "Info Sent to UN after issue to POC", id:"infoSent", type:"boolean" },
			{name: "Expiry Date", id:"expiry", type:"date" },
		] 


	 } ,
	RSD : {
		title :"RSD",
		fields: [
			{name: "RSD Date", id:"rsdDate", type:"date", required:true},
			{name: "RSD Status", id:"rsdStatus", type:"enum"},
			{name: "RC Received Date", id:"rcRecvDate", type:"date"},
			{name: "RSD Appeal Sent Date", id:"appealSentDate", type:"date"},
			{name: "RC Issue Date", id:"rcIssueDate", type:"date"},
			{name: "RC Expiry Date", id:"rcExpiryDate", type:"date"},
			
		] 
	},
	RC: {
		title :"RC",
		fields: [
			{name: "RC Received Date", id:"rcRecdDate", type:"date", required:true},
			{name: "RC Issue Date", id:"rcIssueDate", type:"date", required:true},
			{name: "RC Expiry Date", id:"expiryDate", type:"date", required:true},
		],

	},
	LTV : {
		title : "LTV",
		fields: [
			{name: "LTV Form Filled Date", id:"ltvFormDate", type:"date", required: true },
			{name: "LTV Appointment Date", id:"ltvAptDate", type:"date" },
			{name: "Application ID", id:"appId", type:"string"},
			{name: "Acknowledgment Received", id:"ackRecd", type:"boolean"},
			{name: "LTV Issue Date", id:"issue", type:"date"},
			{name: "LTV Expiry Date", id:"expiry", type:"date"},
			{name: "Interpreter", id:"interpreter", type:"string"},
		] 
	}
}

export const Values= {
	sex: ["Male","Female"],
	maritalStatus:["Married","Single","Unmarried","Divorced","Separated","Widow","Engaged"],
	relationship:["Spouse","Child","Sibling","Parent","Other-Blood Relative", "Other-Acquaintance"],
	status:["New Arrival","AMRS","UCC","UCC-Reject","UCC-Accepted","UCC-Issued","UCC-Untraced","RSD-Applied","RSD-Done","RSD-Accepted","RSD-Reject","RSD-Appeal","RSD-Appeal-Reject" ],
	uccStatus:["Reject","Accepted","Issued","Untraced"],
	rsdStatus:["RSD Scheduled","RSD Done/RC Pending","RSD Rejected","Appealed","Appeal Rejected"],
        eventType:["Medical","Social", "Pregnancy","Childbirth"],
        socialCategory:["0 - Poor","1", "2","3","4","5","6","7","8","9 - Rich"],
        location:["Hospital","Home"],
}


