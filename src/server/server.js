
import fetch from 'isomorphic-fetch'
import chunk from 'lodash';
import {RecordStruct} from '../constants/RecordStruct';


var server = {

	createRecord(record, token, callback ) {
	var fields = [];
         RecordStruct.basicInfo.fields.map((field) =>  {
	    fields.push(field.id);
	 });
	 fields.push('statusMap');
	 fields.push('socialStatus');
	 fields.push('statusDate');
	 fields.push('key');
   	 fields.push('dependents');
   	 fields.push('addresses');
   	 fields.push('events');
   	 fields.push('detailedInfo');
   	 fields.push('cr');

      
	var rec = chunk.pick(record, fields)

         let fetchData = {
         method: 'POST',
         body: JSON.stringify( rec ),
         headers: {
                'Content-Type':'application/json',
		'x-auth': token
         }
        }

        fetch(`https://us-central1-cova-rms.cloudfunctions.net/saveRecord`,fetchData). then (function (response) {
	        response.text().then( (v) => {
                if(response.ok ) {
			record.key = JSON.parse(v);
                        callback(true,null, record )
                } else {
                        callback(false,v)
                }
        });

	})
	},

	
	createUser(record, token, callback ) {
		var rec = chunk.pick(record, ['email','name','role']);
	
         	let fetchData = {
         	method: 'POST',
         	body: JSON.stringify( rec ),
         	headers: {
                	'Content-Type':'application/json',
			'x-auth': token
         	}
        	}

        	fetch(`https://us-central1-cova-rms.cloudfunctions.net/saveUser`,fetchData). then (function (response) {
			response.text().then( (v) => {
			if(response.ok)
                        	callback(true,null, JSON.parse(v))
			else 
                        	callback(false, v)
			} );
		})
	},
	createRole(record, token, callback ) {
		var rec = chunk.pick(record, ['id','name','perms']);
	
         	let fetchData = {
         	method: 'POST',
         	body: JSON.stringify( rec ),
         	headers: {
                	'Content-Type':'application/json',
			'x-auth': token
         	}
        	}

        	fetch(`https://us-central1-cova-rms.cloudfunctions.net/saveRole`,fetchData). then (function (response) {
			response.text().then( (v) => {
			if (response.ok) {
                        callback(true,null, JSON.parse(v))
			} else {
			  callback(false,v);
			}
			} );
		})
	},

	saveRecord(record, token, callback) {
	
         let fetchData = {
         method: 'POST',
         body: JSON.stringify( record ),
         headers: {
                'Content-Type':'application/json',
		'x-auth': token
         }
        }

        fetch(`https://us-central1-cova-rms.cloudfunctions.net/saveRecord`,fetchData). then (function (response) {
                if(response.ok ) {
                        callback(true, record)
                } else {
                        callback(false,response.statusText)
                }
        });
	

	},

	getUserDetails(token, callback) {
		let fetchData= {
		  method: 'GET',
		  headers: {
			'Content-Type':'application/json',
			'x-auth':token
		  }
		}
		fetch(`https://us-central1-cova-rms-cloudfunctions.get/getUser, fetchData`).then(function(response) {

		}).catch(function(err){
		});
        },

	 getAll(token, callback, getUserInfo) {
         	let fetchData = {
         	method: 'GET',
         	headers: {
                	'Content-Type':'application/json',
			'x-auth': token
         	}
		}
		var url = `https://us-central1-cova-rms.cloudfunctions.net/getAll` ;
		if(getUserInfo === true ){
			url = url + "?user=true"
		} 	
	
        	fetch(url ,fetchData). then (function (response) {
                	if(response.ok ) {
		           response.json().then(function(data) {  
			  	   callback(true,data)
      				});  

                	} else {
			  callback(false,response.statusText)
                	}
        	}).catch(function(err) {  
  		});



	   }



}

module.exports = server;
